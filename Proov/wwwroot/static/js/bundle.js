/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"bundle": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// script path function
/******/ 	function jsonpScriptSrc(chunkId) {
/******/ 		return __webpack_require__.p + "static/js/" + ({}[chunkId]||chunkId) + ".js"
/******/ 	}
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/ 	// This file contains only the entry chunk.
/******/ 	// The chunk loading function for additional chunks
/******/ 	__webpack_require__.e = function requireEnsure(chunkId) {
/******/ 		var promises = [];
/******/
/******/
/******/ 		// JSONP chunk loading for javascript
/******/
/******/ 		var installedChunkData = installedChunks[chunkId];
/******/ 		if(installedChunkData !== 0) { // 0 means "already installed".
/******/
/******/ 			// a Promise means "currently loading".
/******/ 			if(installedChunkData) {
/******/ 				promises.push(installedChunkData[2]);
/******/ 			} else {
/******/ 				// setup Promise in chunk cache
/******/ 				var promise = new Promise(function(resolve, reject) {
/******/ 					installedChunkData = installedChunks[chunkId] = [resolve, reject];
/******/ 				});
/******/ 				promises.push(installedChunkData[2] = promise);
/******/
/******/ 				// start chunk loading
/******/ 				var script = document.createElement('script');
/******/ 				var onScriptComplete;
/******/
/******/ 				script.charset = 'utf-8';
/******/ 				script.timeout = 120;
/******/ 				if (__webpack_require__.nc) {
/******/ 					script.setAttribute("nonce", __webpack_require__.nc);
/******/ 				}
/******/ 				script.src = jsonpScriptSrc(chunkId);
/******/
/******/ 				onScriptComplete = function (event) {
/******/ 					// avoid mem leaks in IE.
/******/ 					script.onerror = script.onload = null;
/******/ 					clearTimeout(timeout);
/******/ 					var chunk = installedChunks[chunkId];
/******/ 					if(chunk !== 0) {
/******/ 						if(chunk) {
/******/ 							var errorType = event && (event.type === 'load' ? 'missing' : event.type);
/******/ 							var realSrc = event && event.target && event.target.src;
/******/ 							var error = new Error('Loading chunk ' + chunkId + ' failed.\n(' + errorType + ': ' + realSrc + ')');
/******/ 							error.type = errorType;
/******/ 							error.request = realSrc;
/******/ 							chunk[1](error);
/******/ 						}
/******/ 						installedChunks[chunkId] = undefined;
/******/ 					}
/******/ 				};
/******/ 				var timeout = setTimeout(function(){
/******/ 					onScriptComplete({ type: 'timeout', target: script });
/******/ 				}, 120000);
/******/ 				script.onerror = script.onload = onScriptComplete;
/******/ 				document.head.appendChild(script);
/******/ 			}
/******/ 		}
/******/ 		return Promise.all(promises);
/******/ 	};
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// on error function for async loading
/******/ 	__webpack_require__.oe = function(err) { console.error(err); throw err; };
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push([0,"vendor"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./ClientApp/public/styles/app.sass":
/*!******************************************!*\
  !*** ./ClientApp/public/styles/app.sass ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("\nvar content = __webpack_require__(/*! !../../../node_modules/mini-css-extract-plugin/dist/loader.js!../../../node_modules/css-loader!../../../node_modules/sass-loader/lib/loader.js!./app.sass */ \"./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/index.js!./node_modules/sass-loader/lib/loader.js!./ClientApp/public/styles/app.sass\");\n\nif(typeof content === 'string') content = [[module.i, content, '']];\n\nvar transform;\nvar insertInto;\n\n\n\nvar options = {\"hmr\":true}\n\noptions.transform = transform\noptions.insertInto = undefined;\n\nvar update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ \"./node_modules/style-loader/lib/addStyles.js\")(content, options);\n\nif(content.locals) module.exports = content.locals;\n\nif(false) {}\n\n//# sourceURL=webpack:///./ClientApp/public/styles/app.sass?");

/***/ }),

/***/ "./ClientApp/public/styles/application-42da8c9352a73650a56904b49a08ef420cc42da1834ab33cac8f813933119916.css":
/*!******************************************************************************************************************!*\
  !*** ./ClientApp/public/styles/application-42da8c9352a73650a56904b49a08ef420cc42da1834ab33cac8f813933119916.css ***!
  \******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// style-loader: Adds some css to the DOM by adding a <style> tag\n\n// load the styles\nvar content = __webpack_require__(/*! !../../../node_modules/style-loader!../../../node_modules/mini-css-extract-plugin/dist/loader.js!../../../node_modules/css-loader!./application-42da8c9352a73650a56904b49a08ef420cc42da1834ab33cac8f813933119916.css */ \"./node_modules/style-loader/index.js!./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/index.js!./ClientApp/public/styles/application-42da8c9352a73650a56904b49a08ef420cc42da1834ab33cac8f813933119916.css\");\nif(typeof content === 'string') content = [[module.i, content, '']];\nif(content.locals) module.exports = content.locals;\n// add the styles to the DOM\nvar add = __webpack_require__(/*! ../../../node_modules/vue-style-loader/lib/addStylesClient.js */ \"./node_modules/vue-style-loader/lib/addStylesClient.js\").default\nvar update = add(\"23389d3f\", content, false, {});\n// Hot Module Replacement\nif(false) {}\n\n//# sourceURL=webpack:///./ClientApp/public/styles/application-42da8c9352a73650a56904b49a08ef420cc42da1834ab33cac8f813933119916.css?");

/***/ }),

/***/ "./ClientApp/public/styles/avatar-group.sass":
/*!***************************************************!*\
  !*** ./ClientApp/public/styles/avatar-group.sass ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("\nvar content = __webpack_require__(/*! !../../../node_modules/mini-css-extract-plugin/dist/loader.js!../../../node_modules/css-loader!../../../node_modules/sass-loader/lib/loader.js!./avatar-group.sass */ \"./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/index.js!./node_modules/sass-loader/lib/loader.js!./ClientApp/public/styles/avatar-group.sass\");\n\nif(typeof content === 'string') content = [[module.i, content, '']];\n\nvar transform;\nvar insertInto;\n\n\n\nvar options = {\"hmr\":true}\n\noptions.transform = transform\noptions.insertInto = undefined;\n\nvar update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ \"./node_modules/style-loader/lib/addStyles.js\")(content, options);\n\nif(content.locals) module.exports = content.locals;\n\nif(false) {}\n\n//# sourceURL=webpack:///./ClientApp/public/styles/avatar-group.sass?");

/***/ }),

/***/ "./ClientApp/public/styles/bootstrap.scss":
/*!************************************************!*\
  !*** ./ClientApp/public/styles/bootstrap.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("\nvar content = __webpack_require__(/*! !../../../node_modules/mini-css-extract-plugin/dist/loader.js!../../../node_modules/css-loader!../../../node_modules/sass-loader/lib/loader.js!./bootstrap.scss */ \"./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/index.js!./node_modules/sass-loader/lib/loader.js!./ClientApp/public/styles/bootstrap.scss\");\n\nif(typeof content === 'string') content = [[module.i, content, '']];\n\nvar transform;\nvar insertInto;\n\n\n\nvar options = {\"hmr\":true}\n\noptions.transform = transform\noptions.insertInto = undefined;\n\nvar update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ \"./node_modules/style-loader/lib/addStyles.js\")(content, options);\n\nif(content.locals) module.exports = content.locals;\n\nif(false) {}\n\n//# sourceURL=webpack:///./ClientApp/public/styles/bootstrap.scss?");

/***/ }),

/***/ "./ClientApp/public/styles/bracket.sass":
/*!**********************************************!*\
  !*** ./ClientApp/public/styles/bracket.sass ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("\nvar content = __webpack_require__(/*! !../../../node_modules/mini-css-extract-plugin/dist/loader.js!../../../node_modules/css-loader!../../../node_modules/sass-loader/lib/loader.js!./bracket.sass */ \"./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/index.js!./node_modules/sass-loader/lib/loader.js!./ClientApp/public/styles/bracket.sass\");\n\nif(typeof content === 'string') content = [[module.i, content, '']];\n\nvar transform;\nvar insertInto;\n\n\n\nvar options = {\"hmr\":true}\n\noptions.transform = transform\noptions.insertInto = undefined;\n\nvar update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ \"./node_modules/style-loader/lib/addStyles.js\")(content, options);\n\nif(content.locals) module.exports = content.locals;\n\nif(false) {}\n\n//# sourceURL=webpack:///./ClientApp/public/styles/bracket.sass?");

/***/ }),

/***/ "./ClientApp/public/styles/breadcrumb.css":
/*!************************************************!*\
  !*** ./ClientApp/public/styles/breadcrumb.css ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// style-loader: Adds some css to the DOM by adding a <style> tag\n\n// load the styles\nvar content = __webpack_require__(/*! !../../../node_modules/style-loader!../../../node_modules/mini-css-extract-plugin/dist/loader.js!../../../node_modules/css-loader!./breadcrumb.css */ \"./node_modules/style-loader/index.js!./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/index.js!./ClientApp/public/styles/breadcrumb.css\");\nif(typeof content === 'string') content = [[module.i, content, '']];\nif(content.locals) module.exports = content.locals;\n// add the styles to the DOM\nvar add = __webpack_require__(/*! ../../../node_modules/vue-style-loader/lib/addStylesClient.js */ \"./node_modules/vue-style-loader/lib/addStylesClient.js\").default\nvar update = add(\"0cd3d77f\", content, false, {});\n// Hot Module Replacement\nif(false) {}\n\n//# sourceURL=webpack:///./ClientApp/public/styles/breadcrumb.css?");

/***/ }),

/***/ "./ClientApp/public/styles/custom.sass":
/*!*********************************************!*\
  !*** ./ClientApp/public/styles/custom.sass ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("\nvar content = __webpack_require__(/*! !../../../node_modules/mini-css-extract-plugin/dist/loader.js!../../../node_modules/css-loader!../../../node_modules/sass-loader/lib/loader.js!./custom.sass */ \"./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/index.js!./node_modules/sass-loader/lib/loader.js!./ClientApp/public/styles/custom.sass\");\n\nif(typeof content === 'string') content = [[module.i, content, '']];\n\nvar transform;\nvar insertInto;\n\n\n\nvar options = {\"hmr\":true}\n\noptions.transform = transform\noptions.insertInto = undefined;\n\nvar update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ \"./node_modules/style-loader/lib/addStyles.js\")(content, options);\n\nif(content.locals) module.exports = content.locals;\n\nif(false) {}\n\n//# sourceURL=webpack:///./ClientApp/public/styles/custom.sass?");

/***/ }),

/***/ "./ClientApp/public/styles/multiselect.sass":
/*!**************************************************!*\
  !*** ./ClientApp/public/styles/multiselect.sass ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("\nvar content = __webpack_require__(/*! !../../../node_modules/mini-css-extract-plugin/dist/loader.js!../../../node_modules/css-loader!../../../node_modules/sass-loader/lib/loader.js!./multiselect.sass */ \"./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/index.js!./node_modules/sass-loader/lib/loader.js!./ClientApp/public/styles/multiselect.sass\");\n\nif(typeof content === 'string') content = [[module.i, content, '']];\n\nvar transform;\nvar insertInto;\n\n\n\nvar options = {\"hmr\":true}\n\noptions.transform = transform\noptions.insertInto = undefined;\n\nvar update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ \"./node_modules/style-loader/lib/addStyles.js\")(content, options);\n\nif(content.locals) module.exports = content.locals;\n\nif(false) {}\n\n//# sourceURL=webpack:///./ClientApp/public/styles/multiselect.sass?");

/***/ }),

/***/ "./ClientApp/public/styles/placeholder.sass":
/*!**************************************************!*\
  !*** ./ClientApp/public/styles/placeholder.sass ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("\nvar content = __webpack_require__(/*! !../../../node_modules/mini-css-extract-plugin/dist/loader.js!../../../node_modules/css-loader!../../../node_modules/sass-loader/lib/loader.js!./placeholder.sass */ \"./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/index.js!./node_modules/sass-loader/lib/loader.js!./ClientApp/public/styles/placeholder.sass\");\n\nif(typeof content === 'string') content = [[module.i, content, '']];\n\nvar transform;\nvar insertInto;\n\n\n\nvar options = {\"hmr\":true}\n\noptions.transform = transform\noptions.insertInto = undefined;\n\nvar update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ \"./node_modules/style-loader/lib/addStyles.js\")(content, options);\n\nif(content.locals) module.exports = content.locals;\n\nif(false) {}\n\n//# sourceURL=webpack:///./ClientApp/public/styles/placeholder.sass?");

/***/ }),

/***/ "./ClientApp/public/styles/theme.css":
/*!*******************************************!*\
  !*** ./ClientApp/public/styles/theme.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// style-loader: Adds some css to the DOM by adding a <style> tag\n\n// load the styles\nvar content = __webpack_require__(/*! !../../../node_modules/style-loader!../../../node_modules/mini-css-extract-plugin/dist/loader.js!../../../node_modules/css-loader!./theme.css */ \"./node_modules/style-loader/index.js!./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/index.js!./ClientApp/public/styles/theme.css\");\nif(typeof content === 'string') content = [[module.i, content, '']];\nif(content.locals) module.exports = content.locals;\n// add the styles to the DOM\nvar add = __webpack_require__(/*! ../../../node_modules/vue-style-loader/lib/addStylesClient.js */ \"./node_modules/vue-style-loader/lib/addStylesClient.js\").default\nvar update = add(\"0e59ca12\", content, false, {});\n// Hot Module Replacement\nif(false) {}\n\n//# sourceURL=webpack:///./ClientApp/public/styles/theme.css?");

/***/ }),

/***/ "./ClientApp/public/styles/theme_.css":
/*!********************************************!*\
  !*** ./ClientApp/public/styles/theme_.css ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// style-loader: Adds some css to the DOM by adding a <style> tag\n\n// load the styles\nvar content = __webpack_require__(/*! !../../../node_modules/style-loader!../../../node_modules/mini-css-extract-plugin/dist/loader.js!../../../node_modules/css-loader!./theme_.css */ \"./node_modules/style-loader/index.js!./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/index.js!./ClientApp/public/styles/theme_.css\");\nif(typeof content === 'string') content = [[module.i, content, '']];\nif(content.locals) module.exports = content.locals;\n// add the styles to the DOM\nvar add = __webpack_require__(/*! ../../../node_modules/vue-style-loader/lib/addStylesClient.js */ \"./node_modules/vue-style-loader/lib/addStylesClient.js\").default\nvar update = add(\"f38c2242\", content, false, {});\n// Hot Module Replacement\nif(false) {}\n\n//# sourceURL=webpack:///./ClientApp/public/styles/theme_.css?");

/***/ }),

/***/ "./ClientApp/public/styles/theme__.css":
/*!*********************************************!*\
  !*** ./ClientApp/public/styles/theme__.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// style-loader: Adds some css to the DOM by adding a <style> tag\n\n// load the styles\nvar content = __webpack_require__(/*! !../../../node_modules/style-loader!../../../node_modules/mini-css-extract-plugin/dist/loader.js!../../../node_modules/css-loader!./theme__.css */ \"./node_modules/style-loader/index.js!./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/index.js!./ClientApp/public/styles/theme__.css\");\nif(typeof content === 'string') content = [[module.i, content, '']];\nif(content.locals) module.exports = content.locals;\n// add the styles to the DOM\nvar add = __webpack_require__(/*! ../../../node_modules/vue-style-loader/lib/addStylesClient.js */ \"./node_modules/vue-style-loader/lib/addStylesClient.js\").default\nvar update = add(\"5fc828b7\", content, false, {});\n// Hot Module Replacement\nif(false) {}\n\n//# sourceURL=webpack:///./ClientApp/public/styles/theme__.css?");

/***/ }),

/***/ "./ClientApp/src/App.vue":
/*!*******************************!*\
  !*** ./ClientApp/src/App.vue ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _App_vue_vue_type_template_id_fcf2c342___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./App.vue?vue&type=template&id=fcf2c342& */ \"./ClientApp/src/App.vue?vue&type=template&id=fcf2c342&\");\n/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ \"./node_modules/vue-loader/lib/runtime/componentNormalizer.js\");\n\nvar script = {}\n\n\n/* normalize component */\n\nvar component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__[\"default\"])(\n  script,\n  _App_vue_vue_type_template_id_fcf2c342___WEBPACK_IMPORTED_MODULE_0__[\"render\"],\n  _App_vue_vue_type_template_id_fcf2c342___WEBPACK_IMPORTED_MODULE_0__[\"staticRenderFns\"],\n  false,\n  null,\n  null,\n  null\n  \n)\n\n/* hot reload */\nif (false) { var api; }\ncomponent.options.__file = \"ClientApp/src/App.vue\"\n/* harmony default export */ __webpack_exports__[\"default\"] = (component.exports);\n\n//# sourceURL=webpack:///./ClientApp/src/App.vue?");

/***/ }),

/***/ "./ClientApp/src/App.vue?vue&type=template&id=fcf2c342&":
/*!**************************************************************!*\
  !*** ./ClientApp/src/App.vue?vue&type=template&id=fcf2c342& ***!
  \**************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_template_id_fcf2c342___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../node_modules/vue-loader/lib??vue-loader-options!./App.vue?vue&type=template&id=fcf2c342& */ \"./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./ClientApp/src/App.vue?vue&type=template&id=fcf2c342&\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"render\", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_template_id_fcf2c342___WEBPACK_IMPORTED_MODULE_0__[\"render\"]; });\n\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"staticRenderFns\", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_template_id_fcf2c342___WEBPACK_IMPORTED_MODULE_0__[\"staticRenderFns\"]; });\n\n\n\n//# sourceURL=webpack:///./ClientApp/src/App.vue?");

/***/ }),

/***/ "./ClientApp/src/app.js":
/*!******************************!*\
  !*** ./ClientApp/src/app.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\nexports.store = exports.router = exports.app = undefined;\n\nvar _vue = __webpack_require__(/*! vue */ \"./node_modules/vue/dist/vue.esm.js\");\n\nvar _vue2 = _interopRequireDefault(_vue);\n\nvar _App = __webpack_require__(/*! ./App.vue */ \"./ClientApp/src/App.vue\");\n\nvar _App2 = _interopRequireDefault(_App);\n\nvar _vueRouter = __webpack_require__(/*! ./plugins/vue-router */ \"./ClientApp/src/plugins/vue-router.js\");\n\nvar _index = __webpack_require__(/*! ./store/index */ \"./ClientApp/src/store/index.js\");\n\nvar _index2 = _interopRequireDefault(_index);\n\n__webpack_require__(/*! ./plugins/vuex-router-sync */ \"./ClientApp/src/plugins/vuex-router-sync.js\");\n\n__webpack_require__(/*! ./plugins/axios */ \"./ClientApp/src/plugins/axios.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n// import './plugins/fortawesome'\n// import './plugins/vue-component-server'\n\nvar app = new _vue2.default({\n    store: _index2.default,\n    router: _vueRouter.router,\n    render: function render(h) {\n        return h(_App2.default);\n    }\n});\n\nexports.app = app;\nexports.router = _vueRouter.router;\nexports.store = _index2.default;\n\n//# sourceURL=webpack:///./ClientApp/src/app.js?");

/***/ }),

/***/ "./ClientApp/src/entry-client.js":
/*!***************************************!*\
  !*** ./ClientApp/src/entry-client.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\n__webpack_require__(/*! ./plugins/multiselect */ \"./ClientApp/src/plugins/multiselect.js\");\n\n__webpack_require__(/*! ./plugins/stylesheet */ \"./ClientApp/src/plugins/stylesheet.js\");\n\nvar _app = __webpack_require__(/*! ./app */ \"./ClientApp/src/app.js\");\n\n_app.store.replaceState(__INITIAL_STATE__); // import './plugins/turbolinks'\n// import './plugins/vue-turbolinks'\n// import './plugins/vue-component-client'\n\n\n_app.router.onReady(function () {\n    _app.router.beforeResolve(function (to, from, next) {\n        var matched = _app.router.getMatchedComponents(to);\n        var prevMatched = _app.router.getMatchedComponents(from);\n\n        // compare two list of components from previous route and current route\n        var diffed = false;\n        var activated = matched.filter(function (c, i) {\n            return diffed || (diffed = prevMatched[i] !== c);\n        });\n\n        // if no new components loaded, do nothing\n        if (!activated.length) {\n            return next();\n        }\n\n        // NProgress.start()\n\n        // for each newly loaded components, asynchorously load data to them\n        Promise.all(activated.map(function (c) {\n            if (c.asyncData) {\n                return c.asyncData({ store: _app.store, route: to });\n            }\n        })).then(function () {\n            // NProgress.done()\n            next();\n        }).catch(next);\n    });\n    _app.app.$mount('#app');\n});\n\n//# sourceURL=webpack:///./ClientApp/src/entry-client.js?");

/***/ }),

/***/ "./ClientApp/src/plugins/axios.js":
/*!****************************************!*\
  !*** ./ClientApp/src/plugins/axios.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar _vue = __webpack_require__(/*! vue */ \"./node_modules/vue/dist/vue.esm.js\");\n\nvar _vue2 = _interopRequireDefault(_vue);\n\nvar _axios = __webpack_require__(/*! axios */ \"./node_modules/axios/index.js\");\n\nvar _axios2 = _interopRequireDefault(_axios);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n/* ============\r\n * Axios\r\n * ============\r\n *\r\n * Promise based HTTP client for the browser and node.js.\r\n * Because Vue Resource has been retired, Axios will now been used\r\n * to perform AJAX-requests.\r\n *\r\n * https://github.com/mzabriskie/axios\r\n */\n\n_axios2.default.defaults.baseURL = '';\n_axios2.default.defaults.headers.common.Accept = 'application/json';\n\n// Bind Axios to Vue.\n_vue2.default.$http = _axios2.default;\nObject.defineProperty(_vue2.default.prototype, '$http', {\n    get: function get() {\n        return _axios2.default;\n    }\n});\n\n//# sourceURL=webpack:///./ClientApp/src/plugins/axios.js?");

/***/ }),

/***/ "./ClientApp/src/plugins/multiselect.js":
/*!**********************************************!*\
  !*** ./ClientApp/src/plugins/multiselect.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar _vue = __webpack_require__(/*! vue */ \"./node_modules/vue/dist/vue.esm.js\");\n\nvar _vue2 = _interopRequireDefault(_vue);\n\nvar _vueMultiselect = __webpack_require__(/*! vue-multiselect */ \"./node_modules/vue-multiselect/dist/vue-multiselect.min.js\");\n\nvar _vueMultiselect2 = _interopRequireDefault(_vueMultiselect);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n// register globally\n/* ============\n * Vue Multiselect\n * ============\n *\n * Multiselect\n *\n * http://monterail.github.io/vue-multiselect/\n */\n\n_vue2.default.component('multiselect', _vueMultiselect2.default);\n\n//# sourceURL=webpack:///./ClientApp/src/plugins/multiselect.js?");

/***/ }),

/***/ "./ClientApp/src/plugins/stylesheet.js":
/*!*********************************************!*\
  !*** ./ClientApp/src/plugins/stylesheet.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\n__webpack_require__(/*! ../../public/styles/bootstrap.scss */ \"./ClientApp/public/styles/bootstrap.scss\");\n\n__webpack_require__(/*! ../../public/styles/multiselect.sass */ \"./ClientApp/public/styles/multiselect.sass\");\n\n__webpack_require__(/*! ../../public/styles/placeholder.sass */ \"./ClientApp/public/styles/placeholder.sass\");\n\n__webpack_require__(/*! ../../public/styles/theme.css */ \"./ClientApp/public/styles/theme.css\");\n\n__webpack_require__(/*! ../../public/styles/theme_.css */ \"./ClientApp/public/styles/theme_.css\");\n\n__webpack_require__(/*! ../../public/styles/theme__.css */ \"./ClientApp/public/styles/theme__.css\");\n\n__webpack_require__(/*! ../../public/styles/breadcrumb.css */ \"./ClientApp/public/styles/breadcrumb.css\");\n\n__webpack_require__(/*! ../../public/styles/app.sass */ \"./ClientApp/public/styles/app.sass\");\n\n__webpack_require__(/*! ../../public/styles/bracket.sass */ \"./ClientApp/public/styles/bracket.sass\");\n\n__webpack_require__(/*! ../../public/styles/avatar-group.sass */ \"./ClientApp/public/styles/avatar-group.sass\");\n\n__webpack_require__(/*! ../../public/styles/application-42da8c9352a73650a56904b49a08ef420cc42da1834ab33cac8f813933119916.css */ \"./ClientApp/public/styles/application-42da8c9352a73650a56904b49a08ef420cc42da1834ab33cac8f813933119916.css\");\n\n__webpack_require__(/*! ../../public/styles/custom.sass */ \"./ClientApp/public/styles/custom.sass\");\n\n//# sourceURL=webpack:///./ClientApp/src/plugins/stylesheet.js?");

/***/ }),

/***/ "./ClientApp/src/plugins/vue-router.js":
/*!*********************************************!*\
  !*** ./ClientApp/src/plugins/vue-router.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\nexports.router = undefined;\n\nvar _vue = __webpack_require__(/*! vue */ \"./node_modules/vue/dist/vue.esm.js\");\n\nvar _vue2 = _interopRequireDefault(_vue);\n\nvar _vueRouter = __webpack_require__(/*! vue-router */ \"./node_modules/vue-router/dist/vue-router.esm.js\");\n\nvar _vueRouter2 = _interopRequireDefault(_vueRouter);\n\nvar _router = __webpack_require__(/*! @/router */ \"./ClientApp/src/router/index.js\");\n\nvar _router2 = _interopRequireDefault(_router);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n_vue2.default.use(_vueRouter2.default); /* ============\r\n                                         * Vue Router\r\n                                         * ============\r\n                                         *\r\n                                         * The official Router for Vue.js. It deeply integrates with Vue.js core\r\n                                         * to make building Single Page Applications with Vue.js a breeze.\r\n                                         *\r\n                                         * http://router.vuejs.org/en/index.html\r\n                                         */\n\nvar router = exports.router = new _vueRouter2.default({\n  mode: 'history',\n  routes: _router2.default\n});\n\n_vue2.default.router = router;\n\nexports.default = {\n  router: router\n};\n\n//# sourceURL=webpack:///./ClientApp/src/plugins/vue-router.js?");

/***/ }),

/***/ "./ClientApp/src/plugins/vuex-router-sync.js":
/*!***************************************************!*\
  !*** ./ClientApp/src/plugins/vuex-router-sync.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar _vuexRouterSync = __webpack_require__(/*! vuex-router-sync */ \"./node_modules/vuex-router-sync/index.js\");\n\nvar _vuexRouterSync2 = _interopRequireDefault(_vuexRouterSync);\n\nvar _store = __webpack_require__(/*! @/store */ \"./ClientApp/src/store/index.js\");\n\nvar _store2 = _interopRequireDefault(_store);\n\nvar _vueRouter = __webpack_require__(/*! ./vue-router */ \"./ClientApp/src/plugins/vue-router.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n_vuexRouterSync2.default.sync(_store2.default, _vueRouter.router); /* ============\r\n                                                                    * Vuex Router Sync\r\n                                                                    * ============\r\n                                                                    *\r\n                                                                    * Sync vue-router's current $route as part of vuex store's state.\r\n                                                                    *\r\n                                                                    * https://github.com/vuejs/vuex-router-sync\r\n                                                                    */\n\n//# sourceURL=webpack:///./ClientApp/src/plugins/vuex-router-sync.js?");

/***/ }),

/***/ "./ClientApp/src/proxies/AccountProxy.js":
/*!***********************************************!*\
  !*** ./ClientApp/src/proxies/AccountProxy.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\n\nvar _Proxy2 = __webpack_require__(/*! ./Proxy */ \"./ClientApp/src/proxies/Proxy.js\");\n\nvar _Proxy3 = _interopRequireDefault(_Proxy2);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError(\"this hasn't been initialised - super() hasn't been called\"); } return call && (typeof call === \"object\" || typeof call === \"function\") ? call : self; }\n\nfunction _inherits(subClass, superClass) { if (typeof superClass !== \"function\" && superClass !== null) { throw new TypeError(\"Super expression must either be null or a function, not \" + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }\n\nvar AccountProxy = function (_Proxy) {\n  _inherits(AccountProxy, _Proxy);\n\n  /**\n   * The constructor for the ArtistProxy.\n   *\n   * @param {Object} parameters The query parameters.\n   */\n  function AccountProxy() {\n    var parameters = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};\n\n    _classCallCheck(this, AccountProxy);\n\n    return _possibleConstructorReturn(this, (AccountProxy.__proto__ || Object.getPrototypeOf(AccountProxy)).call(this, 'api/profile', parameters));\n  }\n\n  return AccountProxy;\n}(_Proxy3.default);\n\nexports.default = AccountProxy;\n\n//# sourceURL=webpack:///./ClientApp/src/proxies/AccountProxy.js?");

/***/ }),

/***/ "./ClientApp/src/proxies/AuthProxy.js":
/*!********************************************!*\
  !*** ./ClientApp/src/proxies/AuthProxy.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\n\nvar _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();\n\nvar _Proxy2 = __webpack_require__(/*! ./Proxy */ \"./ClientApp/src/proxies/Proxy.js\");\n\nvar _Proxy3 = _interopRequireDefault(_Proxy2);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError(\"this hasn't been initialised - super() hasn't been called\"); } return call && (typeof call === \"object\" || typeof call === \"function\") ? call : self; }\n\nfunction _inherits(subClass, superClass) { if (typeof superClass !== \"function\" && superClass !== null) { throw new TypeError(\"Super expression must either be null or a function, not \" + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }\n\nvar AuthProxy = function (_Proxy) {\n  _inherits(AuthProxy, _Proxy);\n\n  /**\n   * The constructor for the Proxy.\n   *\n   * @param {Object} parameters The query parameters.\n   */\n  function AuthProxy() {\n    var parameters = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};\n\n    _classCallCheck(this, AuthProxy);\n\n    return _possibleConstructorReturn(this, (AuthProxy.__proto__ || Object.getPrototypeOf(AuthProxy)).call(this, 'auth', parameters));\n  }\n\n  /**\n   * Method used to login.\n   *\n   * @param {String} username The username.\n   * @param {String} password The password.\n   *\n   * @returns {Promise} The result in a promise.\n   */\n\n\n  _createClass(AuthProxy, [{\n    key: 'login',\n    value: function login(data) {\n      return this.submit('post', '/' + this.endpoint + '/login', data);\n    }\n\n    /**\n     * Method used to register the user.\n     *\n     * @param {Object} data The register data.\n     *\n     * @returns {Promise} The result in a promise.\n     */\n\n  }, {\n    key: 'register',\n    value: function register(data) {\n      return this.submit('post', '/' + this.endpoint + '/register', data);\n    }\n  }]);\n\n  return AuthProxy;\n}(_Proxy3.default);\n\nexports.default = AuthProxy;\n\n//# sourceURL=webpack:///./ClientApp/src/proxies/AuthProxy.js?");

/***/ }),

/***/ "./ClientApp/src/proxies/Proxy.js":
/*!****************************************!*\
  !*** ./ClientApp/src/proxies/Proxy.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\n\nvar _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();\n\nvar _vue = __webpack_require__(/*! vue */ \"./node_modules/vue/dist/vue.esm.js\");\n\nvar _vue2 = _interopRequireDefault(_vue);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\n/* eslint-disable prefer-promise-reject-errors */\nvar BaseProxy = function () {\n  /**\n   * The constructor of the BaseProxy.\n   *\n   * @param {string} endpoint   The endpoint being used.\n   * @param {Object} parameters The parameters for the request.\n   */\n  function BaseProxy(endpoint) {\n    var parameters = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};\n\n    _classCallCheck(this, BaseProxy);\n\n    this.endpoint = endpoint;\n    this.parameters = parameters;\n  }\n\n  /**\n   * Method used to set the query parameters.\n   *\n   * @param {Object} parameters The given parameters.\n   *\n   * @returns {BaseProxy} The instance of the proxy.\n   */\n\n\n  _createClass(BaseProxy, [{\n    key: 'setParameters',\n    value: function setParameters(parameters) {\n      var _this = this;\n\n      Object.keys(parameters).forEach(function (key) {\n        _this.parameters[key] = parameters[key];\n      });\n\n      return this;\n    }\n\n    /**\n     * Method used to set a single parameter.\n     *\n     * @param {string} parameter The given parameter.\n     * @param {*} value The value to be set.\n     *\n     * @returns {BaseProxy} The instance of the proxy.\n     */\n\n  }, {\n    key: 'setParameter',\n    value: function setParameter(parameter, value) {\n      this.parameters[parameter] = value;\n\n      return this;\n    }\n\n    /**\n     * Method used to remove all the parameters.\n     *\n     * @param {Array} parameters The given parameters.\n     *\n     * @returns {BaseProxy} The instance of the proxy.\n     */\n\n  }, {\n    key: 'removeParameters',\n    value: function removeParameters(parameters) {\n      var _this2 = this;\n\n      parameters.forEach(function (parameter) {\n        delete _this2.parameters[parameter];\n      });\n\n      return this;\n    }\n\n    /**\n     * Method used to remove a single parameter.\n     *\n     * @param {string} parameter The given parameter.\n     *\n     * @returns {BaseProxy} The instance of the proxy.\n     */\n\n  }, {\n    key: 'removeParameter',\n    value: function removeParameter(parameter) {\n      delete this.parameters[parameter];\n\n      return this;\n    }\n\n    /**\n     * The method used to perform an AJAX-request.\n     *\n     * @param {string}      requestType The request type.\n     * @param {string}      url         The URL for the request.\n     * @param {Object|null} data        The data to be send with the request.\n     *\n     * @returns {Promise} The result in a promise.\n     */\n\n  }, {\n    key: 'submit',\n    value: function submit(requestType, url) {\n      var _this3 = this;\n\n      var data = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;\n\n      return new Promise(function (resolve, reject) {\n        _vue2.default.$http[requestType](url + _this3.getParameterString(), data).then(function (response) {\n          resolve(response.data);\n        }).catch(function (_ref) {\n          var response = _ref.response;\n\n          if (response) {\n            reject(response.data);\n          } else {\n            reject();\n          }\n        });\n      });\n    }\n\n    /**\n     * Method used to fetch all items from the API.\n     *\n     * @returns {Promise} The result in a promise.\n     */\n\n  }, {\n    key: 'all',\n    value: function all() {\n      return this.submit('get', '/' + this.endpoint);\n    }\n\n    /**\n     * Method used to fetch a single item from the API.\n     *\n     * @param {int} id The given identifier.\n     *\n     * @returns {Promise} The result in a promise.\n     */\n\n  }, {\n    key: 'find',\n    value: function find(id) {\n      return this.submit('get', '/' + this.endpoint + '/' + id);\n    }\n\n    /**\n     * Method used to create an item.\n     *\n     * @param {Object} item The given item.\n     *\n     * @returns {Promise} The result in a promise.\n     */\n\n  }, {\n    key: 'create',\n    value: function create(item) {\n      return this.submit('post', '/' + this.endpoint, item);\n    }\n\n    /**\n     * Method used to update an item.\n     *\n     * @param {int}    id   The given identifier.\n     * @param {Object} item The given item.\n     *\n     * @returns {Promise} The result in a promise.\n     */\n\n  }, {\n    key: 'update',\n    value: function update(id, item) {\n      return this.submit('put', '/' + this.endpoint + '/' + id, item);\n    }\n\n    /**\n     * Method used to destroy an item.\n     *\n     * @param {int} id The given identifier.\n     *\n     * @returns {Promise} The result in a promise.\n     */\n\n  }, {\n    key: 'destroy',\n    value: function destroy(id) {\n      return this.submit('delete', '/' + this.endpoint + '/' + id);\n    }\n\n    /**\n     * Method used to transform a parameters object to a parameters string.\n     *\n     * @returns {string} The parameter string.\n     */\n\n  }, {\n    key: 'getParameterString',\n    value: function getParameterString() {\n      var _this4 = this;\n\n      var keys = Object.keys(this.parameters);\n\n      var parameterStrings = keys.filter(function (key) {\n        return !!_this4.parameters[key];\n      }).map(function (key) {\n        return key + '=' + _this4.parameters[key];\n      });\n\n      return parameterStrings.length === 0 ? '' : '?' + parameterStrings.join('&');\n    }\n  }]);\n\n  return BaseProxy;\n}();\n\nexports.default = BaseProxy;\n\n//# sourceURL=webpack:///./ClientApp/src/proxies/Proxy.js?");

/***/ }),

/***/ "./ClientApp/src/router/index.js":
/*!***************************************!*\
  !*** ./ClientApp/src/router/index.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\nexports.default = [{\n    path: '/',\n    name: 'home',\n    component: function component() {\n        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(27)]).then(__webpack_require__.bind(null, /*! @/views/Home/Index.vue */ \"./ClientApp/src/views/Home/Index.vue\"));\n    }\n}, {\n    path: '/about/',\n    name: 'about',\n    component: function component() {\n        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(17)]).then(__webpack_require__.bind(null, /*! @/views/Home/About.vue */ \"./ClientApp/src/views/Home/About.vue\"));\n    }\n}, {\n    path: '/concept/',\n    name: 'concept',\n    component: function component() {\n        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(19)]).then(__webpack_require__.bind(null, /*! @/views/Home/Concept.vue */ \"./ClientApp/src/views/Home/Concept.vue\"));\n    }\n}, {\n    path: '/advantage/',\n    name: 'advantage',\n    component: function component() {\n        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(18)]).then(__webpack_require__.bind(null, /*! @/views/Home/Advantage.vue */ \"./ClientApp/src/views/Home/Advantage.vue\"));\n    }\n}, {\n    path: '/relationship/',\n    name: 'relationship',\n    component: function component() {\n        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(28)]).then(__webpack_require__.bind(null, /*! @/views/Home/Relationship.vue */ \"./ClientApp/src/views/Home/Relationship.vue\"));\n    }\n}, {\n    path: '/blogs/',\n    name: 'blogs',\n    component: function component() {\n        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(26)]).then(__webpack_require__.bind(null, /*! @/views/Home/Blogs.vue */ \"./ClientApp/src/views/Home/Blogs.vue\"));\n    }\n}, {\n    path: '/blog/',\n    name: 'blog',\n    component: function component() {\n        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(25)]).then(__webpack_require__.bind(null, /*! @/views/Home/Blog.vue */ \"./ClientApp/src/views/Home/Blog.vue\"));\n    }\n}, {\n    path: '/privacy-and-policy/',\n    name: 'privacy',\n    component: function component() {\n        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(1)]).then(__webpack_require__.bind(null, /*! @/views/Privacy.vue */ \"./ClientApp/src/views/Privacy.vue\"));\n    }\n}, {\n    path: '/terms-of-use/',\n    name: 'privacy',\n    component: function component() {\n        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(1)]).then(__webpack_require__.bind(null, /*! @/views/Privacy.vue */ \"./ClientApp/src/views/Privacy.vue\"));\n    }\n}, {\n    path: '/help/',\n    name: 'help',\n    component: function component() {\n        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(24)]).then(__webpack_require__.bind(null, /*! @/views/Help.vue */ \"./ClientApp/src/views/Help.vue\"));\n    }\n}, {\n    path: '/auth/login/',\n    name: 'login',\n    component: function component() {\n        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(21)]).then(__webpack_require__.bind(null, /*! @/views/Auth/Login.vue */ \"./ClientApp/src/views/Auth/Login.vue\"));\n    }\n}, {\n    path: '/auth/register/',\n    name: 'register',\n    component: function component() {\n        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(14)]).then(__webpack_require__.bind(null, /*! @/views/Auth/Register.vue */ \"./ClientApp/src/views/Auth/Register.vue\"));\n    }\n}, {\n    path: '/auth/recover-password/',\n    name: 'recover',\n    component: function component() {\n        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(13)]).then(__webpack_require__.bind(null, /*! @/views/Auth/Recover.vue */ \"./ClientApp/src/views/Auth/Recover.vue\"));\n    }\n}, {\n    path: '/auth/verify-code/',\n    name: 'verifyCode',\n    component: function component() {\n        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(15)]).then(__webpack_require__.bind(null, /*! @/views/Auth/VerifyCode.vue */ \"./ClientApp/src/views/Auth/VerifyCode.vue\"));\n    }\n}, {\n    path: '/auth/change-password/',\n    name: 'changePassword',\n    component: function component() {\n        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(12)]).then(__webpack_require__.bind(null, /*! @/views/Auth/ChangePassword.vue */ \"./ClientApp/src/views/Auth/ChangePassword.vue\"));\n    }\n}, {\n    path: '/auth/complete-role/',\n    name: 'completeRole',\n    component: function component() {\n        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(5)]).then(__webpack_require__.bind(null, /*! @/views/Auth/CompleteRole.vue */ \"./ClientApp/src/views/Auth/CompleteRole.vue\"));\n    }\n}, {\n    path: '/auth/complete-information/',\n    name: 'completeInformation',\n    component: function component() {\n        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(2)]).then(__webpack_require__.bind(null, /*! @/views/Auth/CompleteInformation.vue */ \"./ClientApp/src/views/Auth/CompleteInformation.vue\"));\n    }\n}, {\n    path: '/auth/complete-done/',\n    name: 'completeIsDone',\n    component: function component() {\n        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(16)]).then(__webpack_require__.bind(null, /*! @/views/Auth/CompleteIsDone.vue */ \"./ClientApp/src/views/Auth/CompleteIsDone.vue\"));\n    }\n}, {\n    path: '/auth/complete-seller/',\n    name: 'completeSeller',\n    component: function component() {\n        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(3)]).then(__webpack_require__.bind(null, /*! @/views/Auth/CompleteSeller.vue */ \"./ClientApp/src/views/Auth/CompleteSeller.vue\"));\n    }\n}, {\n    path: '/auth/complete-tenant/',\n    name: 'completeTenant',\n    component: function component() {\n        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(4)]).then(__webpack_require__.bind(null, /*! @/views/Auth/CompleteTenant.vue */ \"./ClientApp/src/views/Auth/CompleteTenant.vue\"));\n    }\n}, {\n    path: '/app/home/',\n    name: 'appHome',\n    component: function component() {\n        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(20)]).then(__webpack_require__.bind(null, /*! @/views/App/Index.vue */ \"./ClientApp/src/views/App/Index.vue\"));\n    }\n}, {\n    path: '/profile/:id(\\\\d+)/',\n    name: 'showProfile',\n    component: function component() {\n        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(30)]).then(__webpack_require__.bind(null, /*! @/views/Profile/Show.vue */ \"./ClientApp/src/views/Profile/Show.vue\"));\n    }\n}, {\n    path: '/profile/edit/role/',\n    name: 'editRole',\n    component: function component() {\n        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(29)]).then(__webpack_require__.bind(null, /*! @/views/Profile/EditRole.vue */ \"./ClientApp/src/views/Profile/EditRole.vue\"));\n    }\n}, {\n    path: '/profile/edit/password/',\n    name: 'editPassword',\n    component: function component() {\n        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(8)]).then(__webpack_require__.bind(null, /*! @/views/Profile/EditPassword.vue */ \"./ClientApp/src/views/Profile/EditPassword.vue\"));\n    }\n}, {\n    path: '/profile/edit/information/',\n    name: 'editInformation',\n    component: function component() {\n        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(7)]).then(__webpack_require__.bind(null, /*! @/views/Profile/EditInformation.vue */ \"./ClientApp/src/views/Profile/EditInformation.vue\"));\n    }\n}, {\n    path: '/buy/new/',\n    name: 'buyNew',\n    component: function component() {\n        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(22)]).then(__webpack_require__.bind(null, /*! @/views/Buy/Create.vue */ \"./ClientApp/src/views/Buy/Create.vue\"));\n    }\n}, {\n    path: '/buy/list/',\n    name: 'buyList',\n    component: function component() {\n        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(6)]).then(__webpack_require__.bind(null, /*! @/views/Buy/List.vue */ \"./ClientApp/src/views/Buy/List.vue\"));\n    }\n}, {\n    path: '/buy/edit/:id(\\\\d+)/',\n    name: 'buyEdit',\n    component: function component() {\n        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(23)]).then(__webpack_require__.bind(null, /*! @/views/Buy/Update.vue */ \"./ClientApp/src/views/Buy/Update.vue\"));\n    }\n}, {\n    path: '/rent/new/',\n    name: 'rentNew',\n    component: function component() {\n        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(35)]).then(__webpack_require__.bind(null, /*! @/views/Rent/Create.vue */ \"./ClientApp/src/views/Rent/Create.vue\"));\n    }\n}, {\n    path: '/rent/list/',\n    name: 'rentList',\n    component: function component() {\n        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(11)]).then(__webpack_require__.bind(null, /*! @/views/Rent/List.vue */ \"./ClientApp/src/views/Rent/List.vue\"));\n    }\n}, {\n    path: '/rent/edit/:id(\\\\d+)/',\n    name: 'rentEdit',\n    component: function component() {\n        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(36)]).then(__webpack_require__.bind(null, /*! @/views/Rent/Update.vue */ \"./ClientApp/src/views/Rent/Update.vue\"));\n    }\n}, {\n    path: '/proposal/:id(\\\\d+)/',\n    component: function component() {\n        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(10)]).then(__webpack_require__.bind(null, /*! @/views/Proposal/Show.vue */ \"./ClientApp/src/views/Proposal/Show.vue\"));\n    },\n    children: [{\n        path: '',\n        name: 'showProposal.overview',\n        component: function component() {\n            return __webpack_require__.e(/*! import() */ 34).then(__webpack_require__.bind(null, /*! @/views/Proposal/_Overview/Index */ \"./ClientApp/src/views/Proposal/_Overview/Index.vue\"));\n        }\n        // If the user needs to be a guest to view this page.\n    }]\n}, {\n    path: '/property/new/',\n    name: 'propertyNew',\n    component: function component() {\n        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(31)]).then(__webpack_require__.bind(null, /*! @/views/Property/Create.vue */ \"./ClientApp/src/views/Property/Create.vue\"));\n    }\n}, {\n    path: '/property/list/',\n    name: 'propertyList',\n    component: function component() {\n        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(9)]).then(__webpack_require__.bind(null, /*! @/views/Property/List.vue */ \"./ClientApp/src/views/Property/List.vue\"));\n    }\n}, {\n    path: '/property/:id(\\\\d+)/',\n    name: 'propertyShow',\n    component: function component() {\n        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(32)]).then(__webpack_require__.bind(null, /*! @/views/Property/Show.vue */ \"./ClientApp/src/views/Property/Show.vue\"));\n    }\n}, {\n    path: '/property/edit/:id(\\\\d+)/',\n    name: 'propertyEdit',\n    component: function component() {\n        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(33)]).then(__webpack_require__.bind(null, /*! @/views/Property/Update.vue */ \"./ClientApp/src/views/Property/Update.vue\"));\n    }\n}];\n\n//# sourceURL=webpack:///./ClientApp/src/router/index.js?");

/***/ }),

/***/ "./ClientApp/src/store/index.js":
/*!**************************************!*\
  !*** ./ClientApp/src/store/index.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\n\nvar _vue = __webpack_require__(/*! vue */ \"./node_modules/vue/dist/vue.esm.js\");\n\nvar _vue2 = _interopRequireDefault(_vue);\n\nvar _vuex = __webpack_require__(/*! vuex */ \"./node_modules/vuex/dist/vuex.esm.js\");\n\nvar _vuex2 = _interopRequireDefault(_vuex);\n\nvar _logger = __webpack_require__(/*! vuex/dist/logger */ \"./node_modules/vuex/dist/logger.js\");\n\nvar _logger2 = _interopRequireDefault(_logger);\n\nvar _alert = __webpack_require__(/*! ./modules/alert */ \"./ClientApp/src/store/modules/alert/index.js\");\n\nvar _alert2 = _interopRequireDefault(_alert);\n\nvar _auth = __webpack_require__(/*! ./modules/auth */ \"./ClientApp/src/store/modules/auth/index.js\");\n\nvar _auth2 = _interopRequireDefault(_auth);\n\nvar _account = __webpack_require__(/*! ./modules/account */ \"./ClientApp/src/store/modules/account/index.js\");\n\nvar _account2 = _interopRequireDefault(_account);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n/* ============\r\n * Vuex Store\r\n * ============\r\n *\r\n * The store of the application.\r\n *\r\n * http://vuex.vuejs.org/en/index.html\r\n */\n\n_vue2.default.use(_vuex2.default);\n\n// Modules\n\n\nvar debug = \"development\" !== 'production';\n\nexports.default = new _vuex2.default.Store({\n  /**\r\n   * Assign the modules to the store.\r\n   */\n  modules: {\n    auth: _auth2.default,\n    alert: _alert2.default,\n    account: _account2.default\n  },\n\n  /**\r\n   * If strict mode should be enabled.\r\n   */\n  strict: debug,\n\n  /**\r\n   * Plugins used in the store.\r\n   */\n  plugins: debug ? [(0, _logger2.default)()] : []\n});\n\n//# sourceURL=webpack:///./ClientApp/src/store/index.js?");

/***/ }),

/***/ "./ClientApp/src/store/modules/account/actions.js":
/*!********************************************************!*\
  !*** ./ClientApp/src/store/modules/account/actions.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\nexports.find = undefined;\n\nvar _AccountTransformer = __webpack_require__(/*! @/transformers/AccountTransformer */ \"./ClientApp/src/transformers/AccountTransformer.js\");\n\nvar _AccountTransformer2 = _interopRequireDefault(_AccountTransformer);\n\nvar _mutationTypes = __webpack_require__(/*! ./mutation-types */ \"./ClientApp/src/store/modules/account/mutation-types.js\");\n\nvar types = _interopRequireWildcard(_mutationTypes);\n\nvar _AccountProxy = __webpack_require__(/*! @/proxies/AccountProxy */ \"./ClientApp/src/proxies/AccountProxy.js\");\n\nvar _AccountProxy2 = _interopRequireDefault(_AccountProxy);\n\nfunction _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nvar find = exports.find = function find(_ref) {\n  var commit = _ref.commit;\n\n  new _AccountProxy2.default().find('get').then(function (response) {\n    commit(types.FIND, _AccountTransformer2.default.fetch(response.message));\n  }).catch(function () {\n    console.log('Request failed...');\n  });\n}; /* ============\n    * Actions for the account module\n    * ============\n    *\n    * The actions that are available on the\n    * account module.\n    */\n\nexports.default = {\n  find: find\n};\n\n//# sourceURL=webpack:///./ClientApp/src/store/modules/account/actions.js?");

/***/ }),

/***/ "./ClientApp/src/store/modules/account/getters.js":
/*!********************************************************!*\
  !*** ./ClientApp/src/store/modules/account/getters.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\n/* ============\n * Getters for the account module\n * ============\n *\n * The getters that are available on the\n * account module.\n */\n\nexports.default = {};\n\n//# sourceURL=webpack:///./ClientApp/src/store/modules/account/getters.js?");

/***/ }),

/***/ "./ClientApp/src/store/modules/account/index.js":
/*!******************************************************!*\
  !*** ./ClientApp/src/store/modules/account/index.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\n\nvar _actions = __webpack_require__(/*! ./actions */ \"./ClientApp/src/store/modules/account/actions.js\");\n\nvar _actions2 = _interopRequireDefault(_actions);\n\nvar _getters = __webpack_require__(/*! ./getters */ \"./ClientApp/src/store/modules/account/getters.js\");\n\nvar _getters2 = _interopRequireDefault(_getters);\n\nvar _mutations = __webpack_require__(/*! ./mutations */ \"./ClientApp/src/store/modules/account/mutations.js\");\n\nvar _mutations2 = _interopRequireDefault(_mutations);\n\nvar _state = __webpack_require__(/*! ./state */ \"./ClientApp/src/store/modules/account/state.js\");\n\nvar _state2 = _interopRequireDefault(_state);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n/* ============\n * Account Module\n * ============\n */\n\nexports.default = {\n  namespaced: true,\n  actions: _actions2.default,\n  getters: _getters2.default,\n  mutations: _mutations2.default,\n  state: _state2.default\n};\n\n//# sourceURL=webpack:///./ClientApp/src/store/modules/account/index.js?");

/***/ }),

/***/ "./ClientApp/src/store/modules/account/mutation-types.js":
/*!***************************************************************!*\
  !*** ./ClientApp/src/store/modules/account/mutation-types.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\n/* ============\n * Mutation types for the account module\n * ============\n *\n * The mutation types that are available\n * on the account module.\n */\n\nvar FIND = exports.FIND = 'FIND';\n\nexports.default = {\n  FIND: FIND\n};\n\n//# sourceURL=webpack:///./ClientApp/src/store/modules/account/mutation-types.js?");

/***/ }),

/***/ "./ClientApp/src/store/modules/account/mutations.js":
/*!**********************************************************!*\
  !*** ./ClientApp/src/store/modules/account/mutations.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\n\nvar _mutationTypes = __webpack_require__(/*! ./mutation-types */ \"./ClientApp/src/store/modules/account/mutation-types.js\");\n\nfunction _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; } /* ============\n                                                                                                                                                                                                                   * Mutations for the account module\n                                                                                                                                                                                                                   * ============\n                                                                                                                                                                                                                   *\n                                                                                                                                                                                                                   * The mutations that are available on the\n                                                                                                                                                                                                                   * account module.\n                                                                                                                                                                                                                   */\n\nexports.default = _defineProperty({}, _mutationTypes.FIND, function (state, account) {\n  state.email = account.email;\n  state.firstName = account.firstName;\n  state.lastName = account.lastName;\n  state.photoUrl = account.photoUrl;\n});\n\n//# sourceURL=webpack:///./ClientApp/src/store/modules/account/mutations.js?");

/***/ }),

/***/ "./ClientApp/src/store/modules/account/state.js":
/*!******************************************************!*\
  !*** ./ClientApp/src/store/modules/account/state.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\n/* ============\n * State of the account module\n * ============\n *\n * The initial state of the account module.\n */\n\nexports.default = {\n  email: null,\n  firstName: null,\n  lastName: null,\n  photoUrl: null\n};\n\n//# sourceURL=webpack:///./ClientApp/src/store/modules/account/state.js?");

/***/ }),

/***/ "./ClientApp/src/store/modules/alert/actions.js":
/*!******************************************************!*\
  !*** ./ClientApp/src/store/modules/alert/actions.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\nexports.message = undefined;\n\nvar _mutationTypes = __webpack_require__(/*! ./mutation-types */ \"./ClientApp/src/store/modules/alert/mutation-types.js\");\n\nvar types = _interopRequireWildcard(_mutationTypes);\n\nfunction _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }\n\nvar message = exports.message = function message(_ref, payload) {\n    var commit = _ref.commit;\n\n    commit(types.MESSAGE, payload);\n}; /* ============\r\n    * Actions for the auth module\r\n    * ============\r\n    *\r\n    * The actions that are available on the\r\n    * auth module.\r\n    */\n\nexports.default = {\n    message: message\n};\n\n//# sourceURL=webpack:///./ClientApp/src/store/modules/alert/actions.js?");

/***/ }),

/***/ "./ClientApp/src/store/modules/alert/getters.js":
/*!******************************************************!*\
  !*** ./ClientApp/src/store/modules/alert/getters.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\n/* ============\n * Getters for the auth module\n * ============\n *\n * The getters that are available on the\n * auth module.\n */\n\nexports.default = {};\n\n//# sourceURL=webpack:///./ClientApp/src/store/modules/alert/getters.js?");

/***/ }),

/***/ "./ClientApp/src/store/modules/alert/index.js":
/*!****************************************************!*\
  !*** ./ClientApp/src/store/modules/alert/index.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\n\nvar _actions = __webpack_require__(/*! ./actions */ \"./ClientApp/src/store/modules/alert/actions.js\");\n\nvar _actions2 = _interopRequireDefault(_actions);\n\nvar _getters = __webpack_require__(/*! ./getters */ \"./ClientApp/src/store/modules/alert/getters.js\");\n\nvar _getters2 = _interopRequireDefault(_getters);\n\nvar _mutations = __webpack_require__(/*! ./mutations */ \"./ClientApp/src/store/modules/alert/mutations.js\");\n\nvar _mutations2 = _interopRequireDefault(_mutations);\n\nvar _state = __webpack_require__(/*! ./state */ \"./ClientApp/src/store/modules/alert/state.js\");\n\nvar _state2 = _interopRequireDefault(_state);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n/* ============\r\n * Auth Module\r\n * ============\r\n */\n\nexports.default = {\n    namespaced: true,\n    actions: _actions2.default,\n    getters: _getters2.default,\n    mutations: _mutations2.default,\n    state: _state2.default\n};\n\n//# sourceURL=webpack:///./ClientApp/src/store/modules/alert/index.js?");

/***/ }),

/***/ "./ClientApp/src/store/modules/alert/mutation-types.js":
/*!*************************************************************!*\
  !*** ./ClientApp/src/store/modules/alert/mutation-types.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\n/* ============\n * Mutation types for the account module\n * ============\n *\n * The mutation types that are available\n * on the auth module.\n */\n\nvar MESSAGE = exports.MESSAGE = 'MESSAGE';\n\nexports.default = {\n  MESSAGE: MESSAGE\n};\n\n//# sourceURL=webpack:///./ClientApp/src/store/modules/alert/mutation-types.js?");

/***/ }),

/***/ "./ClientApp/src/store/modules/alert/mutations.js":
/*!********************************************************!*\
  !*** ./ClientApp/src/store/modules/alert/mutations.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\n\nvar _mutationTypes = __webpack_require__(/*! ./mutation-types */ \"./ClientApp/src/store/modules/alert/mutation-types.js\");\n\nfunction _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; } /* ============\n                                                                                                                                                                                                                   * Mutations for the auth module\n                                                                                                                                                                                                                   * ============\n                                                                                                                                                                                                                   *\n                                                                                                                                                                                                                   * The mutations that are available on the\n                                                                                                                                                                                                                   * account module.\n                                                                                                                                                                                                                   */\n\nexports.default = _defineProperty({}, _mutationTypes.MESSAGE, function (state, payload) {\n    state.message = payload.message;\n    state.success = payload.success;\n    state.status = payload.status;\n});\n\n//# sourceURL=webpack:///./ClientApp/src/store/modules/alert/mutations.js?");

/***/ }),

/***/ "./ClientApp/src/store/modules/alert/state.js":
/*!****************************************************!*\
  !*** ./ClientApp/src/store/modules/alert/state.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\n/* ============\n * State of the auth module\n * ============\n *\n * The initial state of the auth module.\n */\n\nexports.default = {\n  status: false,\n  success: null,\n  message: null\n};\n\n//# sourceURL=webpack:///./ClientApp/src/store/modules/alert/state.js?");

/***/ }),

/***/ "./ClientApp/src/store/modules/auth/actions.js":
/*!*****************************************************!*\
  !*** ./ClientApp/src/store/modules/auth/actions.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\nexports.logout = exports.recover = exports.activate = exports.login = exports.check = undefined;\n\nvar _vue = __webpack_require__(/*! vue */ \"./node_modules/vue/dist/vue.esm.js\");\n\nvar _vue2 = _interopRequireDefault(_vue);\n\nvar _index = __webpack_require__(/*! @/store/index */ \"./ClientApp/src/store/index.js\");\n\nvar _index2 = _interopRequireDefault(_index);\n\nvar _mutationTypes = __webpack_require__(/*! ./mutation-types */ \"./ClientApp/src/store/modules/auth/mutation-types.js\");\n\nvar types = _interopRequireWildcard(_mutationTypes);\n\nvar _AuthProxy = __webpack_require__(/*! @/proxies/AuthProxy */ \"./ClientApp/src/proxies/AuthProxy.js\");\n\nvar _AuthProxy2 = _interopRequireDefault(_AuthProxy);\n\nfunction _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n// import service from '@/services/index'\n\n/* ============\r\n * Actions for the auth module\r\n * ============\r\n *\r\n * The actions that are available on the\r\n * auth module.\r\n */\n\nvar check = exports.check = function check(_ref) {\n    var commit = _ref.commit;\n\n    commit(types.CHECK);\n};\n\nvar login = exports.login = function login(_ref2, payload) {\n    var commit = _ref2.commit;\n\n    new _AuthProxy2.default().login(payload).then(function (response) {\n        if (response.success) {\n            _vue2.default.router.push({\n                name: 'verifyCode',\n                query: { redirectUrl: response.data.redirectUrl }\n            });\n        } else {\n            _index2.default.dispatch('alert/message', { message: response.message, status: true, success: response.success });\n        }\n    }).catch(function (error) {\n        console.log('Request failed...' + error);\n    });\n};\n\nvar activate = exports.activate = function activate(_ref3, payload) {\n    var commit = _ref3.commit;\n\n    new _AuthProxy2.default().setParameter('t', payload).find('activate').then(function (response) {\n        if (response.success) {\n            commit(types.LOGIN, response.message);\n            // store.dispatch('account/find')\n            _vue2.default.router.push({\n                name: 'home.index'\n            });\n        } else {\n            _index2.default.dispatch('alert/message', { message: response.error, status: true, success: response.success });\n        }\n    }).catch(function (err) {\n        console.log(err);\n    });\n\n    /* Vue.router.push({\r\n      name: 'home.index'\r\n    }) */\n};\n\nvar recover = exports.recover = function recover(_ref4, payload) {\n    var commit = _ref4.commit;\n\n    new _AuthProxy2.default().setParameter('token', payload).find('recover').then(function (response) {\n        if (response.field !== 'Success') {\n            _index2.default.dispatch('alert/message', { message: response.error, status: true, success: response.success });\n        }\n        // commit(types.LOGIN, response)\n        // store.dispatch('account/find')\n        /* Vue.router.push({\r\n          name: 'home.index'\r\n        }) */\n    }).catch(function () {});\n\n    /* Vue.router.push({\r\n      name: 'home.index'\r\n    }) */\n};\n\nvar logout = exports.logout = function logout(_ref5) {\n    var commit = _ref5.commit;\n\n    commit(types.LOGOUT);\n    _vue2.default.router.push({\n        name: 'login.index'\n    });\n};\n\nexports.default = {\n    check: check,\n    login: login,\n    activate: activate,\n    recover: recover,\n    logout: logout\n};\n\n//# sourceURL=webpack:///./ClientApp/src/store/modules/auth/actions.js?");

/***/ }),

/***/ "./ClientApp/src/store/modules/auth/getters.js":
/*!*****************************************************!*\
  !*** ./ClientApp/src/store/modules/auth/getters.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\n/* ============\r\n * Getters for the auth module\r\n * ============\r\n *\r\n * The getters that are available on the\r\n * auth module.\r\n */\n\nexports.default = {};\n\n//# sourceURL=webpack:///./ClientApp/src/store/modules/auth/getters.js?");

/***/ }),

/***/ "./ClientApp/src/store/modules/auth/index.js":
/*!***************************************************!*\
  !*** ./ClientApp/src/store/modules/auth/index.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\n\nvar _actions = __webpack_require__(/*! ./actions */ \"./ClientApp/src/store/modules/auth/actions.js\");\n\nvar _actions2 = _interopRequireDefault(_actions);\n\nvar _getters = __webpack_require__(/*! ./getters */ \"./ClientApp/src/store/modules/auth/getters.js\");\n\nvar _getters2 = _interopRequireDefault(_getters);\n\nvar _mutations = __webpack_require__(/*! ./mutations */ \"./ClientApp/src/store/modules/auth/mutations.js\");\n\nvar _mutations2 = _interopRequireDefault(_mutations);\n\nvar _state = __webpack_require__(/*! ./state */ \"./ClientApp/src/store/modules/auth/state.js\");\n\nvar _state2 = _interopRequireDefault(_state);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n/* ============\r\n * Auth Module\r\n * ============\r\n */\n\nexports.default = {\n    namespaced: true,\n    actions: _actions2.default,\n    getters: _getters2.default,\n    mutations: _mutations2.default,\n    state: _state2.default\n};\n\n//# sourceURL=webpack:///./ClientApp/src/store/modules/auth/index.js?");

/***/ }),

/***/ "./ClientApp/src/store/modules/auth/mutation-types.js":
/*!************************************************************!*\
  !*** ./ClientApp/src/store/modules/auth/mutation-types.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\n/* ============\r\n * Mutation types for the account module\r\n * ============\r\n *\r\n * The mutation types that are available\r\n * on the auth module.\r\n */\n\nvar CHECK = exports.CHECK = 'CHECK';\nvar REGISTER = exports.REGISTER = 'REGISTER';\nvar LOGIN = exports.LOGIN = 'LOGIN';\nvar LOGOUT = exports.LOGOUT = 'LOGOUT';\n\nexports.default = {\n  CHECK: CHECK,\n  REGISTER: REGISTER,\n  LOGIN: LOGIN,\n  LOGOUT: LOGOUT\n};\n\n//# sourceURL=webpack:///./ClientApp/src/store/modules/auth/mutation-types.js?");

/***/ }),

/***/ "./ClientApp/src/store/modules/auth/mutations.js":
/*!*******************************************************!*\
  !*** ./ClientApp/src/store/modules/auth/mutations.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\n\nvar _CHECK$LOGIN$LOGOUT;\n\nvar _axios = __webpack_require__(/*! axios */ \"./node_modules/axios/index.js\");\n\nvar _axios2 = _interopRequireDefault(_axios);\n\nvar _mutationTypes = __webpack_require__(/*! ./mutation-types */ \"./ClientApp/src/store/modules/auth/mutation-types.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nfunction _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; } /* ============\r\n                                                                                                                                                                                                                   * Mutations for the auth module\r\n                                                                                                                                                                                                                   * ============\r\n                                                                                                                                                                                                                   *\r\n                                                                                                                                                                                                                   * The mutations that are available on the\r\n                                                                                                                                                                                                                   * account module.\r\n                                                                                                                                                                                                                   */\n\n// import storage from 'local-storage-fallback'\n\n\nexports.default = (_CHECK$LOGIN$LOGOUT = {}, _defineProperty(_CHECK$LOGIN$LOGOUT, _mutationTypes.CHECK, function (state) {\n    state.authenticated = !!storage.getItem('AUTH-ID');\n    if (state.authenticated) {\n        // Axios.defaults.headers.common['Authorization'] = `Bearer ${storage.getItem('AUTH-ID')}`\n    }\n}), _defineProperty(_CHECK$LOGIN$LOGOUT, _mutationTypes.LOGIN, function (state, payload) {\n    state.authenticated = true;\n    storage.setItem('AUTH-ID', payload.message);\n    _axios2.default.defaults.headers.common['Authorization'] = 'Bearer ' + payload.message;\n}), _defineProperty(_CHECK$LOGIN$LOGOUT, _mutationTypes.LOGOUT, function (state) {\n    state.authenticated = false;\n    storage.removeItem('AUTH-ID');\n    _axios2.default.defaults.headers.common['Authorization'] = '';\n}), _CHECK$LOGIN$LOGOUT);\n\n//# sourceURL=webpack:///./ClientApp/src/store/modules/auth/mutations.js?");

/***/ }),

/***/ "./ClientApp/src/store/modules/auth/state.js":
/*!***************************************************!*\
  !*** ./ClientApp/src/store/modules/auth/state.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\n/* ============\r\n * State of the auth module\r\n * ============\r\n *\r\n * The initial state of the auth module.\r\n */\n\nexports.default = {\n  authenticated: false\n};\n\n//# sourceURL=webpack:///./ClientApp/src/store/modules/auth/state.js?");

/***/ }),

/***/ "./ClientApp/src/transformers/AccountTransformer.js":
/*!**********************************************************!*\
  !*** ./ClientApp/src/transformers/AccountTransformer.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\n\nvar _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();\n\nvar _Transformer2 = __webpack_require__(/*! ./Transformer */ \"./ClientApp/src/transformers/Transformer.js\");\n\nvar _Transformer3 = _interopRequireDefault(_Transformer2);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError(\"this hasn't been initialised - super() hasn't been called\"); } return call && (typeof call === \"object\" || typeof call === \"function\") ? call : self; }\n\nfunction _inherits(subClass, superClass) { if (typeof superClass !== \"function\" && superClass !== null) { throw new TypeError(\"Super expression must either be null or a function, not \" + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /* ============\n                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Account Transformer\n                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * ============\n                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                *\n                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * The transformer for the account.\n                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */\n\nvar AccountTransformer = function (_Transformer) {\n  _inherits(AccountTransformer, _Transformer);\n\n  function AccountTransformer() {\n    _classCallCheck(this, AccountTransformer);\n\n    return _possibleConstructorReturn(this, (AccountTransformer.__proto__ || Object.getPrototypeOf(AccountTransformer)).apply(this, arguments));\n  }\n\n  _createClass(AccountTransformer, null, [{\n    key: 'fetch',\n\n    /**\n     * Method used to transform a fetched account.\n     *\n     * @param account The fetched account.\n     *\n     * @returns {Object} The transformed account.\n     */\n    value: function fetch(account) {\n      return {\n        email: account.credential,\n        firstName: account.profile_information_id.first_name,\n        lastName: account.profile_information_id.last_name,\n        photoUrl: account.profile_information_id.profile_photo_id === null ? '/static/images/svg/user.svg' : account.profile_information_id.profile_photo_id.url\n      };\n    }\n\n    /**\n     * Method used to transform a send account.\n     *\n     * @param account The account to be send.\n     *\n     * @returns {Object} The transformed account.\n     */\n\n  }, {\n    key: 'send',\n    value: function send(account) {\n      return {\n        email: account.email,\n        first_name: account.firstName,\n        last_name: account.lastName\n      };\n    }\n\n    /**\n     * Method used to transform a send role.\n     *\n     * @param role The role to be send.\n     *\n     * @returns {Object} The transformed role.\n     */\n\n  }, {\n    key: 'sendInformation',\n    value: function sendInformation(account) {\n      var formData = new FormData();\n      formData.append('firstName', account.firstName);\n      formData.append('lastName', account.lastName);\n      formData.append('gender', account.gender);\n      formData.append('birthDate', account.birthDate);\n      return formData;\n    }\n  }]);\n\n  return AccountTransformer;\n}(_Transformer3.default);\n\nexports.default = AccountTransformer;\n\n//# sourceURL=webpack:///./ClientApp/src/transformers/AccountTransformer.js?");

/***/ }),

/***/ "./ClientApp/src/transformers/Transformer.js":
/*!***************************************************!*\
  !*** ./ClientApp/src/transformers/Transformer.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\n\nvar _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\n/* ============\n * Transformer\n * ============\n *\n * The base transformer.\n *\n * Transformers are used to transform the fetched data\n * to a more suitable format.\n * For instance, when the fetched data contains snake_cased values,\n * they will be camelCased.\n */\n\nvar Transformer = function () {\n  function Transformer() {\n    _classCallCheck(this, Transformer);\n  }\n\n  _createClass(Transformer, null, [{\n    key: \"fetchCollection\",\n\n    /**\n     * Method used to transform a fetched collection.\n     *\n     * @param items The items to be transformed.\n     *\n     * @returns {Array} The transformed items.\n     */\n    value: function fetchCollection(items) {\n      var _this = this;\n\n      return items.map(function (item) {\n        return _this.fetch(item);\n      });\n    }\n\n    /**\n     * Method used to transform a collection to be send.\n     *\n     * @param items The items to be transformed.\n     *\n     * @returns {Array} The transformed items.\n     */\n\n  }, {\n    key: \"sendCollection\",\n    value: function sendCollection(items) {\n      var _this2 = this;\n\n      return items.map(function (item) {\n        return _this2.send(item);\n      });\n    }\n  }]);\n\n  return Transformer;\n}();\n\nexports.default = Transformer;\n\n//# sourceURL=webpack:///./ClientApp/src/transformers/Transformer.js?");

/***/ }),

/***/ "./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/index.js!./ClientApp/public/styles/application-42da8c9352a73650a56904b49a08ef420cc42da1834ab33cac8f813933119916.css":
/*!**************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader!./ClientApp/public/styles/application-42da8c9352a73650a56904b49a08ef420cc42da1834ab33cac8f813933119916.css ***!
  \**************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./ClientApp/public/styles/application-42da8c9352a73650a56904b49a08ef420cc42da1834ab33cac8f813933119916.css?./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader");

/***/ }),

/***/ "./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/index.js!./ClientApp/public/styles/breadcrumb.css":
/*!********************************************************************************************************************************!*\
  !*** ./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader!./ClientApp/public/styles/breadcrumb.css ***!
  \********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./ClientApp/public/styles/breadcrumb.css?./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader");

/***/ }),

/***/ "./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/index.js!./ClientApp/public/styles/theme.css":
/*!***************************************************************************************************************************!*\
  !*** ./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader!./ClientApp/public/styles/theme.css ***!
  \***************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./ClientApp/public/styles/theme.css?./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader");

/***/ }),

/***/ "./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/index.js!./ClientApp/public/styles/theme_.css":
/*!****************************************************************************************************************************!*\
  !*** ./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader!./ClientApp/public/styles/theme_.css ***!
  \****************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./ClientApp/public/styles/theme_.css?./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader");

/***/ }),

/***/ "./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/index.js!./ClientApp/public/styles/theme__.css":
/*!*****************************************************************************************************************************!*\
  !*** ./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader!./ClientApp/public/styles/theme__.css ***!
  \*****************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./ClientApp/public/styles/theme__.css?./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader");

/***/ }),

/***/ "./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/index.js!./node_modules/sass-loader/lib/loader.js!./ClientApp/public/styles/app.sass":
/*!*******************************************************************************************************************************************************************!*\
  !*** ./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader!./node_modules/sass-loader/lib/loader.js!./ClientApp/public/styles/app.sass ***!
  \*******************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./ClientApp/public/styles/app.sass?./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader!./node_modules/sass-loader/lib/loader.js");

/***/ }),

/***/ "./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/index.js!./node_modules/sass-loader/lib/loader.js!./ClientApp/public/styles/avatar-group.sass":
/*!****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader!./node_modules/sass-loader/lib/loader.js!./ClientApp/public/styles/avatar-group.sass ***!
  \****************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./ClientApp/public/styles/avatar-group.sass?./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader!./node_modules/sass-loader/lib/loader.js");

/***/ }),

/***/ "./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/index.js!./node_modules/sass-loader/lib/loader.js!./ClientApp/public/styles/bootstrap.scss":
/*!*************************************************************************************************************************************************************************!*\
  !*** ./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader!./node_modules/sass-loader/lib/loader.js!./ClientApp/public/styles/bootstrap.scss ***!
  \*************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./ClientApp/public/styles/bootstrap.scss?./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader!./node_modules/sass-loader/lib/loader.js");

/***/ }),

/***/ "./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/index.js!./node_modules/sass-loader/lib/loader.js!./ClientApp/public/styles/bracket.sass":
/*!***********************************************************************************************************************************************************************!*\
  !*** ./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader!./node_modules/sass-loader/lib/loader.js!./ClientApp/public/styles/bracket.sass ***!
  \***********************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./ClientApp/public/styles/bracket.sass?./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader!./node_modules/sass-loader/lib/loader.js");

/***/ }),

/***/ "./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/index.js!./node_modules/sass-loader/lib/loader.js!./ClientApp/public/styles/custom.sass":
/*!**********************************************************************************************************************************************************************!*\
  !*** ./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader!./node_modules/sass-loader/lib/loader.js!./ClientApp/public/styles/custom.sass ***!
  \**********************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./ClientApp/public/styles/custom.sass?./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader!./node_modules/sass-loader/lib/loader.js");

/***/ }),

/***/ "./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/index.js!./node_modules/sass-loader/lib/loader.js!./ClientApp/public/styles/multiselect.sass":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader!./node_modules/sass-loader/lib/loader.js!./ClientApp/public/styles/multiselect.sass ***!
  \***************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./ClientApp/public/styles/multiselect.sass?./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader!./node_modules/sass-loader/lib/loader.js");

/***/ }),

/***/ "./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/index.js!./node_modules/sass-loader/lib/loader.js!./ClientApp/public/styles/placeholder.sass":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader!./node_modules/sass-loader/lib/loader.js!./ClientApp/public/styles/placeholder.sass ***!
  \***************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./ClientApp/public/styles/placeholder.sass?./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader!./node_modules/sass-loader/lib/loader.js");

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/index.js!./ClientApp/public/styles/application-42da8c9352a73650a56904b49a08ef420cc42da1834ab33cac8f813933119916.css":
/*!******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader!./ClientApp/public/styles/application-42da8c9352a73650a56904b49a08ef420cc42da1834ab33cac8f813933119916.css ***!
  \******************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("\nvar content = __webpack_require__(/*! !../../../node_modules/mini-css-extract-plugin/dist/loader.js!../../../node_modules/css-loader!./application-42da8c9352a73650a56904b49a08ef420cc42da1834ab33cac8f813933119916.css */ \"./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/index.js!./ClientApp/public/styles/application-42da8c9352a73650a56904b49a08ef420cc42da1834ab33cac8f813933119916.css\");\n\nif(typeof content === 'string') content = [[module.i, content, '']];\n\nvar transform;\nvar insertInto;\n\n\n\nvar options = {\"hmr\":true}\n\noptions.transform = transform\noptions.insertInto = undefined;\n\nvar update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ \"./node_modules/style-loader/lib/addStyles.js\")(content, options);\n\nif(content.locals) module.exports = content.locals;\n\nif(false) {}\n\n//# sourceURL=webpack:///./ClientApp/public/styles/application-42da8c9352a73650a56904b49a08ef420cc42da1834ab33cac8f813933119916.css?./node_modules/style-loader!./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader");

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/index.js!./ClientApp/public/styles/breadcrumb.css":
/*!************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader!./ClientApp/public/styles/breadcrumb.css ***!
  \************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("\nvar content = __webpack_require__(/*! !../../../node_modules/mini-css-extract-plugin/dist/loader.js!../../../node_modules/css-loader!./breadcrumb.css */ \"./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/index.js!./ClientApp/public/styles/breadcrumb.css\");\n\nif(typeof content === 'string') content = [[module.i, content, '']];\n\nvar transform;\nvar insertInto;\n\n\n\nvar options = {\"hmr\":true}\n\noptions.transform = transform\noptions.insertInto = undefined;\n\nvar update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ \"./node_modules/style-loader/lib/addStyles.js\")(content, options);\n\nif(content.locals) module.exports = content.locals;\n\nif(false) {}\n\n//# sourceURL=webpack:///./ClientApp/public/styles/breadcrumb.css?./node_modules/style-loader!./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader");

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/index.js!./ClientApp/public/styles/theme.css":
/*!*******************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader!./ClientApp/public/styles/theme.css ***!
  \*******************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("\nvar content = __webpack_require__(/*! !../../../node_modules/mini-css-extract-plugin/dist/loader.js!../../../node_modules/css-loader!./theme.css */ \"./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/index.js!./ClientApp/public/styles/theme.css\");\n\nif(typeof content === 'string') content = [[module.i, content, '']];\n\nvar transform;\nvar insertInto;\n\n\n\nvar options = {\"hmr\":true}\n\noptions.transform = transform\noptions.insertInto = undefined;\n\nvar update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ \"./node_modules/style-loader/lib/addStyles.js\")(content, options);\n\nif(content.locals) module.exports = content.locals;\n\nif(false) {}\n\n//# sourceURL=webpack:///./ClientApp/public/styles/theme.css?./node_modules/style-loader!./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader");

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/index.js!./ClientApp/public/styles/theme_.css":
/*!********************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader!./ClientApp/public/styles/theme_.css ***!
  \********************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("\nvar content = __webpack_require__(/*! !../../../node_modules/mini-css-extract-plugin/dist/loader.js!../../../node_modules/css-loader!./theme_.css */ \"./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/index.js!./ClientApp/public/styles/theme_.css\");\n\nif(typeof content === 'string') content = [[module.i, content, '']];\n\nvar transform;\nvar insertInto;\n\n\n\nvar options = {\"hmr\":true}\n\noptions.transform = transform\noptions.insertInto = undefined;\n\nvar update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ \"./node_modules/style-loader/lib/addStyles.js\")(content, options);\n\nif(content.locals) module.exports = content.locals;\n\nif(false) {}\n\n//# sourceURL=webpack:///./ClientApp/public/styles/theme_.css?./node_modules/style-loader!./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader");

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/index.js!./ClientApp/public/styles/theme__.css":
/*!*********************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader!./ClientApp/public/styles/theme__.css ***!
  \*********************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("\nvar content = __webpack_require__(/*! !../../../node_modules/mini-css-extract-plugin/dist/loader.js!../../../node_modules/css-loader!./theme__.css */ \"./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/index.js!./ClientApp/public/styles/theme__.css\");\n\nif(typeof content === 'string') content = [[module.i, content, '']];\n\nvar transform;\nvar insertInto;\n\n\n\nvar options = {\"hmr\":true}\n\noptions.transform = transform\noptions.insertInto = undefined;\n\nvar update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ \"./node_modules/style-loader/lib/addStyles.js\")(content, options);\n\nif(content.locals) module.exports = content.locals;\n\nif(false) {}\n\n//# sourceURL=webpack:///./ClientApp/public/styles/theme__.css?./node_modules/style-loader!./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader");

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./ClientApp/src/App.vue?vue&type=template&id=fcf2c342&":
/*!********************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./ClientApp/src/App.vue?vue&type=template&id=fcf2c342& ***!
  \********************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"render\", function() { return render; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"staticRenderFns\", function() { return staticRenderFns; });\nvar render = function() {\n  var _vm = this\n  var _h = _vm.$createElement\n  var _c = _vm._self._c || _h\n  return _c(\"div\", { attrs: { id: \"app\" } }, [_c(\"router-view\")], 1)\n}\nvar staticRenderFns = []\nrender._withStripped = true\n\n\n\n//# sourceURL=webpack:///./ClientApp/src/App.vue?./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options");

/***/ }),

/***/ "./node_modules/webpack/hot sync ^\\.\\/log$":
/*!*************************************************!*\
  !*** (webpack)/hot sync nonrecursive ^\.\/log$ ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var map = {\n\t\"./log\": \"./node_modules/webpack/hot/log.js\"\n};\n\n\nfunction webpackContext(req) {\n\tvar id = webpackContextResolve(req);\n\treturn __webpack_require__(id);\n}\nfunction webpackContextResolve(req) {\n\tif(!__webpack_require__.o(map, req)) {\n\t\tvar e = new Error(\"Cannot find module '\" + req + \"'\");\n\t\te.code = 'MODULE_NOT_FOUND';\n\t\tthrow e;\n\t}\n\treturn map[req];\n}\nwebpackContext.keys = function webpackContextKeys() {\n\treturn Object.keys(map);\n};\nwebpackContext.resolve = webpackContextResolve;\nmodule.exports = webpackContext;\nwebpackContext.id = \"./node_modules/webpack/hot sync ^\\\\.\\\\/log$\";\n\n//# sourceURL=webpack:///(webpack)/hot_sync_nonrecursive_^\\.\\/log$?");

/***/ }),

/***/ 0:
/*!******************************************************************************************!*\
  !*** multi (webpack)-dev-server/client?http://0.0.0.0:8080 ./ClientApp/src/entry-client ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("__webpack_require__(/*! C:\\Users\\ymobiot\\source\\repos\\Proov_real\\Proov\\node_modules\\webpack-dev-server\\client\\index.js?http://0.0.0.0:8080 */\"./node_modules/webpack-dev-server/client/index.js?http://0.0.0.0:8080\");\nmodule.exports = __webpack_require__(/*! C:\\Users\\ymobiot\\source\\repos\\Proov_real\\Proov\\ClientApp\\src\\entry-client */\"./ClientApp/src/entry-client.js\");\n\n\n//# sourceURL=webpack:///multi_(webpack)-dev-server/client?");

/***/ })

/******/ });