(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[15],{

/***/ "./ClientApp/src/services/errorMessage.js":
/*!************************************************!*\
  !*** ./ClientApp/src/services/errorMessage.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\n\nexports.default = function (payload) {\n    /*\r\n     * We change the value of the errors messages\r\n     */\n    if (Array.isArray(payload.response.data)) {\n        var _loop = function _loop(i) {\n            var item = payload.object.form[i];\n            payload.response.data.some(function (e) {\n                var status = item.classList.contains('group_' + e.propertyName);\n                if (status) {\n                    item.parentElement.classList.add('has-error');\n                    item.parentElement.querySelector('span.error').innerHTML = e.errorMessage;\n                } else {\n                    item.parentElement.classList.remove('has-error');\n                    item.parentElement.querySelector('span.error') == null ? '' : item.parentElement.querySelector('span.error').innerHTML = '';\n                }\n                return status;\n            });\n        };\n\n        for (var i = 0; i < payload.object.form.length; i++) {\n            _loop(i);\n        }\n    }\n\n    if (!Array.isArray(payload.response.data)) {\n        for (var i = 0; i < payload.object.form.length; i++) {\n            var _item = payload.object.form[i];\n            _item.parentElement.classList.remove('has-error');\n            _item.parentElement.querySelector('span.error') == null ? '' : _item.parentElement.querySelector('span.error').innerHTML = '';\n        }\n    }\n};\n\n//# sourceURL=webpack:///./ClientApp/src/services/errorMessage.js?");

/***/ }),

/***/ "./ClientApp/src/services/index.js":
/*!*****************************************!*\
  !*** ./ClientApp/src/services/index.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\n\nvar _errorMessage = __webpack_require__(/*! ./errorMessage */ \"./ClientApp/src/services/errorMessage.js\");\n\nvar _errorMessage2 = _interopRequireDefault(_errorMessage);\n\nvar _removeMessage = __webpack_require__(/*! ./removeMessage */ \"./ClientApp/src/services/removeMessage.js\");\n\nvar _removeMessage2 = _interopRequireDefault(_removeMessage);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nexports.default = {\n    errorMessage: _errorMessage2.default,\n    removeMessage: _removeMessage2.default\n};\n\n//# sourceURL=webpack:///./ClientApp/src/services/index.js?");

/***/ }),

/***/ "./ClientApp/src/services/removeMessage.js":
/*!*************************************************!*\
  !*** ./ClientApp/src/services/removeMessage.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\n\nexports.default = function () {\n    /*\n     * We change the value of the errors messages\n     */\n\n    var $form = document.querySelector('form');\n    for (var i = 0; i < $form.querySelectorAll('div.js-form-group').length; i++) {\n        var item = $form.querySelectorAll('div.js-form-group')[i];\n        item.classList.remove('has-error');\n        console.log(item.querySelector('span.error').innerHTML = '');\n    }\n};\n\n//# sourceURL=webpack:///./ClientApp/src/services/removeMessage.js?");

/***/ }),

/***/ "./ClientApp/src/views/Auth/VerifyCode.vue":
/*!*************************************************!*\
  !*** ./ClientApp/src/views/Auth/VerifyCode.vue ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _VerifyCode_vue_vue_type_template_id_5d7e388a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./VerifyCode.vue?vue&type=template&id=5d7e388a& */ \"./ClientApp/src/views/Auth/VerifyCode.vue?vue&type=template&id=5d7e388a&\");\n/* harmony import */ var _VerifyCode_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./VerifyCode.vue?vue&type=script&lang=js& */ \"./ClientApp/src/views/Auth/VerifyCode.vue?vue&type=script&lang=js&\");\n/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _VerifyCode_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _VerifyCode_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[key]; }) }(__WEBPACK_IMPORT_KEY__));\n/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ \"./node_modules/vue-loader/lib/runtime/componentNormalizer.js\");\n\n\n\n\n\n/* normalize component */\n\nvar component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__[\"default\"])(\n  _VerifyCode_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[\"default\"],\n  _VerifyCode_vue_vue_type_template_id_5d7e388a___WEBPACK_IMPORTED_MODULE_0__[\"render\"],\n  _VerifyCode_vue_vue_type_template_id_5d7e388a___WEBPACK_IMPORTED_MODULE_0__[\"staticRenderFns\"],\n  false,\n  null,\n  null,\n  null\n  \n)\n\n/* hot reload */\nif (false) { var api; }\ncomponent.options.__file = \"ClientApp/src/views/Auth/VerifyCode.vue\"\n/* harmony default export */ __webpack_exports__[\"default\"] = (component.exports);\n\n//# sourceURL=webpack:///./ClientApp/src/views/Auth/VerifyCode.vue?");

/***/ }),

/***/ "./ClientApp/src/views/Auth/VerifyCode.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ./ClientApp/src/views/Auth/VerifyCode.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_VerifyCode_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib!../../../../node_modules/vue-loader/lib??vue-loader-options!./VerifyCode.vue?vue&type=script&lang=js& */ \"./node_modules/babel-loader/lib/index.js!./node_modules/vue-loader/lib/index.js?!./ClientApp/src/views/Auth/VerifyCode.vue?vue&type=script&lang=js&\");\n/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_VerifyCode_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_VerifyCode_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__);\n/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_VerifyCode_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_VerifyCode_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));\n /* harmony default export */ __webpack_exports__[\"default\"] = (_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_VerifyCode_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default.a); \n\n//# sourceURL=webpack:///./ClientApp/src/views/Auth/VerifyCode.vue?");

/***/ }),

/***/ "./ClientApp/src/views/Auth/VerifyCode.vue?vue&type=template&id=5d7e388a&":
/*!********************************************************************************!*\
  !*** ./ClientApp/src/views/Auth/VerifyCode.vue?vue&type=template&id=5d7e388a& ***!
  \********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_VerifyCode_vue_vue_type_template_id_5d7e388a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./VerifyCode.vue?vue&type=template&id=5d7e388a& */ \"./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./ClientApp/src/views/Auth/VerifyCode.vue?vue&type=template&id=5d7e388a&\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"render\", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_VerifyCode_vue_vue_type_template_id_5d7e388a___WEBPACK_IMPORTED_MODULE_0__[\"render\"]; });\n\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"staticRenderFns\", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_VerifyCode_vue_vue_type_template_id_5d7e388a___WEBPACK_IMPORTED_MODULE_0__[\"staticRenderFns\"]; });\n\n\n\n//# sourceURL=webpack:///./ClientApp/src/views/Auth/VerifyCode.vue?");

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/vue-loader/lib/index.js?!./ClientApp/src/views/Auth/VerifyCode.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./ClientApp/src/views/Auth/VerifyCode.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\n\nvar _Default = __webpack_require__(/*! @/layouts/Default.vue */ \"./ClientApp/src/layouts/Default.vue\");\n\nvar _Default2 = _interopRequireDefault(_Default);\n\nvar _index = __webpack_require__(/*! @/services/index */ \"./ClientApp/src/services/index.js\");\n\nvar _index2 = _interopRequireDefault(_index);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n\n// @ is an alias to /src\nexports.default = {\n    /**\n    * The name of the page.\n    */\n    name: 'verifyCode',\n\n    /**\n    * The data that can be used by the page.\n    *\n    * @returns {Object} The view-model data.\n    */\n    data: function data() {\n        return {\n            isLoading: false,\n            model: {\n                verifyCode: ''\n            }\n        };\n    },\n\n\n    /**\n    * The methods the page can use.\n    */\n    methods: {\n        /**\n        * Will register the user.\n        *\n        * @param {Object} model The user to be registered.\n        */\n        submit: function submit(model) {\n            var _this = this;\n\n            this.isLoading = true;\n            this.$http.post('/auth/verify-code', model).then(function (response) {\n                _index2.default.errorMessage({ object: _this.$refs, response: response.data });\n                if (!response.data.success) {\n                    if (!Array.isArray(response.data.data)) {\n                        _this.$store.dispatch('alert/message', { message: response.data.data.message, status: true, success: response.data.data.success });\n                    }\n                } else {\n                    window.location.replace(_this.$route.query.redirectUrl);\n                }\n                _this.isLoading = false;\n            }, function (response) {\n                // get failed\n                console.log(response);\n                _this.isLoading = false;\n            });\n        }\n    },\n\n    computed: {\n        getMessage: function getMessage() {\n            var message = this.$store.state.alert;\n            if (message.status) {\n                this.model = { verifyCode: '' };\n            }\n            return message;\n        }\n    },\n\n    /**\n    * The components that the page can use.\n    */\n    components: {\n        VLayout: _Default2.default\n    }\n};\n\n//# sourceURL=webpack:///./ClientApp/src/views/Auth/VerifyCode.vue?./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options");

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./ClientApp/src/views/Auth/VerifyCode.vue?vue&type=template&id=5d7e388a&":
/*!**************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./ClientApp/src/views/Auth/VerifyCode.vue?vue&type=template&id=5d7e388a& ***!
  \**************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"render\", function() { return render; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"staticRenderFns\", function() { return staticRenderFns; });\nvar render = function() {\n  var _vm = this\n  var _h = _vm.$createElement\n  var _c = _vm._self._c || _h\n  return _c(\"v-layout\", [\n    _c(\n      \"section\",\n      { attrs: { \"aria-label\": \"breadcrumb\", role: \"navigation\" } },\n      [\n        _c(\"div\", { staticClass: \"container-fluid\" }, [\n          _c(\"div\", { staticClass: \"col-md-10\" }, [\n            _c(\"ol\", { staticClass: \"breadcrumb bg-transparent pd-l-5\" }, [\n              _c(\"li\", { staticClass: \"breadcrumb-item\" }, [\n                _c(\"a\", { attrs: { href: \"/\" } }, [_vm._v(\"Home\")])\n              ]),\n              _vm._v(\" \"),\n              _c(\"li\", { staticClass: \"breadcrumb-item\" }, [\n                _c(\"a\", { attrs: { href: \"/auth\" } }, [\n                  _vm._v(\"Authentication\")\n                ])\n              ]),\n              _vm._v(\" \"),\n              _c(\n                \"li\",\n                {\n                  staticClass: \"breadcrumb-item active\",\n                  attrs: { \"aria-current\": \"page\" }\n                },\n                [_vm._v(\"Verify code\")]\n              )\n            ])\n          ])\n        ])\n      ]\n    ),\n    _vm._v(\" \"),\n    _c(\n      \"div\",\n      { staticClass: \"auth pb-5\" },\n      [\n        _c(\"div\", { staticClass: \"container\" }, [\n          _c(\"div\", { staticClass: \"row mb-4\" }, [\n            _c(\"div\", { staticClass: \"col text-center\" }, [\n              _c(\"img\", {\n                attrs: {\n                  alt: \"Image\",\n                  src: \"https://wingman.mediumra.re/assets/img/logo-gray.svg\"\n                }\n              })\n            ])\n          ])\n        ]),\n        _vm._v(\" \"),\n        _c(\"message-alert\", { attrs: { alert: _vm.getMessage } }),\n        _vm._v(\" \"),\n        _c(\n          \"form\",\n          {\n            ref: \"form\",\n            staticClass: \"auth-form\",\n            on: {\n              submit: function($event) {\n                $event.preventDefault()\n                return _vm.submit(_vm.model)\n              }\n            }\n          },\n          [\n            _c(\"div\", { staticClass: \"form-group\" }, [\n              _c(\"div\", { staticClass: \"form-label-group\" }, [\n                _c(\"input\", {\n                  directives: [\n                    {\n                      name: \"model\",\n                      rawName: \"v-model\",\n                      value: _vm.model.verifyCode,\n                      expression: \"model.verifyCode\"\n                    }\n                  ],\n                  staticClass: \"form-control group_VerifyCode\",\n                  attrs: { type: \"text\", placeholder: \"Verify code Sms\" },\n                  domProps: { value: _vm.model.verifyCode },\n                  on: {\n                    input: function($event) {\n                      if ($event.target.composing) {\n                        return\n                      }\n                      _vm.$set(_vm.model, \"verifyCode\", $event.target.value)\n                    }\n                  }\n                }),\n                _vm._v(\" \"),\n                _c(\"label\", [_vm._v(\"Verify sms code\")]),\n                _vm._v(\" \"),\n                _c(\"span\", { staticClass: \"error\" })\n              ])\n            ]),\n            _vm._v(\" \"),\n            _c(\"div\", { staticClass: \"form-group\" }, [\n              _c(\n                \"button\",\n                {\n                  staticClass: \"btn btn-lg btn-danger btn-block\",\n                  class: { loading: _vm.isLoading },\n                  attrs: { type: \"submit\" }\n                },\n                [_vm._v(\"verify sms code\")]\n              )\n            ]),\n            _vm._v(\" \"),\n            _c(\"p\", { staticClass: \"text-muted\" }, [\n              _vm._v(\"\\n           Have an account? \"),\n              _c(\"a\", { attrs: { href: \"/auth/login\" } }, [_vm._v(\"Signin »\")])\n            ])\n          ]\n        )\n      ],\n      1\n    )\n  ])\n}\nvar staticRenderFns = []\nrender._withStripped = true\n\n\n\n//# sourceURL=webpack:///./ClientApp/src/views/Auth/VerifyCode.vue?./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options");

/***/ })

}]);