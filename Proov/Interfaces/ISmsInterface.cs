using System;

namespace Proov.Interfaces
{
    public interface ISmsInterface
    {
        String SendSms(String phoneNumber);
    }
}
