using System;
using Proov.Models;
using System.Collections.Generic;

namespace Proov.Interfaces
{
    public interface ICityInterface
    {
        IEnumerable<City> FindByName(String name);
    }
}
