using System;
using Proov.BindingModels;
using System.Threading.Tasks;

namespace Proov.Interfaces
{
    public interface IEmailInterface
    {
        Task SendEmailAsync(ProfileModelView profileModelView, String subject, String message);
    }
}
