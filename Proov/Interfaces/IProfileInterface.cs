using Proov.Models;
using System;
using System.Threading.Tasks;

namespace Proov.Interfaces
{
    public interface IProfileInterface
    {
        Profile FindByEmail(String email);

        Profile FindByToken(String token);

        Profile TwoFactor(long id, String verifyCode);

        Profile Update(Profile profile);

        Task SaveAsync(Profile profile);
    }
}
