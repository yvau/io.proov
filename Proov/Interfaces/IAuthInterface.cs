using Proov.BindingModels;
using System;

namespace Proov.Interfaces
{
    public interface IAuthInterface
    {
        ProfileModelConnect SignIn(String Email, String Password, Boolean RememberMe);
    }
}
