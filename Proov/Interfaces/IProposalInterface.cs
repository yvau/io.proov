using Proov.Models;
using System.Threading.Tasks;

namespace Proov.Interfaces
{
    public interface IProposalInterface
    {
        Task SaveAsync(Proposal proposal);
    }
}
