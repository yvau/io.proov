using System;
using System.Collections.Generic;
using FluentValidation.Results;
using Newtonsoft.Json.Linq;
using Proov.BindingModels;

namespace Proov.Interfaces
{
    public interface IUtilInterface
    {
        ResponseMessage DisplayErrors(IList<ValidationFailure> Errors);

        long GetId(String resources);

        String SpaData(JToken prerender);
    }
}
