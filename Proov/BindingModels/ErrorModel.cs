using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Proov.BindingModels
{
    public class ErrorModel
    {
        public String PropertyName { get; set; }
        public String ErrorMessage { get; set; }
    }
}
