using System;

namespace Proov.BindingModels
{
    public class ResponseMessage
    {
        public ResponseMessage(Boolean _success, Object _objectData)
        {
            success = _success;
            data = _objectData;
        }

        public Boolean success { get; set; }
        public Object data { get; set; }
    }
}
