using Proov.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Proov.BindingModels
{
    public class ProfileModelConnect
    {
        public ProfileModelConnect(Boolean _status, Profile _profile)
        {
            status = _status;
            profile = _profile;
        }

        public Boolean status { get; set; }
        public Profile profile { get; set; }
    }
}
