using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Proov.BindingModels
{
    public class ProfileModelView
    {
        public int Id { get; set; }
        public string RedirectUrl { get; set; }
        public string VerifyCode { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string AccreditationNumber { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }
        public string Gender { get; set; }
        public string Role { get; set; }
        public string Password { get; set; }
        public string RePassword { get; set; }
        public string PhoneMobile { get; set; }
        public string PhoneOffice { get; set; }
        public string Website { get; set; }
        public string Linkedin { get; set; }
        public string Facebook { get; set; }
        public string Instagram { get; set; }
        public string Twitter { get; set; }
        public bool RememberMe { get; set; }
    }
}
