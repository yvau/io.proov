﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Proov.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "agent",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false),
                    email = table.Column<string>(maxLength: 255, nullable: true),
                    first_name = table.Column<string>(maxLength: 255, nullable: true),
                    last_name = table.Column<string>(maxLength: 255, nullable: true),
                    token = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_agent", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "auto_increment",
                columns: table => new
                {
                    id = table.Column<string>(maxLength: 255, nullable: false),
                    increment_number = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_auto_increment", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "country",
                columns: table => new
                {
                    id = table.Column<string>(maxLength: 255, nullable: false),
                    continent = table.Column<string>(maxLength: 255, nullable: true),
                    currency_code = table.Column<string>(maxLength: 255, nullable: true),
                    currency_name = table.Column<string>(maxLength: 255, nullable: true),
                    languages = table.Column<string>(maxLength: 255, nullable: true),
                    name = table.Column<string>(maxLength: 255, nullable: true),
                    phone_code = table.Column<string>(maxLength: 255, nullable: true),
                    postal_code = table.Column<string>(maxLength: 255, nullable: true),
                    postal_format = table.Column<string>(maxLength: 255, nullable: true),
                    tld = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_country", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "payment",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_payment", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "profile",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false),
                    first_name = table.Column<string>(maxLength: 255, nullable: true),
                    last_name = table.Column<string>(maxLength: 255, nullable: true),
                    email = table.Column<string>(maxLength: 255, nullable: false),
                    token = table.Column<string>(maxLength: 255, nullable: true),
                    date_of_creation_token = table.Column<DateTime>(nullable: false),
                    password = table.Column<string>(maxLength: 255, nullable: false),
                    date_of_creation = table.Column<DateTime>(nullable: true),
                    role = table.Column<string>(maxLength: 255, nullable: false),
                    enabled = table.Column<bool>(nullable: false),
                    ip_address = table.Column<string>(maxLength: 255, nullable: true),
                    phone_landline = table.Column<string>(maxLength: 255, nullable: true),
                    phone = table.Column<string>(maxLength: 255, nullable: true),
                    phone_confirmed = table.Column<bool>(nullable: true),
                    verify_code = table.Column<string>(maxLength: 255, nullable: true),
                    gender = table.Column<string>(maxLength: 255, nullable: true),
                    date_of_birth = table.Column<DateTime>(type: "date", nullable: true),
                    agent_type = table.Column<string>(maxLength: 255, nullable: true),
                    accreditation_number = table.Column<string>(maxLength: 255, nullable: true),
                    website = table.Column<string>(maxLength: 255, nullable: true),
                    linkedin = table.Column<string>(maxLength: 255, nullable: true),
                    facebook = table.Column<string>(maxLength: 255, nullable: true),
                    instagram = table.Column<string>(maxLength: 255, nullable: true),
                    twitter = table.Column<string>(maxLength: 255, nullable: true),
                    language = table.Column<string>(maxLength: 255, nullable: true),
                    communication_channel = table.Column<string>(maxLength: 45, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_profile", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "province",
                columns: table => new
                {
                    id = table.Column<string>(maxLength: 255, nullable: false),
                    name = table.Column<string>(maxLength: 255, nullable: true),
                    name_ascii = table.Column<string>(maxLength: 255, nullable: true),
                    country_id = table.Column<string>(maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_province", x => x.id);
                    table.ForeignKey(
                        name: "fk_province_country1",
                        column: x => x.country_id,
                        principalTable: "country",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "profile_photo",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false),
                    type = table.Column<string>(maxLength: 255, nullable: true),
                    size = table.Column<string>(maxLength: 255, nullable: true),
                    created_date = table.Column<string>(maxLength: 255, nullable: true),
                    file_name = table.Column<string>(maxLength: 255, nullable: true),
                    profile_id = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_profile_photo", x => x.id);
                    table.ForeignKey(
                        name: "fk_profile_photo_profile1",
                        column: x => x.profile_id,
                        principalTable: "profile",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "profile_usage",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false),
                    profile_id = table.Column<long>(nullable: false),
                    is_using_banner = table.Column<bool>(nullable: true),
                    name_of_banner = table.Column<string>(maxLength: 255, nullable: true),
                    smoking_permitted = table.Column<string>(maxLength: 255, nullable: true),
                    pets_allowed = table.Column<string>(maxLength: 255, nullable: true),
                    pre_approved = table.Column<string>(maxLength: 255, nullable: true),
                    first_buyer = table.Column<string>(maxLength: 255, nullable: true),
                    contingency = table.Column<string>(maxLength: 255, nullable: true),
                    urgency = table.Column<string>(maxLength: 255, nullable: true),
                    category_of_property = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_profile_usage", x => x.id);
                    table.ForeignKey(
                        name: "fk_profile_usage_profile1",
                        column: x => x.profile_id,
                        principalTable: "profile",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "proposal",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false),
                    bathrooms = table.Column<string>(maxLength: 255, nullable: true),
                    bedrooms = table.Column<string>(maxLength: 255, nullable: true),
                    area = table.Column<decimal>(type: "numeric", nullable: true),
                    number_of_parking = table.Column<string>(maxLength: 255, nullable: true),
                    number_of_garage = table.Column<string>(maxLength: 255, nullable: true),
                    category_of_property = table.Column<string>(nullable: true),
                    price_minimum = table.Column<string>(maxLength: 255, nullable: true),
                    price_maximum = table.Column<string>(maxLength: 255, nullable: true),
                    is_currently_owner = table.Column<bool>(nullable: true),
                    is_there_contingency = table.Column<bool>(nullable: true),
                    is_with_pool = table.Column<bool>(nullable: true),
                    is_pre_approved = table.Column<bool>(nullable: true),
                    banking_institution = table.Column<string>(maxLength: 255, nullable: true),
                    urgency = table.Column<string>(maxLength: 255, nullable: true),
                    profile_id = table.Column<long>(nullable: false),
                    is_enabled = table.Column<bool>(nullable: true),
                    is_published = table.Column<bool>(nullable: true),
                    is_first_buyer = table.Column<bool>(nullable: true),
                    date_of_creation = table.Column<DateTime>(nullable: true),
                    elevator = table.Column<string>(maxLength: 255, nullable: true),
                    wheelchair = table.Column<string>(maxLength: 255, nullable: true),
                    nearby_school = table.Column<string>(maxLength: 255, nullable: true),
                    nearby_parc = table.Column<string>(maxLength: 255, nullable: true),
                    nearby_sports_center = table.Column<string>(maxLength: 255, nullable: true),
                    nearby_trade = table.Column<string>(maxLength: 255, nullable: true),
                    nearby_public_transport = table.Column<string>(maxLength: 255, nullable: true),
                    nearby_waterfront = table.Column<string>(maxLength: 255, nullable: true),
                    nearby_navigable_water_body = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_proposal", x => x.id);
                    table.ForeignKey(
                        name: "fk_proposal_profile1",
                        column: x => x.profile_id,
                        principalTable: "profile",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "recovery_questionnaire",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false),
                    recovery_question = table.Column<string>(maxLength: 255, nullable: true),
                    recovery_answer = table.Column<string>(maxLength: 255, nullable: true),
                    profile_id = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_recovery_questionnaire", x => x.id);
                    table.ForeignKey(
                        name: "fk_recovery_questionnaire_profile1",
                        column: x => x.profile_id,
                        principalTable: "profile",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "city",
                columns: table => new
                {
                    id = table.Column<string>(maxLength: 255, nullable: false),
                    alternate_names = table.Column<string>(maxLength: 255, nullable: true),
                    f_code = table.Column<string>(maxLength: 255, nullable: true),
                    latitude = table.Column<string>(maxLength: 255, nullable: true),
                    longitude = table.Column<string>(maxLength: 255, nullable: true),
                    name = table.Column<string>(maxLength: 255, nullable: true),
                    name_ascii = table.Column<string>(maxLength: 255, nullable: true),
                    timezone = table.Column<string>(maxLength: 255, nullable: true),
                    province_id = table.Column<string>(maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_city", x => x.id);
                    table.ForeignKey(
                        name: "fk_city_province1",
                        column: x => x.province_id,
                        principalTable: "province",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "type_of_property_usage",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false),
                    name = table.Column<string>(maxLength: 45, nullable: true),
                    price_minimum = table.Column<decimal>(type: "numeric", nullable: true),
                    price_maximum = table.Column<decimal>(type: "numeric", nullable: true),
                    profile_usage_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_type_of_property_usage", x => x.id);
                    table.ForeignKey(
                        name: "fk_type_of_property_usage_profile_usage1",
                        column: x => x.profile_usage_id,
                        principalTable: "profile_usage",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "location",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false),
                    address = table.Column<string>(maxLength: 255, nullable: true),
                    postal_code = table.Column<string>(maxLength: 255, nullable: true),
                    city_id = table.Column<string>(maxLength: 255, nullable: false),
                    country_id = table.Column<string>(maxLength: 255, nullable: false),
                    province_id = table.Column<string>(maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_location", x => x.id);
                    table.ForeignKey(
                        name: "fk_location_city1",
                        column: x => x.city_id,
                        principalTable: "city",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_location_country1",
                        column: x => x.country_id,
                        principalTable: "country",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_location_province1",
                        column: x => x.province_id,
                        principalTable: "province",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "profile_usage_has_location",
                columns: table => new
                {
                    profile_usage_id = table.Column<int>(nullable: false),
                    location_id = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("profile_usage_has_location_pkey", x => new { x.profile_usage_id, x.location_id });
                    table.ForeignKey(
                        name: "fk_profile_usage_has_location_location1",
                        column: x => x.location_id,
                        principalTable: "location",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_profile_usage_has_location_profile_usage1",
                        column: x => x.profile_usage_id,
                        principalTable: "profile_usage",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "property",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false),
                    type = table.Column<string>(maxLength: 45, nullable: true),
                    sale_type = table.Column<string>(maxLength: 45, nullable: true),
                    description = table.Column<string>(maxLength: 45, nullable: true),
                    price = table.Column<decimal>(type: "numeric(45,0)", nullable: true),
                    bedrooms = table.Column<int>(nullable: true),
                    bathrooms = table.Column<int>(nullable: true),
                    characteristics = table.Column<string>(maxLength: 45, nullable: true),
                    status = table.Column<string>(maxLength: 45, nullable: true),
                    size = table.Column<decimal>(type: "numeric", nullable: true),
                    created_date = table.Column<DateTime>(nullable: true),
                    profile_id = table.Column<long>(nullable: false),
                    location_id = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_property", x => x.id);
                    table.ForeignKey(
                        name: "fk_property_location1",
                        column: x => x.location_id,
                        principalTable: "location",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_property_profile1",
                        column: x => x.profile_id,
                        principalTable: "profile",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "proposal_has_location",
                columns: table => new
                {
                    proposal_id = table.Column<int>(nullable: false),
                    location_id = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("proposal_has_location_pkey", x => new { x.proposal_id, x.location_id });
                    table.ForeignKey(
                        name: "fk_proposal_has_location_location1",
                        column: x => x.location_id,
                        principalTable: "location",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_proposal_has_location_proposal1",
                        column: x => x.proposal_id,
                        principalTable: "proposal",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "property_photo",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false),
                    type = table.Column<string>(maxLength: 45, nullable: true),
                    size = table.Column<long>(nullable: true),
                    created_date = table.Column<DateTime>(nullable: true),
                    thumbnail_file_name = table.Column<string>(maxLength: 45, nullable: true),
                    thumbnail_size = table.Column<long>(nullable: true),
                    file_name = table.Column<string>(maxLength: 45, nullable: true),
                    url = table.Column<string>(maxLength: 45, nullable: true),
                    content_type = table.Column<string>(maxLength: 45, nullable: true),
                    property_id = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_property_photo", x => x.id);
                    table.ForeignKey(
                        name: "fk_property_photo_property1",
                        column: x => x.property_id,
                        principalTable: "property",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "fk_city_province1_idx",
                table: "city",
                column: "province_id");

            migrationBuilder.CreateIndex(
                name: "fk_location_city1_idx",
                table: "location",
                column: "city_id");

            migrationBuilder.CreateIndex(
                name: "fk_location_country1_idx",
                table: "location",
                column: "country_id");

            migrationBuilder.CreateIndex(
                name: "fk_location_province1_idx",
                table: "location",
                column: "province_id");

            migrationBuilder.CreateIndex(
                name: "fk_profile_photo_profile1_idx",
                table: "profile_photo",
                column: "profile_id");

            migrationBuilder.CreateIndex(
                name: "fk_profile_usage_profile1_idx",
                table: "profile_usage",
                column: "profile_id");

            migrationBuilder.CreateIndex(
                name: "fk_profile_usage_has_location_location1_idx",
                table: "profile_usage_has_location",
                column: "location_id");

            migrationBuilder.CreateIndex(
                name: "fk_profile_usage_has_location_profile_usage1_idx",
                table: "profile_usage_has_location",
                column: "profile_usage_id");

            migrationBuilder.CreateIndex(
                name: "fk_property_location1_idx",
                table: "property",
                column: "location_id");

            migrationBuilder.CreateIndex(
                name: "fk_property_profile1_idx",
                table: "property",
                column: "profile_id");

            migrationBuilder.CreateIndex(
                name: "fk_property_photo_property1_idx",
                table: "property_photo",
                column: "property_id");

            migrationBuilder.CreateIndex(
                name: "fk_proposal_profile1_idx",
                table: "proposal",
                column: "profile_id");

            migrationBuilder.CreateIndex(
                name: "fk_proposal_has_location_location1_idx",
                table: "proposal_has_location",
                column: "location_id");

            migrationBuilder.CreateIndex(
                name: "fk_proposal_has_location_proposal1_idx",
                table: "proposal_has_location",
                column: "proposal_id");

            migrationBuilder.CreateIndex(
                name: "fk_province_country1_idx",
                table: "province",
                column: "country_id");

            migrationBuilder.CreateIndex(
                name: "fk_recovery_questionnaire_profile1_idx",
                table: "recovery_questionnaire",
                column: "profile_id");

            migrationBuilder.CreateIndex(
                name: "fk_type_of_property_usage_profile_usage1_idx",
                table: "type_of_property_usage",
                column: "profile_usage_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "agent");

            migrationBuilder.DropTable(
                name: "auto_increment");

            migrationBuilder.DropTable(
                name: "payment");

            migrationBuilder.DropTable(
                name: "profile_photo");

            migrationBuilder.DropTable(
                name: "profile_usage_has_location");

            migrationBuilder.DropTable(
                name: "property_photo");

            migrationBuilder.DropTable(
                name: "proposal_has_location");

            migrationBuilder.DropTable(
                name: "recovery_questionnaire");

            migrationBuilder.DropTable(
                name: "type_of_property_usage");

            migrationBuilder.DropTable(
                name: "property");

            migrationBuilder.DropTable(
                name: "proposal");

            migrationBuilder.DropTable(
                name: "profile_usage");

            migrationBuilder.DropTable(
                name: "location");

            migrationBuilder.DropTable(
                name: "profile");

            migrationBuilder.DropTable(
                name: "city");

            migrationBuilder.DropTable(
                name: "province");

            migrationBuilder.DropTable(
                name: "country");
        }
    }
}
