using Proov.Data;
using Proov.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using Proov.BindingModels;
using FluentValidation.Results;
using Proov.Models;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace Proov.Services
{
    public class UtilService : IUtilInterface
    {
        private readonly ApplicationDbContext _context;

        public UtilService(ApplicationDbContext context)
        {
            _context = context;
            
        }

        // display errors 
        public ResponseMessage DisplayErrors(IList<ValidationFailure> Errors)
        {
            // init lit of ErrorModel
            List<ErrorModel> errorModel = new List<ErrorModel>();

            // assign value to the ErrorModel for each distinct propertyname
            foreach (var failure in Errors.GroupBy(o => o.PropertyName).Select(o => o.First()))
            {
                errorModel.Add(new ErrorModel { PropertyName = failure.PropertyName, ErrorMessage = failure.ErrorMessage });
            }

            return new ResponseMessage(false, errorModel);
        }


        // manual increment id
        public long GetId(String resource)
        {
            // get the autoincrement object based on the parameter {resource}
            var autoIncrement = _context.AutoIncrement.FirstOrDefault(m => m.Id == resource);

            // assign 0 to the incrementNumber to return
            long incrementNumber = 0;

            // if autoincrement doesn't exist create the data
            if (autoIncrement == null)
            {
                AutoIncrement incrementIfNull = new AutoIncrement { Id = resource, IncrementNumber = ++incrementNumber };
                _context.Add(incrementIfNull);
                _context.SaveChanges();
            }

            // if autoincrement doesn't exist update the data add +1 to the IncrementNumber
            else
            {
                incrementNumber = autoIncrement.IncrementNumber.Value;
                autoIncrement.Id = resource;
                autoIncrement.IncrementNumber = ++incrementNumber;

                _context.Update(autoIncrement);
                _context.SaveChanges();
            }

            return incrementNumber;
        }

        public String SpaData(JToken prerender)
        {
            return "window.__INITIAL_STATE__ = " + prerender.ToString(Formatting.None);
        }

    }
}
