using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Proov.BindingModels;
using Proov.Interfaces;
using Scriban;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace Proov.Services
{
    public class EmailService : IEmailInterface
    {
        private IConfiguration _config;

        public EmailService(IConfiguration config)
        {
            _config = config;
        }

        public async Task SendEmailAsync(ProfileModelView profileModelView, string subject, string template)
        {

            FileInfo fi = new FileInfo(@"wwwroot/lib/EmailTemplate/" + template);
            StreamReader reader = fi.OpenText();
            var content = reader.ReadToEnd();
            var templateRender = Template.ParseLiquid(content);
            // var htmlContent = templateRender.Render(profileModelView);

            var htmlContent = "<a href='http://yahoo.com'>ggrgr</a>";

            var apiKey = _config["SendGrid:Key"];
            var emailSender = _config["SendGrid:EmailSender"];
            var emailLabel = _config["SendGrid:EmailLabel"];
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress(emailSender, emailLabel);
            var to = new EmailAddress(profileModelView.Email);

            var msg = MailHelper.CreateSingleEmail(from, to, subject, "", htmlContent);

            Console.WriteLine(msg.HtmlContent);
            var response = await client.SendEmailAsync(msg);

        }
    }
}
