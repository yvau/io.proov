using Proov.Data;
using Proov.Models;
using Proov.Interfaces;
using System.Threading.Tasks;

namespace Proov.Services
{
    public class ProposalService : IProposalInterface
    {
        private readonly ApplicationDbContext _context;

        public ProposalService(ApplicationDbContext context)
        {
            _context = context;
        }

        // save proposal
        public async Task SaveAsync(Proposal proposal)
        {
            _context.Add(proposal);
            await _context.SaveChangesAsync();
        }

    }
}
