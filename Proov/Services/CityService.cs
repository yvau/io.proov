using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Proov.Data;
using Proov.Interfaces;
using Proov.Models;


namespace Proov.Services
{
    public class CityService : ICityInterface
    {
        private readonly ApplicationDbContext _context;

        public CityService(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<City> FindByName(String name)
        {
            return _context.City.Where(b => EF.Functions.Like(b.Name.ToLower(), $"%{name.ToLower()}%") || EF.Functions.Like(b.NameAscii.ToLower(), $"%{name.ToLower()}%")).Include(p => p.Province).OrderBy(b => b.Name).Take(Data.Resources.LIMIT_CITIES).ToList();
        }
    }
}
