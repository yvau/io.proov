using Proov.Data;
using Proov.Models;
using Proov.Interfaces;
using System.Linq;
using System.Threading.Tasks;
using System;

namespace Proov.Services
{
    public class ProfileService : IProfileInterface
    {
        private readonly ApplicationDbContext _context;
        private readonly IUtilInterface _utilInterface;

        public ProfileService(ApplicationDbContext context,
                              IUtilInterface utilInterface)
        {
            _context = context;
            _utilInterface = utilInterface;
        }

        // find profile by Email
        public Profile FindByEmail(String email)
        {
            return _context.Profile.FirstOrDefault(x => x.Email == email);
        }

        // find profile by Token
        public Profile FindByToken(String token)
        {
            return _context.Profile.FirstOrDefault(x => x.Token == token);
        }

        // find profile by Token
        public Profile TwoFactor(long id, String verifyCode)
        {
            return _context.Profile.FirstOrDefault(x => x.VerifyCode == verifyCode && x.Id == id);
        }

        // save profile
        public async Task SaveAsync(Profile profile)
        {
            _context.Add(profile);
            await _context.SaveChangesAsync();
        }

        // update profile
        public Profile Update(Profile profile)
        {
            _context.Update(profile);
           _context.SaveChanges();

           return profile;
        }
    }
}
