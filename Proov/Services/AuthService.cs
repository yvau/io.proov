using Proov.BindingModels;
using Proov.Data;
using Proov.Interfaces;
using Proov.Models;
using System;
using System.Linq;

namespace Proov.Services
{
    public class AuthService : IAuthInterface
    {
        private readonly ApplicationDbContext _context;
        private IProfileInterface _profileInterface;

        public AuthService(ApplicationDbContext context, IProfileInterface profileInterface)
        {
            _context = context;
            _profileInterface = profileInterface;
        }

        public ProfileModelConnect SignIn(String Email, String Password, Boolean RememberMe)
        {
            var profile = _profileInterface.FindByEmail(Email);
            if (profile != null)
            {
                Boolean validPassword = BCrypt.Net.BCrypt.Verify(Password, profile.Password);
                if (validPassword)
                {
                    return new ProfileModelConnect(true, profile);
                }
                else
                {
                    return new ProfileModelConnect(false, null);
                }
            }
            return new ProfileModelConnect(false, profile);
        }

        private bool ProfileExists(string email)
        {
            var profile = _context.Profile.Any(e => e.Email == email);
            return profile;
        }
    }
}
