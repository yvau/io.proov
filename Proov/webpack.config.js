const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const dirApp = path.join(__dirname, 'ClientApp/src')

assetsPath = function (_path) {
  const assetsSubDirectory = 'static'
  return path.posix.join(assetsSubDirectory, _path)
}

module.exports = {
  /*optimization: {
    splitChunks: {
      cacheGroups: {
        styles: {
          name: 'vendor',
          // test: /\.css$/,
          chunks: 'all',
          enforce: true
        }
      }
    }
  },*/
    optimization: {
        splitChunks: {
            cacheGroups: {
                commons: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendor',
                    chunks: 'all'
                }
            }
        }
    },
  mode: 'development',
  stats: { modules: false },
  resolve: {
    extensions: ['.js', '.vue', '.json'],
    alias: {
      'vue$': 'vue/dist/vue.esm.js',
      '@': dirApp
    }
  },
  output: {
    filename: '[name].js',
    publicPath: '/dist/'
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        include: __dirname,
        exclude: file => (
          /node_modules/.test(file) &&
          !/\.vue\.js/.test(file)
        )
      },
      /*{
        test: /\.css$/,
        oneOf: [
          // this matches `<style module>`
          {
            resourceQuery: /module/,
            use: [
              'vue-style-loader',
                {
                  loader: 'css-loader',
                  options: {
                    modules: true,
                    localIdentName: '[local]_[hash:base64:5]'
                  }
                }
            ]
          },
          // this matches plain `<style>` or `<style scoped>`
          {
            use: [
              'vue-style-loader',
              'style-loader',
              MiniCssExtractPlugin.loader,
              'css-loader'
            ]
          }
        ]
      },*/
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: assetsPath('fonts/[name].[hash:7].[ext]')
        }
      },
      // IMAGES
      {
        test: /\.(jpe?g|png|gif)$/,
        loader: 'file-loader',
        options: {
          name: assetsPath('images/[name].[ext]')
        }
      },
      {
        test: /\.css$/,
        use: ['vue-style-loader', 'style-loader', MiniCssExtractPlugin.loader,'css-loader']
      },
      {
        test: /\.scss$|\.sass$/,
        use: ['style-loader', MiniCssExtractPlugin.loader, 'css-loader','sass-loader']
      }
    ]
  },
  plugins: [
    new VueLoaderPlugin(),
    new MiniCssExtractPlugin({
      filename: assetsPath('css/[name].css')
    })
  ]
}
