const merge = require('webpack-merge')
const webpackConfig = require('./webpack.config')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const path = require('path')

const dirApp = path.join(__dirname, 'ClientApp/src')

assetsPath = function (_path) {
  const assetsSubDirectory = 'static'
  return path.posix.join(assetsSubDirectory, _path)
}

let cleanOptions = {
  root:     path.join(__dirname, 'wwwroot/static'),
  verbose:  true
}

module.exports = merge(webpackConfig, {
    
    devtool: 'source-map',

    entry: {
        bundle: path.join(dirApp, 'entry-client')
    },
    output: {
        path: path.join(__dirname, 'wwwroot')
    },
    output: {
        path: path.join(__dirname, 'wwwroot'),
        pathinfo: true,
        filename: assetsPath('js/[name].js'),
        publicPath: '/'
    },
    plugins: [
        new CleanWebpackPlugin(['css', 'images', 'js'], cleanOptions)
    ]
})
