const path = require('path')
const webpack = require('webpack')
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const dirApp = path.join(__dirname, 'ClientApp/src')

assetsPath = function (_path) {
  const assetsSubDirectory = 'static'
  return path.posix.join(assetsSubDirectory, _path)
}

let cleanOptions = {
    root: path.join(__dirname, 'ClientApp'),
    verbose: true
}

module.exports = {
    target: 'node',
    mode: 'development',
    entry: { 'server-bundle': './ClientApp/src/entry-server.js' },
    output: {
        libraryTarget: 'commonjs2',
        filename: '[name].js',
        path: path.join(__dirname, 'ClientApp/dist'),
        pathinfo: true,
        publicPath: '/'
    },
    stats: { modules: false },
    resolve: {
		extensions: ['.js', '.vue', '.json'],
    	alias: {
			'vue$': 'vue/dist/vue.esm.js',
      		'@': dirApp
    	}
  	},
    module: {
      rules: [
        {
          test: /\.vue$/,
          loader: 'vue-loader'
        },
        {
          test: /\.js$/,
          loader: 'babel-loader',
          include: __dirname,
          exclude: file => (
            /node_modules/.test(file) &&
            !/\.vue\.js/.test(file)
          )
        },
        {
          test: /\.css$/,
          oneOf: [
            // this matches `<style module>`	
            {
              resourceQuery: /module/,
              use: [
                'vue-style-loader',
                {
                  loader: 'css-loader',
                  options: {
                    modules: true,
                    localIdentName: '[local]_[hash:base64:5]'
                  }
                }
              ]
            },
            // this matches plain `<style>` or `<style scoped>`
            {
              use: [
                'vue-style-loader',
                'css-loader'
              ]
            }
          ]
        },
        // IMAGES
            {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: assetsPath('fonts/[name].[hash:7].[ext]')
        }
      },
      // IMAGES
      {
        test: /\.(jpe?g|png|gif)$/,
        loader: 'file-loader',
        options: {
          name: assetsPath('images/[name].[ext]')
        }
      },
      ]
    },
    plugins: [
      new VueLoaderPlugin(),
      new webpack.optimize.LimitChunkCountPlugin({
        maxChunks: 1
      }),
      new CleanWebpackPlugin(['dist'], cleanOptions)
    ]
}
