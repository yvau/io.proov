using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Proov.Data;
using Proov.Models;

namespace Proov.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Proposals1Controller : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public Proposals1Controller(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Proposals1
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Proposal>>> GetProposal()
        {
            return await _context.Proposal.ToListAsync();
        }

        // GET: api/Proposals1/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Proposal>> GetProposal(long id)
        {
            var proposal = await _context.Proposal.FindAsync(id);

            if (proposal == null)
            {
                return NotFound();
            }

            return proposal;
        }

        // PUT: api/Proposals1/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProposal(long id, Proposal proposal)
        {
            if (id != proposal.Id)
            {
                return BadRequest();
            }

            _context.Entry(proposal).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProposalExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Proposals1
        [HttpPost]
        public async Task<ActionResult<Proposal>> PostProposal(Proposal proposal)
        {
            _context.Proposal.Add(proposal);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ProposalExists(proposal.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetProposal", new { id = proposal.Id }, proposal);
        }

        // DELETE: api/Proposals1/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Proposal>> DeleteProposal(long id)
        {
            var proposal = await _context.Proposal.FindAsync(id);
            if (proposal == null)
            {
                return NotFound();
            }

            _context.Proposal.Remove(proposal);
            await _context.SaveChangesAsync();

            return proposal;
        }

        private bool ProposalExists(long id)
        {
            return _context.Proposal.Any(e => e.Id == id);
        }
    }
}
