using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Proov.Helpers;
using Proov.Interfaces;


namespace Proov.Controllers
{
    [Route("/profile/")]
    public class ProfileEditController : Controller
    {
        private IUtilInterface _utilInterface;

        public ProfileEditController(IUtilInterface utilInterface)
        {
            _utilInterface = utilInterface;
        }

        // GET: Profile/Edit/Role
        [HttpGet("{id}")]
        public async Task<IActionResult> Show(long id)
        {
            var prerenderResult = await Request.BuildPrerender("showProfile");
            
            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View();
        }

        // GET: Profile/Edit/Role
        [HttpGet("edit/role")]
        public async Task<IActionResult> EditRole()
        {
            var prerenderResult = await Request.BuildPrerender("editRole");
            
            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View();
        }

        // GET: Profile/Edit/Password
        [HttpGet("edit/password")]
        public async Task<IActionResult> EditPassword()
        {
            var prerenderResult = await Request.BuildPrerender("editPassword");
            
            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View();
        }

        // GET: Profile/Edit/Information
        [HttpGet("edit/information")]
        public async Task<IActionResult> EditInformation()
        {
            var prerenderResult = await Request.BuildPrerender("editInformation");
            
            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View();
        }
    }
}
