using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Proov.Helpers;
using Proov.Interfaces;


namespace Proov.Controllers
{
    public class HomeController : Controller
    {
        private IEmailInterface _emailInterface;
        private IUtilInterface _utilInterface;

        public HomeController(IEmailInterface emailInterface,
                              IUtilInterface utilInterface)
        {
            _emailInterface = emailInterface;
            _utilInterface = utilInterface;
        }

        public async Task<IActionResult> Index()
        {
            var prerenderResult = await Request.BuildPrerender("home");
            
            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View();
        }

        /*public JsonResult FetchMessages(DateTime lastFetchedMessageDate)
        {
            return Json(FakeMessageStore.FakeMessages.OrderByDescending(m => m.Date).SkipWhile(m => m.Date >= lastFetchedMessageDate).Take(1));
        }*/

        [HttpGet("/about/")]
        public async Task<IActionResult> About()
        {

            var prerenderResult = await Request.BuildPrerender("about");

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View();
        }

        [HttpGet("/concept/")]
        public async Task<IActionResult> Concept()
        {
            var prerenderResult = await Request.BuildPrerender("concept");

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View();
        }

        [HttpGet("/advantage/")]
        public async Task<IActionResult> Advantage()
        {
            var prerenderResult = await Request.BuildPrerender("advantage");

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View();
        }

        [HttpGet("/relationship/")]
        public async Task<IActionResult> Relationship()
        {
            var prerenderResult = await Request.BuildPrerender("relationship");

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View();
        }

        [HttpGet("/blogs/")]
        public async Task<IActionResult> Blogs()
        {
            var prerenderResult = await Request.BuildPrerender("blogs");

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View();
        }

        [HttpGet("/blog/")]
        public async Task<IActionResult> Blog()
        {
            var prerenderResult = await Request.BuildPrerender("blog");

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View();
        }

        [HttpGet("/privacy-and-policy/")]
        public async Task<IActionResult> Privacy()
        {
            var prerenderResult = await Request.BuildPrerender("privacy");

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View();
        }

        [HttpGet("/terms-of-use/")]
        public async Task<IActionResult> Terms()
        {
            var prerenderResult = await Request.BuildPrerender("terms");

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View();
        }

        [HttpGet("/help/")]
        public async Task<IActionResult> Help()
        {

            var prerenderResult = await Request.BuildPrerender("help");

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View();
        }

        /*[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }*/
    }
}
