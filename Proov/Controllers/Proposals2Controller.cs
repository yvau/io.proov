using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Proov.Data;
using Proov.Models;

namespace Proov.Controllers
{
    public class Proposals2Controller : Controller
    {
        private readonly ApplicationDbContext _context;

        public Proposals2Controller(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Proposals
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Proposal.Include(p => p.Profile);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Proposals/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var proposal = await _context.Proposal
                .Include(p => p.Profile)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (proposal == null)
            {
                return NotFound();
            }

            return View(proposal);
        }

        // GET: Proposals/Create
        public IActionResult Create()
        {
            ViewData["ProfileId"] = new SelectList(_context.Profile, "Id", "Email");
            return View();
        }

        // POST: Proposals/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Bathrooms,Bedrooms,Area,NumberOfParking,NumberOfGarage,CategoryOfProperty,PriceMinimum,PriceMaximum,IsCurrentlyOwner,IsThereContingency,IsWithPool,IsPreApproved,BankingInstitution,Urgency,ProfileId,IsEnabled,IsPublished,IsFirstBuyer,DateOfCreation,Elevator,Wheelchair,NearbySchool,NearbyParc,NearbySportsCenter,NearbyTrade,NearbyPublicTransport,NearbyWaterfront,NearbyNavigableWaterBody")] Proposal proposal)
        {
            if (ModelState.IsValid)
            {
                _context.Add(proposal);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProfileId"] = new SelectList(_context.Profile, "Id", "Email", proposal.ProfileId);
            return View(proposal);
        }

        // GET: Proposals/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var proposal = await _context.Proposal.FindAsync(id);
            if (proposal == null)
            {
                return NotFound();
            }
            ViewData["ProfileId"] = new SelectList(_context.Profile, "Id", "Email", proposal.ProfileId);
            return View(proposal);
        }

        // POST: Proposals/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("Id,Bathrooms,Bedrooms,Area,NumberOfParking,NumberOfGarage,CategoryOfProperty,PriceMinimum,PriceMaximum,IsCurrentlyOwner,IsThereContingency,IsWithPool,IsPreApproved,BankingInstitution,Urgency,ProfileId,IsEnabled,IsPublished,IsFirstBuyer,DateOfCreation,Elevator,Wheelchair,NearbySchool,NearbyParc,NearbySportsCenter,NearbyTrade,NearbyPublicTransport,NearbyWaterfront,NearbyNavigableWaterBody")] Proposal proposal)
        {
            if (id != proposal.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(proposal);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProposalExists(proposal.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProfileId"] = new SelectList(_context.Profile, "Id", "Email", proposal.ProfileId);
            return View(proposal);
        }

        // GET: Proposals/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var proposal = await _context.Proposal
                .Include(p => p.Profile)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (proposal == null)
            {
                return NotFound();
            }

            return View(proposal);
        }

        // POST: Proposals/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var proposal = await _context.Proposal.FindAsync(id);
            _context.Proposal.Remove(proposal);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProposalExists(long id)
        {
            return _context.Proposal.Any(e => e.Id == id);
        }
    }
}
