using Microsoft.AspNetCore.Mvc;
using Proov.BindingModels;
using Proov.Interfaces;
using Proov.Helpers;
using Proov.Models;
using System;
using System.Threading.Tasks;

namespace Proov.Controllers
{
    [Route("/")]
    public class ProposalsController : Controller
    {
        private IProposalInterface _proposalInterface;
        private IUtilInterface _utilInterface;

        public ProposalsController(IProposalInterface proposalInterface,
                                   IUtilInterface utilInterface)
        {
            _proposalInterface = proposalInterface;
            _utilInterface = utilInterface;
        }

        // GET: Buy/New
        [HttpGet("buy/new")]
        public IActionResult CreateBuy()
        {
            return View();
        }

        // POST: Buy/New
        [HttpPost("buy/new")]
        public IActionResult CreateBuy([FromBody] ProposalModelView proposalModelView)
        {
            var buy = new Proposal
            {
                Id = _utilInterface.GetId(Data.Resources.BUY),
                Bathrooms = proposalModelView.Bathrooms,
                Bedrooms =proposalModelView.Bedrooms,
                Area = proposalModelView.Area,
                NumberOfParking = proposalModelView.NumberOfParking,
                NumberOfGarage = proposalModelView.NumberOfGarage,
                CategoryOfProperty = proposalModelView.CategoryOfProperty,
                PriceMinimum = proposalModelView.PriceMinimum,
                PriceMaximum = proposalModelView.PriceMaximum,
                IsCurrentlyOwner = proposalModelView.IsCurrentlyOwner,
                IsThereContingency = proposalModelView.IsThereContingency,
                IsWithPool = proposalModelView.IsWithPool,
                IsPreApproved = proposalModelView.IsPreApproved,
                BankingInstitution = proposalModelView.BankingInstitution,
                Urgency = proposalModelView.Urgency,
                IsEnabled = proposalModelView.IsEnabled,
                IsPublished = proposalModelView.IsPublished,
                IsFirstBuyer = proposalModelView.IsFirstBuyer,
                DateOfCreation = DateTime.Now,
                Elevator = proposalModelView.Elevator,
                Wheelchair = proposalModelView.Wheelchair,
                NearbySchool = proposalModelView.NearbySchool,
                NearbyParc = proposalModelView.NearbyParc,
                NearbySportsCenter = proposalModelView.NearbySportsCenter,
                NearbyTrade = proposalModelView.NearbyTrade,
                NearbyPublicTransport = proposalModelView.NearbyPublicTransport,
                NearbyWaterfront = proposalModelView.NearbyWaterfront,
                NearbyNavigableWaterBody = proposalModelView.NearbyNavigableWaterBody,
            };

            return View();
        }

        // GET: Buy/Edit/{id}
        [HttpGet("buy/edit/{id}")]
        public IActionResult UpdateBuy(int? id)
        {
            return View();
        }

        // GET: Buy/List
        [HttpGet("buy/list")]
        public async Task<IActionResult> ListBuy()
        {
            var prerenderResult = await Request.BuildPrerender("buyList");

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View();
        }


        // GET: Rent/New
        [HttpGet("rent/new")]
        public async Task<IActionResult> CreateRent()
        {
            var prerenderResult = await Request.BuildPrerender("rentNew");

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View();
        }

        // POST: Rent/New
        [HttpPost("rent/new")]
        public IActionResult CreateRent([FromBody] ProposalModelView proposalModelView)
        {
            var rent = new Proposal
            { 
                Id = _utilInterface.GetId(Data.Resources.RENT),
                Bathrooms = proposalModelView.Bathrooms,
                Bedrooms = proposalModelView.Bedrooms,
                Area = proposalModelView.Area,
                NumberOfParking = proposalModelView.NumberOfParking,
                NumberOfGarage = proposalModelView.NumberOfGarage,
                CategoryOfProperty = proposalModelView.CategoryOfProperty,
                PriceMinimum = proposalModelView.PriceMinimum,
                PriceMaximum = proposalModelView.PriceMaximum,
                IsCurrentlyOwner = proposalModelView.IsCurrentlyOwner,
                IsThereContingency = proposalModelView.IsThereContingency,
                IsWithPool = proposalModelView.IsWithPool,
                IsPreApproved = proposalModelView.IsPreApproved,
                BankingInstitution = proposalModelView.BankingInstitution,
                Urgency = proposalModelView.Urgency,
                IsEnabled = proposalModelView.IsEnabled,
                IsPublished = proposalModelView.IsPublished,
                IsFirstBuyer = proposalModelView.IsFirstBuyer,
                DateOfCreation = DateTime.Now,
                Elevator = proposalModelView.Elevator,
                Wheelchair = proposalModelView.Wheelchair,
                NearbySchool = proposalModelView.NearbySchool,
                NearbyParc = proposalModelView.NearbyParc,
                NearbySportsCenter = proposalModelView.NearbySportsCenter,
                NearbyTrade = proposalModelView.NearbyTrade,
                NearbyPublicTransport = proposalModelView.NearbyPublicTransport,
                NearbyWaterfront = proposalModelView.NearbyWaterfront,
                NearbyNavigableWaterBody = proposalModelView.NearbyNavigableWaterBody,
            };
            return View();
        }

        // GET: Rent/Edit/{id}
        [HttpGet("rent/edit/{id}")]
        public IActionResult UpdateRent(int? id)
        {
            return View();
        }

        // GET: Rent/List
        [HttpGet("rent/list")]
        public async Task<IActionResult> ListRent()
        {
            var prerenderResult = await Request.BuildPrerender("rentList");

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View();
        }

        // GET: Rent/List
        [HttpGet("proposal/{id}")]
        public async Task<IActionResult> ShowProposal(int? id)
        {
            var prerenderResult = await Request.BuildPrerender("showProposal");

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View();
        }

    }
}
