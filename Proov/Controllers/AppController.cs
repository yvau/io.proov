using FluentValidation.Results;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Proov.BindingModels;
using Proov.Helpers;
using Proov.Interfaces;
using Proov.Models;
using Proov.Resources;
using Proov.Validators;
using System;
using System.Linq;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Proov.Controllers
{
    [Route("app/")]
    public class AppController : Controller
    {
        private IAuthInterface _authInterface;
        private IProfileInterface _profileInterface;
        private IEmailInterface _emailInterface;
        private IUtilInterface _utilInterface;
        private ISmsInterface _smsInterface;
        private LocService _locService;
        private readonly IHttpContextAccessor _accessor;
        private static Random random = new Random();

        public AppController(IAuthInterface authInterface,
                              IProfileInterface profileInterface,
                              IEmailInterface emailInterface,
                              ISmsInterface smsInterface,
                              LocService locService,
                              IHttpContextAccessor accessor,
                              IUtilInterface utilInterface)
        {
            _profileInterface = profileInterface;
            _authInterface = authInterface;
            _emailInterface = emailInterface;
            _smsInterface = smsInterface;
            _locService = locService;
            _utilInterface = utilInterface;
            _accessor = accessor;
        }

        // GET: Auth
        [HttpGet]
        public IActionResult Index()
        {
            return RedirectToAction(nameof(Home));
        }

        // GET: App/Home
        [HttpGet("home")]
        public async Task<IActionResult> Home()
        {
            var prerenderResult = await Request.BuildPrerender("appHome");

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View();
        }

    


    }
}
