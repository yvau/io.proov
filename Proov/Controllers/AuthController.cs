using FluentValidation.Results;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Proov.BindingModels;
using Proov.Helpers;
using Proov.Interfaces;
using Proov.Models;
using Proov.Resources;
using Proov.Validators;
using System;
using System.Linq;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Proov.Controllers
{
    [Route("auth/")]
    public class AuthController : Controller
    {
        private IAuthInterface _authInterface;
        private IProfileInterface _profileInterface;
        private IEmailInterface _emailInterface;
        private IUtilInterface _utilInterface;
        private ISmsInterface _smsInterface;
        private LocService _locService;
        private readonly IHttpContextAccessor _accessor;
        public IConfiguration _configuration { get; }
        private static Random random = new Random();

        public AuthController(IAuthInterface authInterface,
                              IProfileInterface profileInterface,
                              IEmailInterface emailInterface,
                              ISmsInterface smsInterface,
                              LocService locService,
                              IHttpContextAccessor accessor,
                              IUtilInterface utilInterface,
                              IConfiguration configuration)
        {
            _profileInterface = profileInterface;
            _authInterface = authInterface;
            _emailInterface = emailInterface;
            _smsInterface = smsInterface;
            _locService = locService;
            _utilInterface = utilInterface;
            _accessor = accessor;
            _configuration = configuration;
        }

        // GET: Auth
        [HttpGet]
        public IActionResult Index()
        {
            return RedirectToAction(nameof(Login));
        }

        // GET: Auth/Login
        [HttpGet("login")]
        public async Task<IActionResult> Login()
        {
            var prerenderResult = await Request.BuildPrerender("login");

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View();
        }

        // POST: Auth/Login
        [HttpPost("login")]
        public IActionResult Login([FromBody] ProfileModelView profileModelView)
        {
            var authProcess = _authInterface.SignIn(profileModelView.Email, profileModelView.Password, profileModelView.RememberMe);

            if (authProcess.status)
            {
                SetCookies("AspTwoFactor", authProcess.profile.Id.ToString(), 10);
                Regex pattern = new Regex("[()\\- ]");
                String phone = pattern.Replace(authProcess.profile.Phone, "");
                authProcess.profile.VerifyCode = _smsInterface.SendSms(phone);
                _profileInterface.Update(authProcess.profile);
                var redirectUrl =  String.IsNullOrWhiteSpace(profileModelView.RedirectUrl) ? "/" : profileModelView.RedirectUrl;
                return Json(new ResponseMessage(true, new { RedirectUrl = redirectUrl }));
            }
            return Json(new ResponseMessage(true, new { Message = "Your email and password don't match. Please try again.", Success = false }));
        }

        // POST: Auth/GenerateToken
        [HttpPost("generateToken")]
        public IActionResult GenerateToken()
        {
            var token = new JwtTokenBuilder()
                                .AddSecurityKey(JwtSecurityKey.Create(_configuration.GetValue<string>("JwtSecretKey")))
                                .AddIssuer(_configuration.GetValue<string>("JwtIssuer"))
                                .AddAudience(_configuration.GetValue<string>("JwtAudience"))
                                .AddExpiry(1)
                                .AddClaim("Name", "Admin")
                                .AddRole("Admin")
                                .Build();

            return View();
        }


        // GET: Auth/VerifyCode
        [HttpGet("verify-code")]
        public async Task<IActionResult> VerifyCode(string provider, string returnUrl)
        {
            /*String cookies = GetCookies("AspTwoFactor");
            if (cookies == null)
            {
                throw new ApplicationException($"Unable to load two-factor authentication user.");
            }*/
            var prerenderResult = await Request.BuildPrerender("verifyCode");

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View();
        }

        // POST: Auth/VerifyCode
        [HttpPost("verify-code")]
        public async Task<IActionResult> VerifyCode([FromBody] ProfileModelView profileModelView)
        {
            // check validation
            VerifyCodeValidator validator = new VerifyCodeValidator();
            ValidationResult results = validator.Validate(profileModelView);

            // if validation is unsuccessful show error
            if (!results.IsValid)
            {
                var errors = _utilInterface.DisplayErrors(results.Errors);
                return Json(errors);
            }

            String cookies = GetCookies("AspTwoFactor");
            if (cookies == null)
            {
                throw new ApplicationException($"Unable to load two-factor authentication user.");
            }

            var profile = _profileInterface.TwoFactor(Convert.ToInt64(cookies), profileModelView.VerifyCode);

            if (profile == null)
            {
                return Json(new ResponseMessage(false, null));
            }

            string[] roles = profile.Role.Split(",");

            var identity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme, ClaimTypes.Name, ClaimTypes.Role);
            identity.AddClaim(new Claim(ClaimTypes.Sid, profile.Id.ToString()));
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, profile.Email));
            identity.AddClaim(new Claim(ClaimTypes.Name, profile.LastName));
            identity.AddClaim(new Claim(ClaimTypes.Surname, profile.FirstName));

            foreach (var role in roles)
            {
                identity.AddClaim(new Claim(ClaimTypes.Role, role));
            }

            // Authenticate using the identity
            var principal = new ClaimsPrincipal(identity);
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal, new AuthenticationProperties { IsPersistent = true });

            return Json(new ResponseMessage(true, profile));
        }


        // GET: Auth/Register
        [HttpGet("register")]
        public async Task<IActionResult> Register()
        {
            var prerenderResult = await Request.BuildPrerender("register");

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View();
        }

        // POST: Auth/Register
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] ProfileModelView profileModelView)
        {
            // check validation
            RegisterValidator validator = new RegisterValidator(_profileInterface, _locService);
            ValidationResult results = validator.Validate(profileModelView);

            // if validation is unsuccessful show error
            if (!results.IsValid)
            {
                var errors = _utilInterface.DisplayErrors(results.Errors);
                return Json(errors);
            }

            var profile = new Profile
            {
                Id = _utilInterface.GetId(Data.Resources.PROFILE),
                FirstName = profileModelView.FirstName,
                LastName = profileModelView.LastName,
                Email = profileModelView.Email,
                PhoneConfirmed = false,
                Token = RandomString(24),
                DateOfCreationToken = DateTime.Now,
                Password = BCrypt.Net.BCrypt.HashPassword(profileModelView.Password),
                DateOfCreation = DateTime.Now,
                Role = "ROLE_LIMITED",
                Enabled = false,
                IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString()
            };

            // then we save data
            await _profileInterface.SaveAsync(profile);

            // send email
            String website = $"http://vone.club/auth/activate?token={profile.Token}";
            // await _emailInterface.SendEmailAsync(new ProfileModelView { Email = profile.Email, Website = website }, "registration proov - app", Data.Resources.REGISTER_TEMPLATE);
            return Json(new ResponseMessage(true, new { Message = $"an email has been sent to {profile.Email}", Success = true }));
        }

        // GET: Auth/RecoverPassword
        [HttpGet("recover-password")]
        public async Task<IActionResult> RecoverPassword()
        {
            var prerenderResult = await Request.BuildPrerender("recover");

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View();
        }

        // POST: Auth/RecoverPassword
        [HttpPost("recover-password")]
        public async Task<IActionResult> RecoverPasswordAsync([FromBody] ProfileModelView profileModelView)
        {
            // check validation
            RecoverValidator validator = new RecoverValidator(_profileInterface, _locService);
            ValidationResult results = validator.Validate(profileModelView);

            // if validation is unsuccessful show error
            if (!results.IsValid)
            {
                var errors = _utilInterface.DisplayErrors(results.Errors);
                return Json(errors);
            }

            var profile = _profileInterface.FindByEmail(profileModelView.Email);

            profile.Token = RandomString(24);

            _profileInterface.Update(profile);

            // send email
            String website = $"http://vone.club/auth/change_password?token={profile.Token}";
            await _emailInterface.SendEmailAsync(new ProfileModelView { Email = profile.Email, Website = website }, "recover proov - app", Data.Resources.RECOVER_TEMPLATE);

            return Json(new ResponseMessage(true, new { Message = $"an email has been sent to {profile.Email}", Success = true }));
        }

        // GET: Auth/Activate
        [HttpGet("activate")]
        public async Task<IActionResult> ActivateAsync(String token)
        {
            // assign value query string value to token
            token = HttpContext.Request.Query["token"];

            // if token is not null
            if (!String.IsNullOrEmpty(token))
            {
                // search for token in the database
                var profile = _profileInterface.FindByToken(token);

                // if data is null return message
                if (profile == null)
                {
                    return Json(new ResponseMessage(false, "no token found"));
                }

                // update data
                profile.Token = null;
                profile.Enabled = true;
                _profileInterface.Update(profile);

                // log in user
                string[] roles = profile.Role.Split(",");

                var identity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme, ClaimTypes.Name, ClaimTypes.Role);
                identity.AddClaim(new Claim(ClaimTypes.Sid, profile.Id.ToString()));
                identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, profile.LastName));
                identity.AddClaim(new Claim(ClaimTypes.Name, profile.Email));
                identity.AddClaim(new Claim(ClaimTypes.Email, profile.Email));
                identity.AddClaim(new Claim(ClaimTypes.Surname, profile.FirstName));

                foreach (var role in roles)
                {
                    identity.AddClaim(new Claim(ClaimTypes.Role, role));
                }

                // Authenticate using the identity
                var principal = new ClaimsPrincipal(identity);
                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal, new AuthenticationProperties { IsPersistent = true });

                // if token been provided is good
                return RedirectToAction(nameof(CompleteRole));

            }

            return Json(new ResponseMessage(false, "token not provided !"));
        }

        // GET: Auth/ChangePassword
        [HttpGet("change-password")]
        public async Task<IActionResult> ChangePassword(String token)
        {
            var prerenderResult = await Request.BuildPrerender("changePassword");

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            // assign value query string value to token
            token = HttpContext.Request.Query["token"];

            // if token is not null
            if (!String.IsNullOrEmpty(token))
            {
                // search for token in the database
                var profile = _profileInterface.FindByToken(token);

                // if data is null return message
                if (profile == null)
                {
                    throw new ApplicationException($"No corresponding token found .");
                }

                // if token been provided is good
                return View();
            }

            throw new ApplicationException($"Token not provided.");

        }

        // POST: Auth/ChangePassword
        [HttpPost("change_password")]
        public IActionResult ChangePassword([FromBody] ProfileModelView profileModelView)
        {
            // check validation
            ChangePasswordValidator validator = new ChangePasswordValidator();
            ValidationResult results = validator.Validate(profileModelView);

            // if validation is unsuccessful show error
            if (!results.IsValid)
            {
                var errors = _utilInterface.DisplayErrors(results.Errors);
                return Json(errors);
            }

            var profile = _profileInterface.FindByToken(profileModelView.Token);

            // update data

            profile.Token = null;
            profile.Password = BCrypt.Net.BCrypt.HashPassword(profileModelView.Password);
            _profileInterface.Update(profile);


            return Json(new ResponseMessage(true, new { Message = "your password has been changed, please login", Success = true }));
        }

        // GET: Auth/CompleteRole
        //[Authorize(Roles = "ROLE_LIMITED")]
        [HttpGet("complete-role")]
        public async Task<IActionResult> CompleteRole()
        {
            var prerenderResult = await Request.BuildPrerender("completeRole");

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View();
        }

        // POST: Auth/CompleteRole
        // [Authorize(Roles = "ROLE_LIMITED")]
        [HttpPost("complete-role")]
        public IActionResult CompleteRole([FromBody] ProfileModelView profileModelView)
        {
            // check validation
            CompleteRoleValidator validator = new CompleteRoleValidator();
            ValidationResult results = validator.Validate(profileModelView);

            // if validation is unsuccessful show error
            if (!results.IsValid)
            {
                var errors = _utilInterface.DisplayErrors(results.Errors);
                return Json(errors);
            }

            var email = User.Identity.Name;
            // var currentUserName = _contextAccessor.HttpContext.User?.Identity?.Name;
            /*
             * public class UserResolverService
                {
                    private readonly IHttpContextAccessor _context;
    
                    public UserResolverService(IHttpContextAccessor context)
                    {
                        _context = context;
                    }

                    public string GetUser()
                    {
                        return await _context.HttpContext.User?.Identity?.Name;
                    }
                }
             */
            //in your startup.cs ConfigureServices method add
            // services.AddTransient<UserResolverService>();

            /*
                public partial class ExampleContext : IExampleContext
                {
                    private YourContext _context;
                    private string _user;
        
                    public ExampleContext(YourContext context, UserResolverService userService)
                    {
                        _context = context;
                        _user = userService.GetUser();
                    }
                }
             */
            var profile = _profileInterface.FindByEmail(email);
            profile.Role = profileModelView.Role;
            _profileInterface.Update(profile);

            SetCookies("FormToCompleteInformation", "complete_information", 10);

            return Json(new ResponseMessage(true, null));
        }

        // GET: Auth/CompleteInformation
        // [Authorize(Roles = "ROLE_LIMITED")]
        [HttpGet("complete-information")]
        public async Task<IActionResult> CompleteInformation()
        {
            var prerenderResult = await Request.BuildPrerender("completeInformation");

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View();
        }

        // POST: Auth/CompleteInformation
        // [Authorize(Roles = "ROLE_LIMITED")]
        [HttpPost("complete_information")]
        public IActionResult CompleteInformation([FromBody] ProfileModelView profileModelView)
        {
            // check validation
            CompleteInformationValidator validator = new CompleteInformationValidator();
            ValidationResult results = validator.Validate(profileModelView);

            // if validation is unsuccessful show error
            if (!results.IsValid)
            {
                var errors = _utilInterface.DisplayErrors(results.Errors);
                return Json(errors);
            }

            var email = User.Identity.Name;
            var profile = _profileInterface.FindByEmail(email);
            profile.Phone = profileModelView.PhoneMobile;
            profile.Gender = profileModelView.Gender;
            profile.AccreditationNumber = profileModelView.AccreditationNumber;
            profile.Website = profileModelView.Website;
            profile.Linkedin = profileModelView.Linkedin;
            profile.Facebook = profileModelView.Facebook;
            profile.Instagram = profileModelView.Instagram;
            profile.Twitter = profileModelView.Twitter;

            _profileInterface.Update(profile);

            SetCookies("FormToCompleteInformation", "complete_All", 10);

            return Json(new ResponseMessage(true, null));
        }

        // GET: Auth/CompleteSeller
        [Authorize(Roles = "ROLE_LIMITED")]
        [HttpGet("complete-seller")]
        public async Task<IActionResult> CompleteSeller()
        {
            var prerenderResult = await Request.BuildPrerender("completeSeller");

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View();
        }

        // GET: Auth/CompleteTenant
        [Authorize(Roles = "ROLE_LIMITED")]
        [HttpGet("complete-tenant")]
        public async Task<IActionResult> CompleteTenant()
        {
            var prerenderResult = await Request.BuildPrerender("completeTenant");

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View();
        }

        // GET: Auth/CompleteDone
        [Authorize]
        [HttpGet("complete_done")]
        public IActionResult CompleteDone()
        {
            return View();
        }

        // GET: Auth/Me
        [HttpGet("me")]
        public IActionResult Me()
        {
            if (User.Identity.IsAuthenticated)
            {
                var profile = _profileInterface.FindByEmail(User.Identity.Name);
                return Json(new ResponseMessage(true, profile));
            }

            return Json(new ResponseMessage(false, null));
        }

        // GET: Auth/Logout
        [HttpGet("logout")]
        public async Task<IActionResult> LogoutAsync()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction(nameof(Login));
        }

        // randow string to generate token
        public static String RandomString(int length)
        {
            const String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz$@";
            return new String(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        // Get the cookie
        private string GetCookies(string key)
        {
            return Request.Cookies[key];
        }

        // set the cookie   
        private void SetCookies(string key, string value, int? expireTime)
        {
            CookieOptions option = new CookieOptions();
            if (expireTime.HasValue)
                option.Expires = DateTime.Now.AddMinutes(expireTime.Value);
            else
                option.Expires = DateTime.Now.AddMinutes(10);
            Response.Cookies.Append(key, value, option);
        }

        // Delete the key  
        private void RemoveCookies(string key)
        {
            Response.Cookies.Delete(key);
        }
    }
}
