using Microsoft.AspNetCore.Mvc;
using Proov.BindingModels;
using Proov.Interfaces;
using Proov.Helpers;
using Proov.Models;
using System;
using System.Threading.Tasks;

namespace Proov.Controllers
{
    [Route("/")]
    public class PropertiesController : Controller
    {
        private IProposalInterface _proposalInterface;
        private IUtilInterface _utilInterface;

        public PropertiesController(IProposalInterface proposalInterface,
                                   IUtilInterface utilInterface)
        {
            _proposalInterface = proposalInterface;
            _utilInterface = utilInterface;
        }

        // GET: Property/New
        [HttpGet("property/new")]
        public IActionResult CreateBuy()
        {
            return View("/Views/Global/Home.cshtml");
        }


        // GET: Propety/List
        [HttpGet("property/list")]
        public async Task<IActionResult> ListProperty()
        {
            var prerenderResult = await Request.BuildPrerender("propertyList");

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View("/Views/Global/Home.cshtml");
        }


        // GET: Property/Edit/{id}
        [HttpGet("property/edit/{id}")]
        public IActionResult UpdateProperty(int? id)
        {
            return View("/Views/Global/Home.cshtml");
        }

        // GET: Property/{id}
        [HttpGet("property/{id}")]
        public async Task<IActionResult> ShowProperty(int? id)
        {
            var prerenderResult = await Request.BuildPrerender("showProperty");

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _utilInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View("/Views/Global/Home.cshtml");
        }

    }
}
