using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.NodeServices;
using Microsoft.AspNetCore.SpaServices.Prerendering;
using Microsoft.Extensions.DependencyInjection;
using Proov.Data;

namespace Proov.Helpers
{
    public static class HttpRequestExtensions
    {

        /*public static IRequest AbstractRequestInfo(this HttpRequest request) => new IRequest()
        {
            cookies = request.Cookies,
            headers = request.Headers,
            host = request.Host
        };*/

        public static async Task<RenderToStringResult> BuildPrerender(this HttpRequest request, String routeName) =>
            // Prerender / Serialize application (with Universal)
            await Prerenderer.RenderToString(
                "/",
                request.HttpContext.RequestServices.GetRequiredService<INodeServices>(),
                new System.Threading.CancellationTokenSource().Token,
                new JavaScriptModuleExport(request.HttpContext.RequestServices.GetRequiredService<IHostingEnvironment>().ContentRootPath + "/ClientApp/renderOnServer"),
                $"{request.Scheme}://{request.Host}{request.HttpContext.Features.Get<IHttpRequestFeature>().RawTarget}",
                request.HttpContext.Features.Get<IHttpRequestFeature>().RawTarget,
                // ** TransferData concept **
                // Here we can pass any Custom Data we want !
                // By default we're passing down Cookies, Headers, Host from the Request object here
                new TransferData
                {
                    auth = new { authenticated = request.HttpContext.User.Identity.IsAuthenticated },
                    alert = new { status = false, success = "null", message  = "null" },
                    account = new { email = request.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value, firstName = request.HttpContext.User.FindFirst(ClaimTypes.Surname).Value, lastName = request.HttpContext.User.FindFirst(ClaimTypes.Name).Value, photoUrl = request.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value },
                    route = new { name = routeName, path = request.Path, hash = "", query = new { }, @params= new { }, fullPath = request.Path + request.QueryString, meta = new { }, from = new { name = "null", path = "/", hash = "", query = new {}, @params = new {},fullPath = "/",
                    meta = new {} } },
                    // request = request.AbstractRequestInfo(),
                    // thisCameFromDotNET = "Hi Angular it's asp.net :)"
                }, // Our simplified Request object & any other CustommData you want to send!
                30000,
                request.PathBase.ToString()
            );
    }

    internal class TransferData
    {
        public Object auth { get; set; }
        public Object alert { get; set; }
        public Object account { get; set; }
        public Object route { get; set; }
        // public IRequest request { get; set; }
        // public string thisCameFromDotNET { get; set; }
    }
}
