using System;

namespace Proov.Data
{
    public class Resources
    {
        // DATA MODEL
        public static String PROFILE = "profile";
        public static String BUY = "buy";
        public static String RENT = "rent";

        // TEMPLATE VALUE
        public static String REGISTER_TEMPLATE = "Register.hbs";
        public static String RECOVER_TEMPLATE = "ForgotPassword.hbs";

        // STATIC VARIABLE
        public static int LIMIT_CITIES = 300;
    }
}
