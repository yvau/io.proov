﻿using System;
using Proov.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Proov.Data
{
    public partial class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext()
        {
        }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Agent> Agent { get; set; }
        public virtual DbSet<AutoIncrement> AutoIncrement { get; set; }
        public virtual DbSet<City> City { get; set; }
        public virtual DbSet<Country> Country { get; set; }
        public virtual DbSet<Location> Location { get; set; }
        public virtual DbSet<Payment> Payment { get; set; }
        public virtual DbSet<Profile> Profile { get; set; }
        public virtual DbSet<ProfilePhoto> ProfilePhoto { get; set; }
        public virtual DbSet<ProfileUsage> ProfileUsage { get; set; }
        public virtual DbSet<ProfileUsageHasLocation> ProfileUsageHasLocation { get; set; }
        public virtual DbSet<Property> Property { get; set; }
        public virtual DbSet<PropertyPhoto> PropertyPhoto { get; set; }
        public virtual DbSet<Proposal> Proposal { get; set; }
        public virtual DbSet<ProposalHasLocation> ProposalHasLocation { get; set; }
        public virtual DbSet<Province> Province { get; set; }
        public virtual DbSet<RecoveryQuestionnaire> RecoveryQuestionnaire { get; set; }
        public virtual DbSet<TypeOfPropertyUsage> TypeOfPropertyUsage { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            /*if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseNpgsql("Host=localhost;Database=proov;Username=postgres;Password=postgres");
            }*/
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.0-rtm-35687");

            modelBuilder.Entity<Agent>(entity =>
            {
                entity.ToTable("agent");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(255);

                entity.Property(e => e.FirstName)
                    .HasColumnName("first_name")
                    .HasMaxLength(255);

                entity.Property(e => e.LastName)
                    .HasColumnName("last_name")
                    .HasMaxLength(255);

                entity.Property(e => e.Token)
                    .HasColumnName("token")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<AutoIncrement>(entity =>
            {
                entity.ToTable("auto_increment");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(255)
                    .ValueGeneratedNever();

                entity.Property(e => e.IncrementNumber).HasColumnName("increment_number");
            });

            modelBuilder.Entity<City>(entity =>
            {
                entity.ToTable("city");

                entity.HasIndex(e => e.ProvinceId)
                    .HasName("fk_city_province1_idx");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(255)
                    .ValueGeneratedNever();

                entity.Property(e => e.AlternateNames)
                    .HasColumnName("alternate_names")
                    .HasMaxLength(255);

                entity.Property(e => e.FCode)
                    .HasColumnName("f_code")
                    .HasMaxLength(255);

                entity.Property(e => e.Latitude)
                    .HasColumnName("latitude")
                    .HasMaxLength(255);

                entity.Property(e => e.Longitude)
                    .HasColumnName("longitude")
                    .HasMaxLength(255);

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255);

                entity.Property(e => e.NameAscii)
                    .HasColumnName("name_ascii")
                    .HasMaxLength(255);

                entity.Property(e => e.ProvinceId)
                    .IsRequired()
                    .HasColumnName("province_id")
                    .HasMaxLength(255);

                entity.Property(e => e.Timezone)
                    .HasColumnName("timezone")
                    .HasMaxLength(255);

                entity.HasOne(d => d.Province)
                    .WithMany(p => p.City)
                    .HasForeignKey(d => d.ProvinceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_city_province1");
            });

            modelBuilder.Entity<Country>(entity =>
            {
                entity.ToTable("country");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(255)
                    .ValueGeneratedNever();

                entity.Property(e => e.Continent)
                    .HasColumnName("continent")
                    .HasMaxLength(255);

                entity.Property(e => e.CurrencyCode)
                    .HasColumnName("currency_code")
                    .HasMaxLength(255);

                entity.Property(e => e.CurrencyName)
                    .HasColumnName("currency_name")
                    .HasMaxLength(255);

                entity.Property(e => e.Languages)
                    .HasColumnName("languages")
                    .HasMaxLength(255);

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255);

                entity.Property(e => e.PhoneCode)
                    .HasColumnName("phone_code")
                    .HasMaxLength(255);

                entity.Property(e => e.PostalCode)
                    .HasColumnName("postal_code")
                    .HasMaxLength(255);

                entity.Property(e => e.PostalFormat)
                    .HasColumnName("postal_format")
                    .HasMaxLength(255);

                entity.Property(e => e.Tld)
                    .HasColumnName("tld")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<Location>(entity =>
            {
                entity.ToTable("location");

                entity.HasIndex(e => e.CityId)
                    .HasName("fk_location_city1_idx");

                entity.HasIndex(e => e.CountryId)
                    .HasName("fk_location_country1_idx");

                entity.HasIndex(e => e.ProvinceId)
                    .HasName("fk_location_province1_idx");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Address)
                    .HasColumnName("address")
                    .HasMaxLength(255);

                entity.Property(e => e.CityId)
                    .IsRequired()
                    .HasColumnName("city_id")
                    .HasMaxLength(255);

                entity.Property(e => e.CountryId)
                    .IsRequired()
                    .HasColumnName("country_id")
                    .HasMaxLength(255);

                entity.Property(e => e.PostalCode)
                    .HasColumnName("postal_code")
                    .HasMaxLength(255);

                entity.Property(e => e.ProvinceId)
                    .IsRequired()
                    .HasColumnName("province_id")
                    .HasMaxLength(255);

                entity.HasOne(d => d.City)
                    .WithMany(p => p.Location)
                    .HasForeignKey(d => d.CityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_location_city1");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.Location)
                    .HasForeignKey(d => d.CountryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_location_country1");

                entity.HasOne(d => d.Province)
                    .WithMany(p => p.Location)
                    .HasForeignKey(d => d.ProvinceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_location_province1");
            });

            modelBuilder.Entity<Payment>(entity =>
            {
                entity.ToTable("payment");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();
            });

            modelBuilder.Entity<Profile>(entity =>
            {
                entity.ToTable("profile");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.AccreditationNumber)
                    .HasColumnName("accreditation_number")
                    .HasMaxLength(255);

                entity.Property(e => e.AgentType)
                    .HasColumnName("agent_type")
                    .HasMaxLength(255);

                entity.Property(e => e.CommunicationChannel)
                    .HasColumnName("communication_channel")
                    .HasMaxLength(45);

                entity.Property(e => e.DateOfBirth)
                    .HasColumnName("date_of_birth")
                    .HasColumnType("date");

                entity.Property(e => e.DateOfCreation).HasColumnName("date_of_creation");

                entity.Property(e => e.DateOfCreationToken).HasColumnName("date_of_creation_token");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasMaxLength(255);

                entity.Property(e => e.Enabled).HasColumnName("enabled");

                entity.Property(e => e.Facebook)
                    .HasColumnName("facebook")
                    .HasMaxLength(255);

                entity.Property(e => e.FirstName)
                    .HasColumnName("first_name")
                    .HasMaxLength(255);

                entity.Property(e => e.Gender)
                    .HasColumnName("gender")
                    .HasMaxLength(255);

                entity.Property(e => e.Instagram)
                    .HasColumnName("instagram")
                    .HasMaxLength(255);

                entity.Property(e => e.IpAddress)
                    .HasColumnName("ip_address")
                    .HasMaxLength(255);

                entity.Property(e => e.Language)
                    .HasColumnName("language")
                    .HasMaxLength(255);

                entity.Property(e => e.LastName)
                    .HasColumnName("last_name")
                    .HasMaxLength(255);

                entity.Property(e => e.Linkedin)
                    .HasColumnName("linkedin")
                    .HasMaxLength(255);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password")
                    .HasMaxLength(255);

                entity.Property(e => e.Phone)
                    .HasColumnName("phone")
                    .HasMaxLength(255);

                entity.Property(e => e.PhoneConfirmed).HasColumnName("phone_confirmed");

                entity.Property(e => e.PhoneLandline)
                    .HasColumnName("phone_landline")
                    .HasMaxLength(255);

                entity.Property(e => e.Role)
                    .IsRequired()
                    .HasColumnName("role")
                    .HasMaxLength(255);

                entity.Property(e => e.Token)
                    .HasColumnName("token")
                    .HasMaxLength(255);

                entity.Property(e => e.Twitter)
                    .HasColumnName("twitter")
                    .HasMaxLength(255);

                entity.Property(e => e.VerifyCode)
                    .HasColumnName("verify_code")
                    .HasMaxLength(255);

                entity.Property(e => e.Website)
                    .HasColumnName("website")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<ProfilePhoto>(entity =>
            {
                entity.ToTable("profile_photo");

                entity.HasIndex(e => e.ProfileId)
                    .HasName("fk_profile_photo_profile1_idx");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasMaxLength(255);

                entity.Property(e => e.FileName)
                    .HasColumnName("file_name")
                    .HasMaxLength(255);

                entity.Property(e => e.ProfileId).HasColumnName("profile_id");

                entity.Property(e => e.Size)
                    .HasColumnName("size")
                    .HasMaxLength(255);

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasMaxLength(255);

                entity.HasOne(d => d.Profile)
                    .WithMany(p => p.ProfilePhoto)
                    .HasForeignKey(d => d.ProfileId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_profile_photo_profile1");
            });

            modelBuilder.Entity<ProfileUsage>(entity =>
            {
                entity.ToTable("profile_usage");

                entity.HasIndex(e => e.ProfileId)
                    .HasName("fk_profile_usage_profile1_idx");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.CategoryOfProperty).HasColumnName("category_of_property");

                entity.Property(e => e.Contingency)
                    .HasColumnName("contingency")
                    .HasMaxLength(255);

                entity.Property(e => e.FirstBuyer)
                    .HasColumnName("first_buyer")
                    .HasMaxLength(255);

                entity.Property(e => e.IsUsingBanner).HasColumnName("is_using_banner");

                entity.Property(e => e.NameOfBanner)
                    .HasColumnName("name_of_banner")
                    .HasMaxLength(255);

                entity.Property(e => e.PetsAllowed)
                    .HasColumnName("pets_allowed")
                    .HasMaxLength(255);

                entity.Property(e => e.PreApproved)
                    .HasColumnName("pre_approved")
                    .HasMaxLength(255);

                entity.Property(e => e.ProfileId).HasColumnName("profile_id");

                entity.Property(e => e.SmokingPermitted)
                    .HasColumnName("smoking_permitted")
                    .HasMaxLength(255);

                entity.Property(e => e.Urgency)
                    .HasColumnName("urgency")
                    .HasMaxLength(255);

                entity.HasOne(d => d.Profile)
                    .WithMany(p => p.ProfileUsage)
                    .HasForeignKey(d => d.ProfileId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_profile_usage_profile1");
            });

            modelBuilder.Entity<ProfileUsageHasLocation>(entity =>
            {
                entity.HasKey(e => new { e.ProfileUsageId, e.LocationId })
                    .HasName("profile_usage_has_location_pkey");

                entity.ToTable("profile_usage_has_location");

                entity.HasIndex(e => e.LocationId)
                    .HasName("fk_profile_usage_has_location_location1_idx");

                entity.HasIndex(e => e.ProfileUsageId)
                    .HasName("fk_profile_usage_has_location_profile_usage1_idx");

                entity.Property(e => e.ProfileUsageId).HasColumnName("profile_usage_id");

                entity.Property(e => e.LocationId).HasColumnName("location_id");

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.ProfileUsageHasLocation)
                    .HasForeignKey(d => d.LocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_profile_usage_has_location_location1");

                entity.HasOne(d => d.ProfileUsage)
                    .WithMany(p => p.ProfileUsageHasLocation)
                    .HasForeignKey(d => d.ProfileUsageId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_profile_usage_has_location_profile_usage1");
            });

            modelBuilder.Entity<Property>(entity =>
            {
                entity.ToTable("property");

                entity.HasIndex(e => e.LocationId)
                    .HasName("fk_property_location1_idx");

                entity.HasIndex(e => e.ProfileId)
                    .HasName("fk_property_profile1_idx");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Bathrooms).HasColumnName("bathrooms");

                entity.Property(e => e.Bedrooms).HasColumnName("bedrooms");

                entity.Property(e => e.Characteristics)
                    .HasColumnName("characteristics")
                    .HasMaxLength(45);

                entity.Property(e => e.CreatedDate).HasColumnName("created_date");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(45);

                entity.Property(e => e.LocationId).HasColumnName("location_id");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("numeric(45,0)");

                entity.Property(e => e.ProfileId).HasColumnName("profile_id");

                entity.Property(e => e.SaleType)
                    .HasColumnName("sale_type")
                    .HasMaxLength(45);

                entity.Property(e => e.Size)
                    .HasColumnName("size")
                    .HasColumnType("numeric");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(45);

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasMaxLength(45);

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.Property)
                    .HasForeignKey(d => d.LocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_property_location1");

                entity.HasOne(d => d.Profile)
                    .WithMany(p => p.Property)
                    .HasForeignKey(d => d.ProfileId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_property_profile1");
            });

            modelBuilder.Entity<PropertyPhoto>(entity =>
            {
                entity.ToTable("property_photo");

                entity.HasIndex(e => e.PropertyId)
                    .HasName("fk_property_photo_property1_idx");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.ContentType)
                    .HasColumnName("content_type")
                    .HasMaxLength(45);

                entity.Property(e => e.CreatedDate).HasColumnName("created_date");

                entity.Property(e => e.FileName)
                    .HasColumnName("file_name")
                    .HasMaxLength(45);

                entity.Property(e => e.PropertyId).HasColumnName("property_id");

                entity.Property(e => e.Size).HasColumnName("size");

                entity.Property(e => e.ThumbnailFileName)
                    .HasColumnName("thumbnail_file_name")
                    .HasMaxLength(45);

                entity.Property(e => e.ThumbnailSize).HasColumnName("thumbnail_size");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasMaxLength(45);

                entity.Property(e => e.Url)
                    .HasColumnName("url")
                    .HasMaxLength(45);

                entity.HasOne(d => d.Property)
                    .WithMany(p => p.PropertyPhoto)
                    .HasForeignKey(d => d.PropertyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_property_photo_property1");
            });

            modelBuilder.Entity<Proposal>(entity =>
            {
                entity.ToTable("proposal");

                entity.HasIndex(e => e.ProfileId)
                    .HasName("fk_proposal_profile1_idx");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Area)
                    .HasColumnName("area")
                    .HasColumnType("numeric");

                entity.Property(e => e.BankingInstitution)
                    .HasColumnName("banking_institution")
                    .HasMaxLength(255);

                entity.Property(e => e.Bathrooms).HasColumnName("bathrooms");

                entity.Property(e => e.Bedrooms).HasColumnName("bedrooms");

                entity.Property(e => e.CategoryOfProperty).HasColumnName("category_of_property");

                entity.Property(e => e.DateOfCreation).HasColumnName("date_of_creation");

                entity.Property(e => e.Elevator)
                    .HasColumnName("elevator")
                    .HasMaxLength(255);

                entity.Property(e => e.IsCurrentlyOwner).HasColumnName("is_currently_owner");

                entity.Property(e => e.IsEnabled).HasColumnName("is_enabled");

                entity.Property(e => e.IsFirstBuyer).HasColumnName("is_first_buyer");

                entity.Property(e => e.IsPreApproved).HasColumnName("is_pre_approved");

                entity.Property(e => e.IsPublished).HasColumnName("is_published");

                entity.Property(e => e.IsThereContingency).HasColumnName("is_there_contingency");

                entity.Property(e => e.IsWithPool).HasColumnName("is_with_pool");

                entity.Property(e => e.NearbyNavigableWaterBody)
                    .HasColumnName("nearby_navigable_water_body")
                    .HasMaxLength(255);

                entity.Property(e => e.NearbyParc)
                    .HasColumnName("nearby_parc")
                    .HasMaxLength(255);

                entity.Property(e => e.NearbyPublicTransport)
                    .HasColumnName("nearby_public_transport")
                    .HasMaxLength(255);

                entity.Property(e => e.NearbySchool)
                    .HasColumnName("nearby_school")
                    .HasMaxLength(255);

                entity.Property(e => e.NearbySportsCenter)
                    .HasColumnName("nearby_sports_center")
                    .HasMaxLength(255);

                entity.Property(e => e.NearbyTrade)
                    .HasColumnName("nearby_trade")
                    .HasMaxLength(255);

                entity.Property(e => e.NearbyWaterfront)
                    .HasColumnName("nearby_waterfront")
                    .HasMaxLength(255);

                entity.Property(e => e.NumberOfGarage).HasColumnName("number_of_garage");

                entity.Property(e => e.NumberOfParking).HasColumnName("number_of_parking");

                entity.Property(e => e.PriceMaximum)
                    .HasColumnName("price_maximum")
                    .HasColumnType("numeric");

                entity.Property(e => e.PriceMinimum)
                    .HasColumnName("price_minimum")
                    .HasColumnType("numeric");

                entity.Property(e => e.ProfileId).HasColumnName("profile_id");

                entity.Property(e => e.Urgency)
                    .HasColumnName("urgency")
                    .HasMaxLength(255);

                entity.Property(e => e.Wheelchair)
                    .HasColumnName("wheelchair")
                    .HasMaxLength(255);

                entity.HasOne(d => d.Profile)
                    .WithMany(p => p.Proposal)
                    .HasForeignKey(d => d.ProfileId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_proposal_profile1");
            });

            modelBuilder.Entity<ProposalHasLocation>(entity =>
            {
                entity.ToTable("proposal_has_location");

                entity.HasIndex(e => e.LocationId)
                    .HasName("fk_proposal_has_location_location1_idx");

                entity.HasIndex(e => e.ProposalId)
                    .HasName("fk_proposal_has_location_proposal1_idx");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.LocationId).HasColumnName("location_id");

                entity.Property(e => e.ProposalId).HasColumnName("proposal_id");

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.ProposalHasLocation)
                    .HasForeignKey(d => d.LocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_proposal_has_location_location1");

                entity.HasOne(d => d.Proposal)
                    .WithMany(p => p.ProposalHasLocation)
                    .HasForeignKey(d => d.ProposalId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_proposal_has_location_proposal1");
            });

            modelBuilder.Entity<Province>(entity =>
            {
                entity.ToTable("province");

                entity.HasIndex(e => e.CountryId)
                    .HasName("fk_province_country1_idx");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(255)
                    .ValueGeneratedNever();

                entity.Property(e => e.CountryId)
                    .IsRequired()
                    .HasColumnName("country_id")
                    .HasMaxLength(255);

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255);

                entity.Property(e => e.NameAscii)
                    .HasColumnName("name_ascii")
                    .HasMaxLength(255);

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.Province)
                    .HasForeignKey(d => d.CountryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_province_country1");
            });

            modelBuilder.Entity<RecoveryQuestionnaire>(entity =>
            {
                entity.ToTable("recovery_questionnaire");

                entity.HasIndex(e => e.ProfileId)
                    .HasName("fk_recovery_questionnaire_profile1_idx");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.ProfileId).HasColumnName("profile_id");

                entity.Property(e => e.RecoveryAnswer)
                    .HasColumnName("recovery_answer")
                    .HasMaxLength(255);

                entity.Property(e => e.RecoveryQuestion)
                    .HasColumnName("recovery_question")
                    .HasMaxLength(255);

                entity.HasOne(d => d.Profile)
                    .WithMany(p => p.RecoveryQuestionnaire)
                    .HasForeignKey(d => d.ProfileId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_recovery_questionnaire_profile1");
            });

            modelBuilder.Entity<TypeOfPropertyUsage>(entity =>
            {
                entity.ToTable("type_of_property_usage");

                entity.HasIndex(e => e.ProfileUsageId)
                    .HasName("fk_type_of_property_usage_profile_usage1_idx");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(45);

                entity.Property(e => e.PriceMaximum)
                    .HasColumnName("price_maximum")
                    .HasColumnType("numeric");

                entity.Property(e => e.PriceMinimum)
                    .HasColumnName("price_minimum")
                    .HasColumnType("numeric");

                entity.Property(e => e.ProfileUsageId).HasColumnName("profile_usage_id");

                entity.HasOne(d => d.ProfileUsage)
                    .WithMany(p => p.TypeOfPropertyUsage)
                    .HasForeignKey(d => d.ProfileUsageId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_type_of_property_usage_profile_usage1");
            });
        }
    }
}
