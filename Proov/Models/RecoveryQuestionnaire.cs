﻿using System;
using System.Collections.Generic;

namespace Proov.Models
{
    public partial class RecoveryQuestionnaire
    {
        public int Id { get; set; }
        public string RecoveryQuestion { get; set; }
        public string RecoveryAnswer { get; set; }
        public long ProfileId { get; set; }

        public virtual Profile Profile { get; set; }
    }
}
