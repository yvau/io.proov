﻿using System;
using System.Collections.Generic;

namespace Proov.Models
{
    public partial class ProposalHasLocation
    {
        public long Id { get; set; }
        public long LocationId { get; set; }
        public long ProposalId { get; set; }

        public virtual Location Location { get; set; }
        public virtual Proposal Proposal { get; set; }
    }
}
