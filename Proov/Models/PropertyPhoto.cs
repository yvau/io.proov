﻿using System;
using System.Collections.Generic;

namespace Proov.Models
{
    public partial class PropertyPhoto
    {
        public long Id { get; set; }
        public string Type { get; set; }
        public long? Size { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ThumbnailFileName { get; set; }
        public long? ThumbnailSize { get; set; }
        public string FileName { get; set; }
        public string Url { get; set; }
        public string ContentType { get; set; }
        public long PropertyId { get; set; }

        public virtual Property Property { get; set; }
    }
}
