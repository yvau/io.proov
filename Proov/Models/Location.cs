﻿using System;
using System.Collections.Generic;

namespace Proov.Models
{
    public partial class Location
    {
        public Location()
        {
            ProfileUsageHasLocation = new HashSet<ProfileUsageHasLocation>();
            Property = new HashSet<Property>();
            ProposalHasLocation = new HashSet<ProposalHasLocation>();
        }

        public long Id { get; set; }
        public string Address { get; set; }
        public string PostalCode { get; set; }
        public string CityId { get; set; }
        public string CountryId { get; set; }
        public string ProvinceId { get; set; }

        public virtual City City { get; set; }
        public virtual Country Country { get; set; }
        public virtual Province Province { get; set; }
        public virtual ICollection<ProfileUsageHasLocation> ProfileUsageHasLocation { get; set; }
        public virtual ICollection<Property> Property { get; set; }
        public virtual ICollection<ProposalHasLocation> ProposalHasLocation { get; set; }
    }
}
