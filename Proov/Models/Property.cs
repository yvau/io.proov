﻿using System;
using System.Collections.Generic;

namespace Proov.Models
{
    public partial class Property
    {
        public Property()
        {
            PropertyPhoto = new HashSet<PropertyPhoto>();
        }

        public long Id { get; set; }
        public string Type { get; set; }
        public string SaleType { get; set; }
        public string Description { get; set; }
        public decimal? Price { get; set; }
        public int? Bedrooms { get; set; }
        public int? Bathrooms { get; set; }
        public string Characteristics { get; set; }
        public string Status { get; set; }
        public decimal? Size { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long ProfileId { get; set; }
        public long LocationId { get; set; }

        public virtual Location Location { get; set; }
        public virtual Profile Profile { get; set; }
        public virtual ICollection<PropertyPhoto> PropertyPhoto { get; set; }
    }
}
