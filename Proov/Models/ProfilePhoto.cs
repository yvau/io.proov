﻿using System;
using System.Collections.Generic;

namespace Proov.Models
{
    public partial class ProfilePhoto
    {
        public long Id { get; set; }
        public string Type { get; set; }
        public string Size { get; set; }
        public string CreatedDate { get; set; }
        public string FileName { get; set; }
        public long ProfileId { get; set; }

        public virtual Profile Profile { get; set; }
    }
}
