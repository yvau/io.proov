﻿using System;
using System.Collections.Generic;

namespace Proov.Models
{
    public partial class TypeOfPropertyUsage
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal? PriceMinimum { get; set; }
        public decimal? PriceMaximum { get; set; }
        public int ProfileUsageId { get; set; }

        public virtual ProfileUsage ProfileUsage { get; set; }
    }
}
