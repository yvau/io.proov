﻿using System;
using System.Collections.Generic;

namespace Proov.Models
{
    public partial class Agent
    {
        public long Id { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Token { get; set; }
    }
}
