﻿using System;
using System.Collections.Generic;

namespace Proov.Models
{
    public partial class ProfileUsage
    {
        public ProfileUsage()
        {
            ProfileUsageHasLocation = new HashSet<ProfileUsageHasLocation>();
            TypeOfPropertyUsage = new HashSet<TypeOfPropertyUsage>();
        }

        public int Id { get; set; }
        public long ProfileId { get; set; }
        public bool? IsUsingBanner { get; set; }
        public string NameOfBanner { get; set; }
        public string SmokingPermitted { get; set; }
        public string PetsAllowed { get; set; }
        public string PreApproved { get; set; }
        public string FirstBuyer { get; set; }
        public string Contingency { get; set; }
        public string Urgency { get; set; }
        public string CategoryOfProperty { get; set; }

        public virtual Profile Profile { get; set; }
        public virtual ICollection<ProfileUsageHasLocation> ProfileUsageHasLocation { get; set; }
        public virtual ICollection<TypeOfPropertyUsage> TypeOfPropertyUsage { get; set; }
    }
}
