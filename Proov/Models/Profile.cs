﻿using System;
using System.Collections.Generic;

namespace Proov.Models
{
    public partial class Profile
    {
        public Profile()
        {
            ProfilePhoto = new HashSet<ProfilePhoto>();
            ProfileUsage = new HashSet<ProfileUsage>();
            Property = new HashSet<Property>();
            Proposal = new HashSet<Proposal>();
            RecoveryQuestionnaire = new HashSet<RecoveryQuestionnaire>();
        }

        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }
        public DateTime DateOfCreationToken { get; set; }
        public string Password { get; set; }
        public DateTime? DateOfCreation { get; set; }
        public string Role { get; set; }
        public bool Enabled { get; set; }
        public string IpAddress { get; set; }
        public string PhoneLandline { get; set; }
        public string Phone { get; set; }
        public bool? PhoneConfirmed { get; set; }
        public string VerifyCode { get; set; }
        public string Gender { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string AgentType { get; set; }
        public string AccreditationNumber { get; set; }
        public string Website { get; set; }
        public string Linkedin { get; set; }
        public string Facebook { get; set; }
        public string Instagram { get; set; }
        public string Twitter { get; set; }
        public string Language { get; set; }
        public string CommunicationChannel { get; set; }

        public virtual ICollection<ProfilePhoto> ProfilePhoto { get; set; }
        public virtual ICollection<ProfileUsage> ProfileUsage { get; set; }
        public virtual ICollection<Property> Property { get; set; }
        public virtual ICollection<Proposal> Proposal { get; set; }
        public virtual ICollection<RecoveryQuestionnaire> RecoveryQuestionnaire { get; set; }
    }
}
