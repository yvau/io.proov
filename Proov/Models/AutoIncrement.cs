﻿using System;
using System.Collections.Generic;

namespace Proov.Models
{
    public partial class AutoIncrement
    {
        public string Id { get; set; }
        public long? IncrementNumber { get; set; }
    }
}
