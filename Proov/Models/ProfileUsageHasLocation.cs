﻿using System;
using System.Collections.Generic;

namespace Proov.Models
{
    public partial class ProfileUsageHasLocation
    {
        public int ProfileUsageId { get; set; }
        public long LocationId { get; set; }

        public virtual Location Location { get; set; }
        public virtual ProfileUsage ProfileUsage { get; set; }
    }
}
