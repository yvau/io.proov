﻿using System;
using System.Collections.Generic;

namespace Proov.Models
{
    public partial class Proposal
    {
        public Proposal()
        {
            ProposalHasLocation = new HashSet<ProposalHasLocation>();
        }

        public long Id { get; set; }
        public int? Bathrooms { get; set; }
        public int? Bedrooms { get; set; }
        public decimal? Area { get; set; }
        public int? NumberOfParking { get; set; }
        public int? NumberOfGarage { get; set; }
        public string CategoryOfProperty { get; set; }
        public decimal? PriceMinimum { get; set; }
        public decimal? PriceMaximum { get; set; }
        public bool? IsCurrentlyOwner { get; set; }
        public bool? IsThereContingency { get; set; }
        public bool? IsWithPool { get; set; }
        public bool? IsPreApproved { get; set; }
        public string BankingInstitution { get; set; }
        public string Urgency { get; set; }
        public long ProfileId { get; set; }
        public bool? IsEnabled { get; set; }
        public bool? IsPublished { get; set; }
        public bool? IsFirstBuyer { get; set; }
        public DateTime? DateOfCreation { get; set; }
        public string Elevator { get; set; }
        public string Wheelchair { get; set; }
        public string NearbySchool { get; set; }
        public string NearbyParc { get; set; }
        public string NearbySportsCenter { get; set; }
        public string NearbyTrade { get; set; }
        public string NearbyPublicTransport { get; set; }
        public string NearbyWaterfront { get; set; }
        public string NearbyNavigableWaterBody { get; set; }

        public virtual Profile Profile { get; set; }
        public virtual ICollection<ProposalHasLocation> ProposalHasLocation { get; set; }
    }
}
