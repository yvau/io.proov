using FluentValidation;
using Proov.BindingModels;
using System;

namespace Proov.Validators
{
    public class CompleteInformationValidator : AbstractValidator<ProfileModelView>
    {

        public CompleteInformationValidator()
        {
            RuleFor(profileModelView => profileModelView.LastName)
                .NotEmpty();

            RuleFor(profileModelView => profileModelView.FirstName)
                .MinimumLength(1)
                .When(profileModelView => !String.IsNullOrWhiteSpace(profileModelView.Password));

            RuleFor(profileModelView => profileModelView.Gender)
                .NotEmpty();

            RuleFor(profileModelView => profileModelView.PhoneMobile)
                .NotEmpty();

            /*RuleFor(profileModelView => profileModelView.PhoneOffice)
                .NotEmpty();*/

            RuleFor(profileModelView => profileModelView.Website)
                .NotEmpty();

            /*RuleFor(profileModelView => profileModelView.Linkedin)
                .NotEmpty();*/

            /*RuleFor(profileModelView => profileModelView.Facebook)
                .NotEmpty();*/

            /*RuleFor(profileModelView => profileModelView.Instagram)
                .NotEmpty();*/

            /*RuleFor(profileModelView => profileModelView.Twitter)
                .NotEmpty();*/
        }
    }
}
