using FluentValidation;
using Proov.BindingModels;
using Proov.Interfaces;
using System;

namespace Proov.Validators
{
    public class VerifyCodeValidator : AbstractValidator<ProfileModelView>
    {

        public VerifyCodeValidator()
        {
            RuleFor(profileModelView => profileModelView.VerifyCode)
                .NotEmpty()
                .Length(6);
        }
    }
}
