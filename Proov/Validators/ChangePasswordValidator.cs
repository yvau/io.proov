using FluentValidation;
using Proov.BindingModels;
using System;

namespace Proov.Validators
{
    public class ChangePasswordValidator : AbstractValidator<ProfileModelView>
    {
        private string passwordPattern = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[+@#$%]).{6,20})";

        public ChangePasswordValidator()
        {
            RuleFor(profileModelView => profileModelView.Password)
                .NotEmpty()
                .Matches(passwordPattern);

            RuleFor(ProfileModelView => ProfileModelView.RePassword)
                .Equal(profileModelView => profileModelView.Password)
                .When(profileModelView => !String.IsNullOrWhiteSpace(profileModelView.Password));
        }
    }
}
