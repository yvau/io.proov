using FluentValidation;
using Proov.BindingModels;

namespace Proov.Validators
{
    public class CompleteRoleValidator : AbstractValidator<ProfileModelView>
    {

        public CompleteRoleValidator()
        {
            RuleFor(profileModelView => profileModelView.Role)
                .NotEmpty();
        }
    }
}
