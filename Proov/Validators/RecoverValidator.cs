using FluentValidation;
using Proov.BindingModels;
using Proov.Interfaces;
using Proov.Resources;
using System;

namespace Proov.Validators
{
    public class RecoverValidator : AbstractValidator<ProfileModelView>
    {
        private readonly IProfileInterface profileinterface;
        private readonly LocService _localizer;

        public RecoverValidator(IProfileInterface profileinterface,
                                LocService _localizer)
        {
            this.profileinterface = profileinterface;

            RuleFor(profileModelView => profileModelView.Email)
                .NotEmpty()
                .EmailAddress()
                .Must(isEmailExist)
                .WithMessage(_localizer.GetLocalizedHtmlString("EmailDontExist"));
        }

        private Boolean isEmailExist(String email)
        {
            var profile = profileinterface.FindByEmail(email);
            if (profile == null)
            {
                return false;
            }
            return true;
        }
    }
}
