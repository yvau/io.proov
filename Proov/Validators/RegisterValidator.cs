using FluentValidation;
using Proov.BindingModels;
using Proov.Interfaces;
using Proov.Resources;
using System;

namespace Proov.Validators
{
    public class RegisterValidator : AbstractValidator<ProfileModelView>
    {
        private readonly IProfileInterface profileinterface;
        private readonly LocService _localizer;
        private string passwordPattern = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[+@#$%]).{6,20})";

        public RegisterValidator(IProfileInterface profileinterface,
                                 LocService _localizer)
        {
            this.profileinterface = profileinterface;

            RuleFor(profileModelView => profileModelView.LastName)
                .NotEmpty();

            RuleFor(profileModelView => profileModelView.FirstName)
                .MinimumLength(0)
                .When(profileModelView => !String.IsNullOrWhiteSpace(profileModelView.Password));

            RuleFor(profileModelView => profileModelView.Email)
                .NotEmpty()
                .EmailAddress()
                .Must(isEmailExist)
                .WithMessage(_localizer.GetLocalizedHtmlString("EmailExist"));

            RuleFor(profileModelView => profileModelView.Password)
                .NotEmpty()
                .Matches(passwordPattern);

            RuleFor(ProfileModelView => ProfileModelView.RePassword)
                .Equal(profileModelView => profileModelView.Password)
                .When(profileModelView => !String.IsNullOrWhiteSpace(profileModelView.Password))
                .WithMessage(_localizer.GetLocalizedHtmlString("ConfirmPasswordNotMatching"));
        }

        private Boolean isEmailExist(String email)
        {
            var profile = profileinterface.FindByEmail(email);
            if (profile == null)
            {
                return true;
            }
            return false;
        }
    }
}
