import Vue from 'vue'
import App from './App.vue'
import { router } from './plugins/vue-router'
import store from './store/index'
import './plugins/vuex-router-sync'
import './plugins/axios'
// import './plugins/fortawesome'
// import './plugins/vue-component-server'

const app = new Vue({
    store,
    router, 
    render: h => h(App)
})

export { app, router, store }
