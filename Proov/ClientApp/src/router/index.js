export default [
	{
		path: '/',
		name: 'home',
    	component: () => import('@/views/Home/Index.vue')
	},
	{
		path: '/about/',
		name: 'about',
		component: () => import('@/views/Home/About.vue')
    },
    {
        path: '/concept/',
        name: 'concept',
        component: () => import('@/views/Home/Concept.vue')
    },
    {
        path: '/advantage/',
        name: 'advantage',
        component: () => import('@/views/Home/Advantage.vue')
    },
    {
        path: '/relationship/',
        name: 'relationship',
        component: () => import('@/views/Home/Relationship.vue')
    },
    {
        path: '/blogs/',
        name: 'blogs',
        component: () => import('@/views/Home/Blogs.vue')
    },
    {
        path: '/blog/',
        name: 'blog',
        component: () => import('@/views/Home/Blog.vue')
    },
    {
        path: '/privacy-and-policy/',
        name: 'privacy',
        component: () => import('@/views/Privacy.vue')
    },
    {
        path: '/terms-of-use/',
        name: 'privacy',
        component: () => import('@/views/Privacy.vue')
    },
    {
        path: '/help/',
        name: 'help',
        component: () => import('@/views/Help.vue')
    },
    {
        path: '/auth/login/',
        name: 'login',
        component: () => import('@/views/Auth/Login.vue')
    },
    {
        path: '/auth/register/',
        name: 'register',
        component: () => import('@/views/Auth/Register.vue')
    },
    {
        path: '/auth/recover-password/',
        name: 'recover',
        component: () => import('@/views/Auth/Recover.vue')
    },
    {
        path: '/auth/verify-code/',
        name: 'verifyCode',
        component: () => import('@/views/Auth/VerifyCode.vue')
    },
    {
        path: '/auth/change-password/',
        name: 'changePassword',
        component: () => import('@/views/Auth/ChangePassword.vue')
    },
    {
        path: '/auth/complete-role/',
        name: 'completeRole',
        component: () => import('@/views/Auth/CompleteRole.vue')
    },
    {
        path: '/auth/complete-information/',
        name: 'completeInformation',
        component: () => import('@/views/Auth/CompleteInformation.vue')
    },
    {
        path: '/auth/complete-done/',
        name: 'completeIsDone',
        component: () => import('@/views/Auth/CompleteIsDone.vue')
    },
    {
        path: '/auth/complete-seller/',
        name: 'completeSeller',
        component: () => import('@/views/Auth/CompleteSeller.vue')
    },
    {
        path: '/auth/complete-tenant/',
        name: 'completeTenant',
        component: () => import('@/views/Auth/CompleteTenant.vue')
    },
    {
        path: '/app/home/',
        name: 'appHome',
        component: () => import('@/views/App/Index.vue')
    },
    {
        path: '/profile/:id(\\d+)/',
        name: 'showProfile',
        component: () => import('@/views/Profile/Show.vue')
    },
    {
        path: '/profile/edit/role/',
        name: 'editRole',
        component: () => import('@/views/Profile/EditRole.vue')
    },
    {
        path: '/profile/edit/password/',
        name: 'editPassword',
        component: () => import('@/views/Profile/EditPassword.vue')
    },
    {
        path: '/profile/edit/information/',
        name: 'editInformation',
        component: () => import('@/views/Profile/EditInformation.vue')
    },
    {
        path: '/buy/new/',
        name: 'buyNew',
        component: () => import('@/views/Buy/Create.vue')
    },
    {
        path: '/buy/list/',
        name: 'buyList',
        component: () => import('@/views/Buy/List.vue')
    },
    {
        path: '/buy/edit/:id(\\d+)/',
        name: 'buyEdit',
        component: () => import('@/views/Buy/Update.vue')
    },
    {
        path: '/rent/new/',
        name: 'rentNew',
        component: () => import('@/views/Rent/Create.vue')
    },
    {
        path: '/rent/list/',
        name: 'rentList',
        component: () => import('@/views/Rent/List.vue')
    },
    {
        path: '/rent/edit/:id(\\d+)/',
        name: 'rentEdit',
        component: () => import('@/views/Rent/Update.vue')
    },
    {
        path: '/proposal/:id(\\d+)/',
        component: () => import('@/views/Proposal/Show.vue'),
        children: [
            {
              path: '',
              name: 'showProposal.overview',
              component: () => import('@/views/Proposal/_Overview/Index'),
              // If the user needs to be a guest to view this page.
            }
        ]
    },
    {
        path: '/property/new/',
        name: 'propertyNew',
        component: () => import('@/views/Property/Create.vue')
    },
    {
        path: '/property/list/',
        name: 'propertyList',
        component: () => import('@/views/Property/List.vue')
    },
    {
        path: '/property/:id(\\d+)/',
        name: 'propertyShow',
        component: () => import('@/views/Property/Show.vue')
    },
    {
        path: '/property/edit/:id(\\d+)/',
        name: 'propertyEdit',
        component: () => import('@/views/Property/Update.vue')
    }
]
