import Vue from 'vue'
import vClickOutside from 'v-click-outside'

Vue.component('message-alert', () => import('@/components/Alert'))
Vue.component('base-dropdown', () => import('@/components/BaseDropdown'))
Vue.component('dropdown', () => import('@/components/Dropdown'))
Vue.component('modal', () => import('@/components/Modal'))

Vue.use(vClickOutside)


