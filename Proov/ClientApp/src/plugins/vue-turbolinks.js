/* ============
 * Vue turbolinks
 * ============
 *
 * vue-turbolinks is a package to allow you to easily add Vue.js components to your Turbolinks powered apps.
 * We handle the Turbolinks events to properly setup and teardown your Vue components on the page.
 *
 * https://github.com/jeffreyguenther/vue-turbolinks
 */
import Vue from 'vue'

import TurbolinksAdapter from 'vue-turbolinks'
Vue.use(TurbolinksAdapter)
