/* ============
 * Vuex Router Sync
 * ============
 *
 * Turbolinks® makes navigating your web application faster.
 *
 * https://github.com/turbolinks/turbolinks
 */

var Turbolinks = require('turbolinks')
Turbolinks.start()
