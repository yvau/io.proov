/* ============
 * Fontawesome
 * ============
 *
 * The internet's most popular icon toolkit has been redesigned and built from scratch. 
 * On top of this, features like icon font ligatures, an SVG framework, 
 * official NPM packages for popular frontend libraries like React, and access to a new CDN.
 *
 * https://github.com/FortAwesome/Font-Awesome
 */

import Vue from 'vue'
import { library, config } from '@fortawesome/fontawesome-svg-core'
import { faChevronDown, faGlobe, faSearch, faProjectDiagram, faUnlockAlt, faCheckCircle } from '@fortawesome/free-solid-svg-icons'
import { faBell, faMoneyBillAlt, faChartBar, faUserCircle, faFileCode } from '@fortawesome/free-regular-svg-icons'
import { faFacebookF, faLinkedinIn, faTwitter, faYoutube } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

config.autoAddCss =  false

library.add(faChevronDown, faGlobe, faChartBar, faUserCircle, faUnlockAlt, faProjectDiagram, faFileCode, faSearch, faMoneyBillAlt, faBell, faFacebookF, faLinkedinIn, faTwitter, faYoutube, faCheckCircle)
Vue.component('font-awesome-icon', FontAwesomeIcon)