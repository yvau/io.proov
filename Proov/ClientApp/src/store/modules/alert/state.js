/* ============
 * State of the auth module
 * ============
 *
 * The initial state of the auth module.
 */

export default {
    status: false,
    success: null,
    message: null
}
