/* ============
 * Mutations for the auth module
 * ============
 *
 * The mutations that are available on the
 * account module.
 */

import {
    MESSAGE
} from './mutation-types'

export default {
    [MESSAGE](state, payload) {
        state.message = payload.message
        state.success = payload.success
        state.status = payload.status
    }
}
