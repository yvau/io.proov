/* ============
 * Actions for the auth module
 * ============
 *
 * The actions that are available on the
 * auth module.
 */

import Vue from 'vue'
import store from '@/store/index'
import * as types from './mutation-types'
import Proxy from '@/proxies/AuthProxy'
// import service from '@/services/index'

export const check = ({ commit }) => {
    commit(types.CHECK)
}

export const login = ({ commit }, payload) => {
    new Proxy()
        .login(payload)
        .then((response) => {
            if (response.success) {
                Vue.router.push({
                    name: 'verifyCode',
                    query: { redirectUrl: response.data.redirectUrl }
                })
            } else {
                store.dispatch('alert/message', { message: response.message, status: true, success: response.success })
            }
        })
        .catch((error) => {
            console.log('Request failed...' + error)
        })
}

export const activate = ({ commit }, payload) => {
    new Proxy().setParameter('t', payload).find('activate')
        .then((response) => {
            if (response.success) {
                commit(types.LOGIN, response.message)
                // store.dispatch('account/find')
                Vue.router.push({
                    name: 'home.index'
                })
            } else {
                store.dispatch('alert/message', { message: response.error, status: true, success: response.success })
            }
        })
        .catch((err) => {
            console.log(err)
        })

    /* Vue.router.push({
      name: 'home.index'
    }) */
}

export const recover = ({ commit }, payload) => {
    new Proxy().setParameter('token', payload).find('recover')
        .then((response) => {
            if (response.field !== 'Success') {
                store.dispatch('alert/message', { message: response.error, status: true, success: response.success })
            }
            // commit(types.LOGIN, response)
            // store.dispatch('account/find')
            /* Vue.router.push({
              name: 'home.index'
            }) */
        })
        .catch(() => {

        })

    /* Vue.router.push({
      name: 'home.index'
    }) */
}

export const logout = ({ commit }) => {
    commit(types.LOGOUT)
    Vue.router.push({
        name: 'login.index'
    })
}

export default {
    check,
    login,
    activate,
    recover,
    logout
}
