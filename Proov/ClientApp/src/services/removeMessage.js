export default () => {
    /*
     * We change the value of the errors messages
     */

    let $form = document.querySelector('form')
    for (let i = 0; i < $form.querySelectorAll('div.js-form-group').length; i++) {
        let item = $form.querySelectorAll('div.js-form-group')[i]
        item.classList.remove('has-error')
        console.log(item.querySelector('span.error').innerHTML = '')
    }
}
