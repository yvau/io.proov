export default (payload) => {
    /*
     * We change the value of the errors messages
     */
    if (Array.isArray(payload.response.data)) {
        for (let i = 0; i < payload.object.form.length; i++) {
            let item = payload.object.form[i]
            payload.response.data.some((function (e) {
                let status = item.classList.contains('group_' + e.propertyName)
                if (status) {
                    item.parentElement.classList.add('has-error')
                    item.parentElement.querySelector('span.error').innerHTML = e.errorMessage
                } else {
                    item.parentElement.classList.remove('has-error')
                    item.parentElement.querySelector('span.error') == null ? '' : item.parentElement.querySelector('span.error').innerHTML = ''
                }
                return status
            }))
        }
    }

    if (!Array.isArray(payload.response.data)) {
        for (let i = 0; i < payload.object.form.length; i++) {
            let item = payload.object.form[i]
            item.parentElement.classList.remove('has-error')
            item.parentElement.querySelector('span.error') == null ? '' : item.parentElement.querySelector('span.error').innerHTML = ''
        }
    }
}
