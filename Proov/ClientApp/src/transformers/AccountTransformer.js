/* ============
 * Account Transformer
 * ============
 *
 * The transformer for the account.
 */

import Transformer from './Transformer'

export default class AccountTransformer extends Transformer {
  /**
   * Method used to transform a fetched account.
   *
   * @param account The fetched account.
   *
   * @returns {Object} The transformed account.
   */
  static fetch (account) {
    return {
      email: account.credential,
      firstName: account.profile_information_id.first_name,
      lastName: account.profile_information_id.last_name,
      photoUrl: (account.profile_information_id.profile_photo_id === null) ? '/static/images/svg/user.svg' : account.profile_information_id.profile_photo_id.url
    }
  }

  /**
   * Method used to transform a send account.
   *
   * @param account The account to be send.
   *
   * @returns {Object} The transformed account.
   */
  static send (account) {
    return {
      email: account.email,
      first_name: account.firstName,
      last_name: account.lastName
    }
  }

  /**
   * Method used to transform a send role.
   *
   * @param role The role to be send.
   *
   * @returns {Object} The transformed role.
   */
  static sendInformation (account) {
    let formData = new FormData()
    formData.append('firstName', account.firstName)
    formData.append('lastName', account.lastName)
    formData.append('gender', account.gender)
    formData.append('birthDate', account.birthDate)
    return formData
  }
}
