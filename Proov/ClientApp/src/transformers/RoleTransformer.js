/* ============
 * Role Transformer
 * ============
 *
 * The transformer for the role.
 */

import Transformer from './Transformer'

export default class RoleTransformer extends Transformer {
  /**
   * Method used to transform a send role.
   *
   * @param role The role to be send.
   *
   * @returns {Object} The transformed role.
   */
  static send (data) {
    return {
        role: data.role.toString().toUpperCase()
    }
  }
}
