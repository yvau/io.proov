/* ============
 * Role Transformer
 * ============
 *
 * The transformer for the role.
 */

import Transformer from './Transformer'

export default class CompleteInformationTransformer extends Transformer {
    /**
    * Method used to transform a send information.
    *
    * @param role The information to be send.
    *
    * @returns {Object} The transformed role.
    */
    static send(data) {
        return {
            firstName: data.firstName,
            lastName: data.lastName,
            gender: data.gender,
            phoneMobile: (data.phoneMobile == '+1 (___) ___-____') ? '' : data.phoneMobile,
            phoneOffice: (data.phoneOffice == '+1 (___) ___-____') ? '' : data.phoneOffice,
            website: data.website,
            linkedin: data.linkedin,
            facebook: data.facebook,
            instagram: data.instagram,
            twitter: data.twitter
        }
    }
}
